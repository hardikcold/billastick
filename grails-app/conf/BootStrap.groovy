import com.billing.domain.Role;
import com.billing.domain.User

class BootStrap {

	def userService
    def init = { servletContext ->
		
		def roleList = Role.findAll()
		roleList.each{roleInstance->
			roleInstance.addToPermissions("auth:*")
			roleInstance.addToPermissions("customer:*")
			roleInstance.addToPermissions("dashboard:*")
			roleInstance.addToPermissions("estimate:*")
			roleInstance.addToPermissions("invoice:*")
			roleInstance.addToPermissions("item:*")
			roleInstance.addToPermissions("measurement:*")
			roleInstance.addToPermissions("receivedPayment:*")
			roleInstance.addToPermissions("tax:*")
			roleInstance.addToPermissions("taxType:*")
			roleInstance.addToPermissions("creditNote:*")
			roleInstance.addToPermissions("refund:*")
			roleInstance.addToPermissions("profile:*")
			roleInstance.addToPermissions("applyCredit:*")
			roleInstance.addToPermissions("report:*")
			roleInstance.addToPermissions("search:*")
			
			if(roleInstance.name == "Admin"){
				roleInstance.addToPermissions("user:*")
			}
			
			roleInstance.save()
		}
		def userInstance = User.findByUsername('admin@billastick.com')
		if(userInstance == null)
			userService.createDefaultUser()
    }
    def destroy = {
    }
}
