
dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
	username = "root"
	password = "root"
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
	//show_sql=true
}
// environment specific settings
environments {
    development {
        dataSource {
             dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			 url = "jdbc:mysql://localhost/probilling"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
			//Cloudbees Crednetial
			/*url = "jdbc:mysql://ec2-50-19-213-178.compute-1.amazonaws.com:3306/testbilling"
			username = "testbilling"
			password = "testbilling"*/
            /*url = "jdbc:mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/testbilling"
			username = "adminbfudyWd"
			password = "uimb6kElI25z"*/
			
		
			//CF Credential
			/*url = "jdbc:mysql://mysql-node01.us-east-1.aws.af.cm:3306/testbilling"
			username = "uB7CoL4Hxv9Ny"
			password = "pzAx0iaOp2yKB"*/
			
			//Secure Biz
			url = "jdbc:mysql://localhost:3306/billastick"
			username = "billastick"
			password = "Pingaxe@2013"
			/*url = "jdbc:mysql://localhost/probilling"
			username = "root"
			password = "root"*/
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
