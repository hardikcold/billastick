class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		//"/"(view:"/index")
		//"/"(controller:"index")
		
		"/"(controller:"dashboard")
		"500"(view:'/error')
	}
}
