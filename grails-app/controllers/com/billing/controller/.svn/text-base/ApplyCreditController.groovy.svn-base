package com.billing.controller

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.ApplyCredit
import com.billing.domain.CreditNote
import com.billing.domain.Invoice
import com.billing.domain.Refund

class ApplyCreditController {

	def taxService
	def invoiceService
	def itemService
	def baseService
	def refundService
	def creditNoteService
	def applyCreditService
	
	def create() {
		def creditNoteInstance = CreditNote.read(params?.id)
		def invoiceList = invoiceService.getInvoiceListByCustomer(creditNoteInstance);
		println "invoiceList :" + invoiceList.invoiceNumber
		[creditNoteInstance: creditNoteInstance, applyCreditInstance : new ApplyCredit(params), invoiceList : invoiceList]
	}
	
	def edit(){
		def applyCreditInstance = ApplyCredit.read(params?.id)
		[applyCreditInstance:applyCreditInstance ]
	}
	
	def useCredit(){
		def invoiceInstance = Invoice.read(params?.id)
		def creditNoteList = creditNoteService.getCreditNoteListByCustomer(invoiceInstance)
		[invoiceInstance: invoiceInstance, applyCreditInstance : new ApplyCredit(params), creditNoteList : creditNoteList]
	}
	
	def saveUseCredit(){
		def invoiceInstance = Invoice.read(params?.invoiceId)
		def creditNoteNumberList =  Arrays.asList(params?.creditNoteNumber)
		def creditAmountList =  Arrays.asList(params?.creditAmount)
		
		def totalCreditAmount = 0
		creditAmountList.each{
			totalCreditAmount = totalCreditAmount + NumberUtils.toDouble(it)
		}
		println "totalCreditAmount :" + totalCreditAmount
		if(totalCreditAmount > invoiceInstance.balanceDue){
			flash.errorMessage="Total Amount to Credit should be less than or equals to Invoice Balance."
			redirect(action: "search", controller:'invoice')
			return
		}
		
		applyCreditService.saveUseCredit(creditNoteNumberList, creditAmountList, invoiceInstance)
		flash.message = message(code: 'default.created.message')
		redirect(action: "search", controller:'invoice')
	}
	
	def save(){
		def creditNoteInstance = CreditNote.read(params?.creditNoteId)
		def invoiceNumberList =  Arrays.asList(params?.invoiceNumber)
		def creditAmountList =  Arrays.asList(params?.creditAmount)
		def totalCreditAmount = 0
		creditAmountList.each{
			totalCreditAmount = totalCreditAmount + NumberUtils.toDouble(it)
		}
		println "totalCreditAmount :" + totalCreditAmount
		if(totalCreditAmount > creditNoteInstance.balanceDue){
			flash.errorMessage="Total Amount to Credit should be less than or equals to Available Credits."
			redirect(action: "search", controller:'creditNote')
			return
		}
		applyCreditService.saveApplyCredit(invoiceNumberList, creditAmountList, creditNoteInstance)

		flash.message = message(code: 'default.created.message')
		redirect(action: "search", controller:'creditNote')
	}
	
	def delete(){
		println "Going to delete AppliedCredit"
		def appliedCreditInstance =  ApplyCredit.read(params?.id)
        if (!appliedCreditInstance) {
            flash.message = message(code: 'default.not.found.message')
            redirect(action: "search", controller:'creditNote')
            return
        }

        try {
            //receivedPaymentInstance.delete(flush: true)
			applyCreditService.deleteAppliedCredit(appliedCreditInstance)
            flash.message = message(code: 'default.deleted.message')
            redirect(action: "search", controller:'creditNote')
        }
        catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "search", controller:'creditNote')
        }
	}
	
}
