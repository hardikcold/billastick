package com.billing.controller

import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Measurement

class MeasurementController {

	def measurementService
	def baseService
    static allowedMethods = [save: "POST", update: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def measurementInstanceList = measurementService.getMeasurementList(params)
		def measurementInstanceListTotal = measurementService.getMeasurementListSize(params)
        render(template : 'dataList',  model : [measurementInstanceList: measurementInstanceList, measurementInstanceListTotal: measurementInstanceListTotal])
    }

    def create() {
        def measurementInstanceList = measurementService.getMeasurementList(params)
		def measurementInstanceListTotal = measurementService.getMeasurementListSize(params)
        [measurementInstance: new Measurement(params), measurementInstanceList: measurementInstanceList, measurementInstanceListTotal: measurementInstanceListTotal]
    }

    def save() {
        def measurementInstance = new Measurement(params)
		measurementInstance.user = baseService.getLoggedInUser()
		def measurementInstanceList = measurementService.getMeasurementList(params)
		def measurementInstanceListTotal = measurementService.getMeasurementListSize(params)
        if (!measurementInstance.save(flush: true)) {
            render(view: "create", model: [measurementInstance: measurementInstance, measurementInstanceList: measurementInstanceList, measurementInstanceListTotal: measurementInstanceListTotal])
            return
        }

        flash.message = message(code: 'default.created.message')
        redirect(action: "create")
    }

    def edit(Long id) {
        def measurementInstance = Measurement.get(id)
		def measurementInstanceList = measurementService.getMeasurementList(params)
		def measurementInstanceListTotal = measurementService.getMeasurementListSize(params)
        if (!measurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'measurement.label', default: 'Measurement'), id])
            redirect(action: "create")
            return
        }

        [measurementInstance: measurementInstance, measurementInstanceList: measurementInstanceList, measurementInstanceListTotal: measurementInstanceListTotal]
    }

    def update(Long id, Long version) {
        def measurementInstance = Measurement.get(id)
		def measurementInstanceList = measurementService.getMeasurementList(params)
		def measurementInstanceListTotal = measurementService.getMeasurementListSize(params)
        if (!measurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'measurement.label', default: 'Measurement'), id])
            redirect(action: "create")
            return
        }

        if (version != null) {
            if (measurementInstance.version > version) {
                measurementInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'measurement.label', default: 'Measurement')] as Object[],
                          "Another user has updated this Measurement while you were editing")
                render(view: "edit", model: [measurementInstance: measurementInstance, measurementInstanceList: measurementInstanceList, measurementInstanceListTotal: measurementInstanceListTotal])
                return
            }
        }

        measurementInstance.properties = params

        if (!measurementInstance.save(flush: true)) {
			measurementInstance.discard()
			measurementInstanceList = measurementService.getMeasurementList(params)
			measurementInstanceListTotal = measurementService.getMeasurementListSize(params)
            render(view: "edit", model: [measurementInstance: measurementInstance, measurementInstanceList: measurementInstanceList, measurementInstanceListTotal: measurementInstanceListTotal])
            return
        }

        flash.message = message(code: 'default.updated.message')
        redirect(action: "create", id: measurementInstance.id)
    }

    def delete(Long id) {
        def measurementInstance = Measurement.get(id)
        if (!measurementInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'measurement.label', default: 'Measurement'), id])
            redirect(action: "create")
            return
        }

        try {
            measurementInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'measurement.label', default: 'Measurement'), id])
            redirect(action: "create")
        }
        catch (DataIntegrityViolationException e) {
            flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "create", id: id)
        }
    }
}
