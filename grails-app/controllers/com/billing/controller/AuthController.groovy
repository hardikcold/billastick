package com.billing.controller

import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.UsernamePasswordToken
import org.apache.shiro.crypto.hash.Sha512Hash
import org.apache.shiro.grails.ConfigUtils
import org.apache.shiro.web.util.WebUtils

import com.billing.domain.Role
import com.billing.domain.User

class AuthController {
	def shiroSecurityManager
	def userService
	
	def index = { redirect(action: "login", params: params) }

	def login = {
		return [ username: params.username, rememberMe: (params.rememberMe != null), targetUri: params.targetUri ]
	}

	def forgotPassword = {
		
	}
	
	def register = {
		
	}
	
	def registerUser(){
		def userInstance = new User(params)
		def userRole = Role.findByName("Company")
		userInstance.addToRoles(userRole)
		
		if (!userInstance.validate()) {
			render(view: "register", model: [userInstance: userInstance])
			return
		}else{
			userInstance.passwordHash = new Sha512Hash(userInstance.passwordHash).toHex()
			println "Create User :" + userInstance
			userService.createUser(userInstance)
			flash.successMessage = "You have registerd to Billastick successfully. Kindly proceed to login."
		}
		
		redirect(action:'login')
		//redirect(controller:'index')

	}
	def signIn = {
		def authToken = new UsernamePasswordToken(params.username, params.password as String)

		// Support for "remember me"
		if (params.rememberMe) {
			authToken.rememberMe = true
		}
		
		// If a controller redirected to this page, redirect back
		// to it. Otherwise redirect to the root URI.
		def targetUri = params.targetUri ?: "/"
		
		// Handle requests saved by Shiro filters.
		def savedRequest = WebUtils.getSavedRequest(request)
		if (savedRequest) {
			targetUri = savedRequest.requestURI - request.contextPath
			if (savedRequest.queryString) targetUri = targetUri + '?' + savedRequest.queryString
		}
		
		try{
			// Perform the actual login. An AuthenticationException
			// will be thrown if the username is unrecognised or the
			// password is incorrect.
			SecurityUtils.subject.login(authToken)
			def userInstance = User.findByUsername(params.username)
			userInstance?.lastLoginDate = new Date()
			userInstance.save(flush:true)
			session.currentUser = userInstance
			log.info "Redirecting to '${targetUri}'."
			redirect(controller:"dashboard")
		}
		catch (AuthenticationException ex){
			// Authentication failed, so display the appropriate message
			// on the login page.
			log.info "Authentication failure for user '${params.username}'."
			flash.message = message(code: "login.failed")

			// Keep the username and "remember me" setting so that the
			// user doesn't have to enter them again.
			def m = [ username: params.username ]
			if (params.rememberMe) {
				m["rememberMe"] = true
			}

			// Remember the target URI too.
			if (params.targetUri) {
				m["targetUri"] = params.targetUri
			}

			// Now redirect back to the login page.
			redirect(controller: "index", params: m)
		}
	}

	def signOut = {
		// Log the user out of the application.
		def principal = SecurityUtils.subject?.principal
		SecurityUtils.subject?.logout()
		// For now, redirect back to the home page.
		if (ConfigUtils.getCasEnable() && ConfigUtils.isFromCas(principal)) {
			redirect(uri:ConfigUtils.getLogoutUrl())
		}else {
			redirect(uri: "/")
		}
		ConfigUtils.removePrincipal(principal)
	}
	
	def unauthorized = {
		render "You do not have permission to access this page."
	}
}
