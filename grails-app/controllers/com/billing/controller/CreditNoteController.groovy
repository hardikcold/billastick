package com.billing.controller

import grails.converters.JSON

import org.apache.commons.lang.math.NumberUtils
import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.CreditNote
import com.billing.domain.Invoice
import com.billing.domain.InvoiceDetail
import com.billing.domain.Item
import com.billing.domain.Tax
import com.billing.domain.TaxTransactionDetails
import com.billing.domain.TaxType
import com.billing.exception.BillingException

class CreditNoteController {

	static allowedMethods = [save: "POST", update: "POST", autocompleteItem : "POST"]
	def customerService
	def taxService
	def itemService
	def baseService
	def creditNoteService
	def reportService
	def refundService
	def applyCreditService

	def index() {
		redirect(action: "search", params: params)
	}

	def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def creditNoteInstanceList = creditNoteService.getCreditNoteList(params) //customerService.getCustomerList(params)
		def creditNoteInstanceListTotal = creditNoteService.getCreditNoteListSize(params) //customerService.getCustomerListSize(params)
		if(request.xhr){
			render(template : 'dataList',  model : [creditNoteInstanceList :creditNoteInstanceList , creditNoteInstanceListTotal : creditNoteInstanceListTotal])
		}else{
			[creditNoteInstanceList :creditNoteInstanceList , creditNoteInstanceListTotal : creditNoteInstanceListTotal]
		}
	}

	def view(){
		def creditNoteInstance = CreditNote.read(params?.id)
		[creditNoteInstance : creditNoteInstance]
	}

	def create() {
		def taxList = taxService.getAllTaxList();
		def creditNoteNumber = creditNoteService.getCreditNoteNumber()
		def customerList = customerService.getCustomerListByUser()
		def itemList = itemService.getAllItemByUser()
		def invoiceInstance = Invoice.read(params?.id)
		[invoiceInstance : invoiceInstance, creditNoteInstance: new CreditNote(params), taxList : taxList,  creditNoteNumber : creditNoteNumber, customerList : customerList, itemList : itemList]
	}

	def save() {

		def creditNoteInstance = new CreditNote()
		creditNoteInstance.creditNoteNumber = NumberUtils.toInt(params?.creditNoteNumber)
		def customerInstance =  customerService.getCustomerById(params?.customerId)
		def invoiceInstance = Invoice.read(params?.invoiceId)
		
		creditNoteInstance.invoice = invoiceInstance
		creditNoteInstance.customer = customerInstance
		creditNoteInstance.user = baseService.getLoggedInUser()
		creditNoteInstance.subTotal = params?.hiddenSubTotal as BigDecimal
		creditNoteInstance.netTotal = params?.hiddenNetTotal as BigDecimal
		creditNoteInstance.balanceDue = params?.hiddenNetTotal as BigDecimal
		if(params?.creditNoteDate){
			creditNoteInstance.creditNoteDate = java.util.Date.parse("dd/MM/yyyy", params?.creditNoteDate)
		}
		
		if(params?.refNumber){
			creditNoteInstance.refNumber = NumberUtils.toInt(params?.refNumber)
		}

		creditNoteInstance.customerNotes = params?.customerNotes
		
		if(creditNoteInstance.validate()){
			try{
				creditNoteService.saveCreditNote(creditNoteInstance ,params)
				flash.message = message(code: 'default.created.message')
			}catch(BillingException be){
				be.printStackTrace()
				flash.errorMessage = be.getMessage()
			}catch(Exception e){
				e.printStackTrace()
			}
		}else{
			def customerList = customerService.getCustomerListByUser()
			def taxList = taxService.getAllTaxList();
			def dbItemList = itemService.getAllItemByUser()
			render(view: "create", model: [invoiceInstance : invoiceInstance, creditNoteInstance : creditNoteInstance, taxList : taxList, params : params, customerList : customerList, itemList : dbItemList])
			return
		}

		redirect(action: "search", controller :"invoice")
	}


	def edit(Long id) {
		def creditNoteInstance = CreditNote.read(id)
		def invoiceInstance = Invoice.read(creditNoteInstance?.invoice?.id)
		if (!creditNoteInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}
		def taxList = taxService.getAllTaxList();
		def customerList = customerService.getCustomerListByUser()
		def dbItemList = itemService.getAllItemByUser()
		def totalRefundedAmount = creditNoteService.getTotalRefundedAmount(creditNoteInstance)
		def totalCreditUsedAmount = creditNoteService.getTotalCreditUsedAmount(creditNoteInstance)
		[invoiceInstance : invoiceInstance , creditNoteInstance: creditNoteInstance, taxList : taxList, customerList : customerList, itemList : dbItemList , totalRefundedAmount : totalRefundedAmount, totalCreditUsedAmount : totalCreditUsedAmount]
	}

	def update(Long id, Long version) {
		def creditNoteInstance = CreditNote.read(id)
		def customerInstance =  customerService.getCustomerById(params?.customerId)
		creditNoteInstance.user = baseService.getLoggedInUser()
		creditNoteInstance.creditNoteNumber = NumberUtils.toInt(params?.creditNoteNumber)
		creditNoteInstance.customer = customerInstance

		creditNoteInstance.subTotal = params?.hiddenSubTotal as BigDecimal
		creditNoteInstance.netTotal = params?.hiddenNetTotal as BigDecimal
		creditNoteInstance.balanceDue = params?.hiddenNetTotal as BigDecimal

		if(params?.creditNoteDate){
			params.creditNoteDate = java.util.Date.parse("dd/MM/yyyy", params?.creditNoteDate)
		}
		if(params?.refNumber){
			creditNoteInstance.refNumber = NumberUtils.toInt(params?.refNumber)
		}

		if (!creditNoteInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}

		creditNoteInstance.invoiceDetails.clear()
		creditNoteInstance.taxTransactionDetails.clear()

		if (version != null) {
			if (creditNoteInstance.version > version) {
				creditNoteInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						[
							message(code: 'customer.label', default: 'Customer')] as Object[],
						"Another user has updated this Customer while you were editing")
				def taxList = taxService.getAllTaxList();
				def customerList = customerService.getCustomerListByUser()
				def dbItemList = itemService.getAllItemByUser()
				render(view: "edit", model: [creditNoteInstance: creditNoteInstance, taxList : taxList, itemList : dbItemList, customerList : customerList])
				return
			}
		}

		creditNoteInstance.properties = params

		if (!creditNoteInstance.validate(flush: true)) {
			def taxList = taxService.getAllTaxList();
			def customerList = customerService.getCustomerListByUser()
			def dbItemList = itemService.getAllItemByUser()
			render(view: "edit", model: [creditNoteInstance: creditNoteInstance, taxList : taxList, itemList : dbItemList, customerList : customerList])
			return
		}else{
			creditNoteService.saveCreditNote(creditNoteInstance, params)
		}

		flash.message = message(code: 'default.updated.message')
		redirect(action: "edit", id: creditNoteInstance.id)
	}

	def delete(Long id) {
		def creditNoteInstance = CreditNote.read(id)
		if (!creditNoteInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search", controller :"invoice")
			return
		}

		try {
			creditNoteService.deleteCreditNote(creditNoteInstance)
			//invoiceInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message')
			redirect(action: "search", controller :"invoice")
		}catch(BillingException be){
			flash.infoMessage = be.getMessage()
			redirect(action: "search", controller :"invoice", id: id)
		}catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
			redirect(action: "search", controller :"invoice" , id: id)
		}
	}
	
	def details(){
		def creditNoteInstance = CreditNote.read(params?.id)
		[creditNoteInstance : creditNoteInstance]
	}

	
	def getItemDetails = {
		def itemInstance = Item.read(params?.id)
		def taxList = taxService.getAllTaxList();
		def itemMap = [:]
		def assignedItemTaxList = itemInstance?.tax*.id
		itemInstance?.tax?.eachWithIndex{taxInstance, i->
			i++
			itemMap.put('taxId' + i,taxInstance?.id?:"")
			itemMap.put('taxRate' + i,taxInstance?.taxRate?:0)
		}
		itemMap.put('itemRate', itemInstance?.rate)
		itemMap.put('taxList', taxList)
		itemMap.put('appliedTaxIdList', itemInstance?.tax?:"")
		itemMap.put('elementId', params?.elementId)
		render itemMap as JSON

	}

	def getTaxDetails = {
		def taxInstance = Tax.read(params?.id)
		def taxList = taxService.getAllTaxList();
		def taxMap = [:]
		taxMap.put('elementId', params?.elementId)
		taxMap.put('taxList', taxList)
		render taxMap as JSON

	}

	def printCreditNote(){
		reportService?.generateCreditNote(params)
		return
	}
	
	def creditNoteDetails(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def creditNoteInstance = CreditNote.read(params?.id)
		def refundInstanceList = refundService.getAllRefundDetailsListByCreditNote(params, creditNoteInstance)
		def refundInstanceListTotal = refundService.getAllRefundDetailsListSizeByCreditNote(params, creditNoteInstance)
		if(request.xhr){
			render(template:"refundList", model : [refundInstanceList : refundInstanceList, refundInstanceListTotal : refundInstanceListTotal, invoicedCreditListTotal : 0, creditNoteInstance : creditNoteInstance])
		}else{
			[ creditNoteInstance : creditNoteInstance ,refundInstanceList : refundInstanceList, refundInstanceListTotal : refundInstanceListTotal, invoicedCreditListTotal : 0]
		}
	}
	
	
	def getInvoicedCreditListByCreditNote(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def creditNoteInstance = CreditNote.read(params?.id)
		def invoicedCreditList = applyCreditService.getAllInvoicedCreditDetailsListByCreditNote(params, creditNoteInstance)
		def invoicedCreditListTotal = applyCreditService.getAllInvoicedCreditDetailsListSizeByCreditNote(params, creditNoteInstance)
		render (template : "invoiceCreditList", model : [invoicedCreditList : invoicedCreditList, invoicedCreditListTotal : invoicedCreditListTotal])
	}
}
