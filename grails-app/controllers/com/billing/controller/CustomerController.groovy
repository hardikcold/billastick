package com.billing.controller

import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Customer
class CustomerController {

    static allowedMethods = [save: "POST", update: "POST"]
	def customerService
	def baseService
    def index() {
        redirect(action: "search", params: params)
    }
	
	def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		println "Search Customer :" + params?.searchText
		def customerInstanceList = customerService.getCustomerList(params)
		def customerInstanceListTotal = customerService.getCustomerListSize(params)
		if(request.xhr)
			render(template : 'dataList',  model : [customerInstanceList :customerInstanceList , customerInstanceListTotal : customerInstanceListTotal])
		else	
			[customerInstanceList :customerInstanceList , customerInstanceListTotal : customerInstanceListTotal]
	}

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [customerInstanceList: Customer.list(params), customerInstanceTotal: Customer.count()]
    }

    def create() {
        [customerInstance: new Customer(params)]
    }

    def save() {
        def customerInstance = new Customer(params)
		customerInstance.user = baseService.getLoggedInUser()
        if (!customerInstance.save(flush: true)) {
            render(view: "create", model: [customerInstance: customerInstance])
            return
        }

        flash.message = message(code: 'default.created.message')
        redirect(action: "search")
    }
	
	def saveCustomerPopup(){
		def customerInstance = new Customer(params)
		customerInstance.user = baseService.getLoggedInUser()
		customerInstance.address1 = ""
		customerInstance.city = ""
		customerInstance.state = ""
		if (!customerInstance.save(flush: true)) {
			render(view: "create", model: [customerInstance: customerInstance])
			return
		}

		flash.message = "Customer was added successfully."
			redirect(action: "create", controller: "invoice")
	}
	
	def saveCustomerPopupForEstimate(){
		def customerInstance = new Customer(params)
		customerInstance.user = baseService.getLoggedInUser()
		customerInstance.address1 = ""
		customerInstance.city = ""
		customerInstance.state = ""
		if (!customerInstance.save(flush: true)) {
			render(view: "create", model: [customerInstance: customerInstance])
			return
		}

		flash.message = "Customer was added successfully."
			redirect(action: "create", controller: "estimate")
	}


    def edit(Long id) {
        def customerInstance = Customer.get(id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message')
            redirect(action: "search")
            return
        }

        [customerInstance: customerInstance]
    }

    def update(Long id, Long version) {
        def customerInstance = Customer.get(id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), id])
            redirect(action: "search")
            return
        }

        if (version != null) {
            if (customerInstance.version > version) {
                customerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'customer.label', default: 'Customer')] as Object[],
                          "Another user has updated this Customer while you were editing")
                render(view: "edit", model: [customerInstance: customerInstance])
                return
            }
        }

        customerInstance.properties = params

        if (!customerInstance.save(flush: true)) {
            render(view: "edit", model: [customerInstance: customerInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'customer.label', default: 'Customer'), customerInstance.id])
        redirect(action: "edit", id: customerInstance.id)
    }

    def delete(Long id) {
        def customerInstance = Customer.get(id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), id])
            redirect(action: "search")
            return
        }

        try {
            customerInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'customer.label', default: 'Customer'), id])
            redirect(action: "search")
        }
        catch (DataIntegrityViolationException e) {
            flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "search", id: id)
        }
    }
	
	def customerDetails(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def customerInstance = Customer.read(params?.id)
		
		def invoiceList = customerService.getInvoiceListByCustomer(params, customerInstance)
		def invoiceListTotal = customerService.getInvoiceListSizeByCustomer(params, customerInstance)
		if(request.xhr){
			render(template:'invoiceList', model : [customerInstance : customerInstance, invoiceList : invoiceList, invoiceListTotal : invoiceListTotal?:0])
		}else{
			[customerInstance : customerInstance, invoiceList : invoiceList, invoiceListTotal : invoiceListTotal?:0]
		}
	}
	
	def creditNoteDetails(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def customerInstance = Customer.read(params?.id)
		def creditNoteList = customerService.getCreditNoteListByCustomer(params, customerInstance)
		def creditNoteListTotal = customerService.getCreditNoteListSizeByCustomer(params, customerInstance)
		render(template:'creditNoteList', model : [customerInstance : customerInstance, creditNoteList : creditNoteList, creditNoteListTotal : creditNoteListTotal?:0])
	}
	
	def paymentDetails(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def customerInstance = Customer.read(params?.id)
		def receivedPaymentList = customerService.getReceivedPaymentListByCustomer(params, customerInstance)
		def receivedPaymentListTotal = customerService.getReceivedPaymentListSizeByCustomer(params, customerInstance)
		println "receivedPaymentListTotal :" + receivedPaymentListTotal
		
		render(template:'receivedPaymentList', model : [customerInstance : customerInstance, receivedPaymentList : receivedPaymentList, receivedPaymentListTotal : receivedPaymentListTotal?:0])
	}
}
