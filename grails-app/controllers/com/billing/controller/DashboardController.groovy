package com.billing.controller

import com.billing.utility.Constants;


class DashboardController {
	def baseService
	def dashboardService
    def index() {
		def userInstance  = baseService.getLoggedInUser()
		session['userName'] = userInstance?.displayName
		def totalReceipts = dashboardService.getTotalReceiptsByDate()
		def totalSales = dashboardService.getTotalSalesByDate()
		def totalDues = dashboardService.getTotalDuesByDate()
		[userInstance : userInstance, totalReceipts : totalReceipts, totalSales : totalSales, totalDues : totalDues]
	}
}
