package com.billing.controller

import grails.converters.JSON

import org.apache.commons.lang.math.NumberUtils
import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Estimate
import com.billing.domain.InvoiceDetail
import com.billing.domain.Item
import com.billing.domain.TaxType

class EstimateController {

	static allowedMethods = [save: "POST", update: "POST", autocompleteItem : "POST"]
	def customerService
	def taxService
	def itemService
	def baseService
	def invoiceService
	def reportService
	
	def index() {
		redirect(action: "search", params: params)
	}

	def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def estimateInstanceList = invoiceService.getEstimateList(params) //customerService.getCustomerList(params)
		def estimateInstanceListTotal = invoiceService.getEtimateListSize(params) //customerService.getCustomerListSize(params)
		if(request.xhr){
			render(template : 'dataList',  model : [estimateInstanceList :estimateInstanceList , estimateInstanceListTotal : estimateInstanceListTotal])
		}else{
			[estimateInstanceList :estimateInstanceList , estimateInstanceListTotal : estimateInstanceListTotal]
		}
	}

	def create() {
		def taxList = taxService.getAllTaxList();
		def estNumber = invoiceService.getEstNumber()
		def customerList = customerService.getCustomerListByUser()
		def itemList = itemService.getAllItemByUser()
		[estimateInstance: new Estimate(params), taxList : taxList, estNumber : estNumber, customerList : customerList, itemList : itemList]
	}

	def save() {
		def estimateInstance = new Estimate()
		estimateInstance.estNumber = NumberUtils.toInt(params?.estNumber)
		def customerInstance =  customerService.getCustomerById(params?.customerId)
		estimateInstance.customer = customerInstance
		estimateInstance.user = baseService.getLoggedInUser()
		
		
		estimateInstance.subTotal = params?.hiddenSubTotal as BigDecimal 
		estimateInstance.netTotal = params?.hiddenNetTotal as BigDecimal
		
		
		if(params?.estimateDate){
			estimateInstance.estimateDate = java.util.Date.parse("dd/MM/yyyy", params?.estimateDate)
		}
		if(params?.expireDate){
			estimateInstance.expireDate = java.util.Date.parse("dd/MM/yyyy", params?.expireDate)
		}
		

		estimateInstance.customerNotes = params?.customerNotes
		if(estimateInstance.validate()){
			invoiceService.saveEstimate(estimateInstance, params)
				flash.message = message(code: 'default.created.message')
		}else{
			def taxList = taxService.getAllTaxList();
			def customerList = customerService.getCustomerListByUser()
			def dbItemList = itemService.getAllItemByUser()
			render(view: "create", model: [estimateInstance : estimateInstance, taxList : taxList, customerList : customerList, itemList : dbItemList])
			return
		}

		flash.message = message(code: 'default.created.message')
		redirect(action: "estimateDetails", id : estimateInstance?.id)
	}


	def edit(Long id) {
		def estimateInstance = Estimate.read(id)
		
		if (!estimateInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}
		def taxList = taxService.getAllTaxList();
		def customerList = customerService.getCustomerListByUser()
		def dbItemList = itemService.getAllItemByUser()
		[estimateInstance: estimateInstance, taxList : taxList, customerList : customerList, itemList : dbItemList]
	}

	def update(Long id, Long version) {
		def estimateInstance = Estimate.read(id)
		def customerInstance =  customerService.getCustomerById(params?.customerId)
		estimateInstance.user = baseService.getLoggedInUser()
		estimateInstance.estNumber = NumberUtils.toInt(params?.estNumber)
		estimateInstance.customer = customerInstance
		
		estimateInstance.subTotal = params?.hiddenSubTotal as BigDecimal
		estimateInstance.netTotal = params?.hiddenNetTotal as BigDecimal
		if(params?.estimateDate){
			params.estimateDate = java.util.Date.parse("dd/MM/yyyy", params?.estimateDate)
		}
		if(params?.expireDate){
			params.expireDate = java.util.Date.parse("dd/MM/yyyy", params?.expireDate)
		}
		
		if (!estimateInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}
		
		estimateInstance.estimateDetails.clear()
		estimateInstance.estimateTaxTransactionDetails.clear()
		
		if (version != null) {
			if (estimateInstance.version > version) {
				estimateInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						[
							message(code: 'customer.label', default: 'Customer')] as Object[],
						"Another user has updated this Customer while you were editing")
				def taxList = taxService.getAllTaxList();
				def customerList = customerService.getCustomerListByUser()
				def dbItemList = itemService.getAllItemByUser()
				render(view: "edit", model: [estimateInstance: estimateInstance, taxList : taxList, itemList : dbItemList, customerList : customerList])
				return
			}
		}

		estimateInstance.properties = params
		
		if (!estimateInstance.validate(flush: true)) {
			def taxList = taxService.getAllTaxList();
			def customerList = customerService.getCustomerListByUser()
			def dbItemList = itemService.getAllItemByUser()
			render(view: "edit", model: [estimateInstance: estimateInstance, taxList : taxList, taxList : taxList, itemList : dbItemList, customerList : customerList])
			return
		}else{
			invoiceService.saveEstimate(estimateInstance, params)
		}

		flash.message = message(code: 'default.updated.message')
		redirect(action: "edit", id: estimateInstance.id)
	}

	def printEstimate(){
		reportService?.generateEstimate(params)
		return
	}
	
	def delete(Long id) {
		def estimateInstance = Estimate.read(id)
		if (!estimateInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}

		try {
			estimateInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message')
			redirect(action: "search")
		}
		catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
			redirect(action: "search", id: id)
		}
	}


	def estimateDetails(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def estimateInstance = Estimate.read(params?.id)
		[ estimateInstance : estimateInstance]
	}
}
