package com.billing.controller

import grails.converters.JSON

import org.apache.commons.lang.math.NumberUtils
import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Estimate
import com.billing.domain.GeneralLedger
import com.billing.domain.Invoice
import com.billing.domain.Item
import com.billing.domain.Tax
import com.billing.exception.BillingException

class InvoiceController {

	static allowedMethods = [save: "POST", update: "POST", autocompleteItem : "POST"]
	def customerService
	def taxService
	def itemService
	def baseService
	def invoiceService
	def reportService
	def receivedPaymentService
	def applyCreditService
	def refundService

	def index() {
		redirect(action: "search", params: params)
	}

	def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def invoiceInstanceList = invoiceService.getInvoiceList(params) //customerService.getCustomerList(params)
		def invoiceInstanceListTotal = invoiceService.getInvoiceListSize(params) //customerService.getCustomerListSize(params)
		if(request.xhr){
			render(template : 'dataList',  model : [invoiceInstanceList :invoiceInstanceList , invoiceInstanceListTotal : invoiceInstanceListTotal, isInvoiceLimitExceed : invoiceService.isInvoiceLimitExceed()])
		}else{
			[invoiceInstanceList :invoiceInstanceList , invoiceInstanceListTotal : invoiceInstanceListTotal, isInvoiceLimitExceed : invoiceService.isInvoiceLimitExceed()]
		}
	}

	def view(){
		def invoiceInstance = Invoice.read(params?.id)
		def totalCreditAppliedAmount =   invoiceService.getTotalCreditAppliedAmount(invoiceInstance)
		def totalReceiptsAmount = invoiceService.getTotalReceiptsAmount(invoiceInstance)
		[invoiceInstance : invoiceInstance, totalCreditAppliedAmount : totalCreditAppliedAmount, totalReceiptsAmount : totalReceiptsAmount]
	}

	def create() {
		if(invoiceService.isInvoiceLimitExceed()){
			redirect(action : "search")
			return
		}
		def taxList = taxService.getAllTaxList();
		def invoiceNumber = invoiceService.getInvoiceNumber()
		def customerList = customerService.getCustomerListByUser()
		def itemList = itemService.getAllItemByUser()
		def userInstance = baseService.getLoggedInUser()
		[invoiceInstance: new Invoice(params), taxList : taxList, invoiceNumber : invoiceNumber, customerList : customerList, itemList : itemList, isConvert : false]
	}

	def save() {
		def invoiceInstance = new Invoice()
		invoiceInstance.invoiceNumber = NumberUtils.toInt(params?.invoiceNumber)
		def customerInstance =  customerService.getCustomerById(params?.customerId)
		invoiceInstance.customer = customerInstance
		invoiceInstance.user = baseService.getLoggedInUser()
		
		invoiceInstance.estimateRefNumber = NumberUtils.toInt(params?.estimateRefNumber)
		if(params?.estimateDate){
			invoiceInstance.estimateDate = java.util.Date.parse("dd/MM/yyyy", params?.estimateDate)
		}
		
		invoiceInstance.subTotal = params?.hiddenSubTotal as BigDecimal
		invoiceInstance.netTotal = params?.hiddenNetTotal as BigDecimal
		invoiceInstance.balanceDue = params?.hiddenNetTotal as BigDecimal
		
		if(params?.invoiceDate){
			invoiceInstance.invoiceDate = java.util.Date.parse("dd/MM/yyyy", params?.invoiceDate)
		}
		if(params?.dueDate){
			invoiceInstance.dueDate = java.util.Date.parse("dd/MM/yyyy", params?.dueDate)
		}

		invoiceInstance.paymentType = params?.paymentType
		invoiceInstance.invoiceType = params?.invoiceType
		invoiceInstance.customerNotes = params?.customerNotes

		if(invoiceInstance.validate()){
			try{
				invoiceService.saveInvoice(invoiceInstance, params, new GeneralLedger())
				flash.message = message(code: 'default.created.message')
			}catch(BillingException be){
				be.printStackTrace()
				flash.errorMessage = be.getMessage()
			}catch(Exception e){
				e.printStackTrace()
			}
		}else{
			def customerList = customerService.getCustomerListByUser()
			def taxList = taxService.getAllTaxList();
			def dbItemList = itemService.getAllItemByUser()
			render(view: "create", model: [invoiceInstance : invoiceInstance, taxList : taxList, params : params, customerList : customerList, itemList : dbItemList])
			return
		}

		redirect(action: "invoiceDetails", id : invoiceInstance?.id)
	}


	def edit(Long id) {
		def invoiceInstance = Invoice.read(id)

		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}
		def taxList = taxService.getAllTaxList();
		def customerList = customerService.getCustomerListByUser()
		def dbItemList = itemService.getAllItemByUser()
		def totalCreditAppliedAmount =   invoiceService.getTotalCreditAppliedAmount(invoiceInstance)
		def totalReceiptsAmount = invoiceService.getTotalReceiptsAmount(invoiceInstance)
		if(totalReceiptsAmount > 0){
			flash.infoMessage = "Payment already received for this invoice, you can not edit Invoice. For correction kindly create Credit Note for Invoice."
		}
		[totalCreditAppliedAmount: totalCreditAppliedAmount, totalReceiptsAmount : totalReceiptsAmount, invoiceInstance: invoiceInstance, taxList : taxList, customerList : customerList, itemList : dbItemList ]
	}
	
	def convertToInvoice(){
		if(invoiceService.isInvoiceLimitExceed()){
			redirect(action : "search")
			return
		}
		
		def estimateInstance = Estimate.read(params?.id)
		if (!estimateInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search", controller : "estimate")
			return
		}
		
		def taxList = taxService.getAllTaxList();
		def invoiceNumber = invoiceService.getInvoiceNumber()
		def customerList = customerService.getCustomerListByUser()
		def itemList = itemService.getAllItemByUser()
		
		def invoiceInstance = new Invoice()
		invoiceInstance.invoiceNumber = estimateInstance.estNumber
		invoiceInstance.invoiceDetails = estimateInstance.estimateDetails
		invoiceInstance.taxTransactionDetails = estimateInstance.estimateTaxTransactionDetails
		invoiceInstance.customer = estimateInstance.customer
		invoiceInstance.estimateRefNumber = estimateInstance.estNumber
		invoiceInstance.estimateDate = estimateInstance.estimateDate
		
		render(view : 'create', model : [invoiceInstance: invoiceInstance, taxList : taxList, invoiceNumber : invoiceNumber, customerList : customerList, itemList : itemList, isConvert : true])
	}

	def update(Long id, Long version) {
		def invoiceInstance = Invoice.read(id)
		def generalLedgerInstance = GeneralLedger.findByInvoice(invoiceInstance)
		def customerInstance =  customerService.getCustomerById(params?.customerId)
		
		invoiceInstance.user = baseService.getLoggedInUser()
		invoiceInstance.invoiceNumber = NumberUtils.toInt(params?.invoiceNumber)
		invoiceInstance.customer = customerInstance

		invoiceInstance.subTotal = params?.hiddenSubTotal as BigDecimal
		invoiceInstance.netTotal = params?.hiddenNetTotal as BigDecimal
		invoiceInstance.balanceDue = params?.hiddenNetTotal as BigDecimal

		if(params?.invoiceDate){
			params.invoiceDate = java.util.Date.parse("dd/MM/yyyy", params?.invoiceDate)
		}
		if(params?.dueDate){
			params.dueDate = java.util.Date.parse("dd/MM/yyyy", params?.dueDate)
		}

		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}

		invoiceInstance.invoiceDetails.clear()
		invoiceInstance.taxTransactionDetails.clear()

		if (version != null) {
			if (invoiceInstance.version > version) {
				invoiceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						[
							message(code: 'customer.label', default: 'Customer')] as Object[],
						"Another user has updated this Customer while you were editing")
				def taxList = taxService.getAllTaxList();
				def customerList = customerService.getCustomerListByUser()
				def dbItemList = itemService.getAllItemByUser()
				render(view: "edit", model: [invoiceInstance: invoiceInstance, taxList : taxList, itemList : dbItemList, customerList : customerList])
				return
			}
		}

		invoiceInstance.properties = params

		if (!invoiceInstance.validate(flush: true)) {
			def taxList = taxService.getAllTaxList();
			def customerList = customerService.getCustomerListByUser()
			def dbItemList = itemService.getAllItemByUser()
			render(view: "edit", model: [invoiceInstance: invoiceInstance, taxList : taxList, itemList : dbItemList, customerList : customerList])
			return
		}else{
			invoiceService.saveInvoice(invoiceInstance, params, generalLedgerInstance);
		}

		flash.message = message(code: 'default.updated.message')
		redirect(action: "edit", id: invoiceInstance.id)
	}

	def delete(Long id) {
		def invoiceInstance = Invoice.read(id)
		if (!invoiceInstance) {
			flash.message = message(code: 'default.not.found.message')
			redirect(action: "search")
			return
		}

		try {
			invoiceService.deleteInvoice(invoiceInstance);
			//invoiceInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message')
			redirect(action: "search")
		}catch(BillingException be){
			flash.infoMessage = be.getMessage()
			redirect(action: "search", id: id)
		}catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
			redirect(action: "search", id: id)
		}
	}


	def getItemDetails = {
		def itemInstance = Item.read(params?.id)
		def taxList = taxService.getAllTaxList();
		def itemMap = [:]
		itemInstance?.tax?.eachWithIndex{taxInstance, i->
			i++
			itemMap.put('taxId' + i,taxInstance?.id?:"")
			itemMap.put('taxRate' + i,taxInstance?.taxRate?:0)
		}
		itemMap.put('taxList', taxList)
		itemMap.put('itemRate', itemInstance?.rate)
		itemMap.put('appliedTaxIdList', itemInstance?.tax?:"")
		itemMap.put('elementId', params?.elementId)
		render itemMap as JSON

	}
	

	def getTaxDetails = {
		def taxInstance = Tax.read(params?.id)
		def taxList = taxService.getAllTaxList();
		def taxMap = [:]
		taxMap.put('elementId', params?.elementId)
		taxMap.put('taxList', taxList)
		render taxMap as JSON

	}

	def printInvoice(){
		reportService?.generateInvoice(params)
		return
	}
	
	def invoiceDetails(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def invoiceInstance = Invoice.read(params?.id)
		def receivedInstanceList = receivedPaymentService.getAllReceivedPaymentDetailsListByInvoice(params, invoiceInstance)
		def receivedInstanceListTotal = receivedPaymentService.getAllReceivedPaymentDetailsListSizeByInvoice(params, invoiceInstance)
		def totalCreditAppliedAmount =   invoiceService.getTotalCreditAppliedAmount(invoiceInstance)
		def totalReceiptsAmount = invoiceService.getTotalReceiptsAmount(invoiceInstance)
		def totalRefundedAmount = invoiceService.getRefundedAmount(invoiceInstance)
		if(request.xhr){
			render(template:"receivedPaymentList", model : [totalRefundedAmount : totalRefundedAmount, receivedInstanceList : receivedInstanceList, receivedInstanceListTotal : receivedInstanceListTotal, appliedCreditListTotal : 0, refundInstanceListTotal : 0, invoiceInstance : invoiceInstance, totalCreditAppliedAmount : totalCreditAppliedAmount, totalReceiptsAmount : totalReceiptsAmount])	
		}else{
			[totalRefundedAmount : totalRefundedAmount, invoiceInstance : invoiceInstance ,receivedInstanceList : receivedInstanceList, receivedInstanceListTotal : receivedInstanceListTotal, appliedCreditNoteListTotal : 0, refundInstanceListTotal : 0, totalCreditAppliedAmount : totalCreditAppliedAmount, totalReceiptsAmount : totalReceiptsAmount]
		}
	}
	
	
	def getAppliedCreditListByInvoice(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def invoiceInstance = Invoice.read(params?.id)
		def appliedCreditNoteList = applyCreditService.getAllAppliedCreditDetailsListByInvoice(params, invoiceInstance)
		def appliedCreditNoteListTotal = applyCreditService.getAllAppliedCreditDetailsListSizeByInvoice(params, invoiceInstance)
		render (template : "applyCreditList", model : [appliedCreditNoteList : appliedCreditNoteList, appliedCreditNoteListTotal : appliedCreditNoteListTotal])
	}
	
	def getAllRefundByInvoice(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def invoiceInstance = Invoice.read(params?.id)
		def refundInstanceList = refundService.getAllRefundDetailsListByInvoice(params, invoiceInstance)
		def refundInstanceListTotal = refundService.getAllRefundDetailsListSizeByInvoice(params, invoiceInstance)
		if(request.xhr){
			render(template:"refundList", model : [refundInstanceList : refundInstanceList, refundInstanceListTotal : refundInstanceListTotal, invoiceInstance : invoiceInstance])
		}else{
			[ invoiceInstance : invoiceInstance ,refundInstanceList : refundInstanceList, refundInstanceListTotal : refundInstanceListTotal]
		}
	}
}
