package com.billing.controller

import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Item
import com.billing.domain.Tax

class ItemController {
	
	def itemService
	def baseService
    static allowedMethods = [save: "POST", update: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
         params.max = Math.min(max ?: 10, 100)
		def itemInstanceList = itemService.getItemList(params)
		def itemInstanceListTotal = itemService.getItemListSize(params)
		def measurementList = itemService?.measurementList()
        render(template : 'dataList',  model : [measurementList : measurementList, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal])
    }

    def create(Integer max) {
		//def taxList = itemService.getTaxList()
		params.max = Math.min(max ?: 10, 100)
		def itemInstanceList = itemService.getItemList(params)
		def itemInstanceListTotal = itemService.getItemListSize(params)
		def measurementList = itemService?.measurementList()
		def taxList = itemService.getTaxList()
        [taxList : taxList, itemInstance: new Item(params), measurementList : measurementList, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal]
    }

    def save() {
		println "Params : " + params?.taxName
		def appliedTaxList = []
		if(params?.taxName)
			appliedTaxList = Arrays.asList(params?.taxName);
			
			
        def itemInstance = new Item(params)
		//def taxList = itemService.getTaxList()
		def itemInstanceList = itemService.getItemList(params)
		def itemInstanceListTotal = itemService.getItemListSize(params)
		def measurementList = itemService?.measurementList()
		itemInstance.user =  baseService.getLoggedInUser()
		def taxList = itemService.getTaxList()
		
		
		/*if(appliedTaxList.empty){
			flash.errorMessage = "Please select atleast one tax."
			render(view: "create", model: [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal])
			return
		}*/
		
		if(appliedTaxList?.size() > 3){
			flash.errorMessage = "You can not select more than 3 tax for Item."
			render(view: "create", model: [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal])
			return
		}
		
		appliedTaxList.each{
			itemInstance.addToTax(Tax.read(it))
		}
		
        if (!itemInstance.save(flush: true)) {
            render(view: "create", model: [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal])
            return
        }

        flash.message = message(code: 'default.created.message')
        redirect(action: "create")
    }

    def edit(Long id) {
        def itemInstance = Item.get(id)
		//def taxList = itemService.getTaxList()
		def itemInstanceList = itemService.getItemList(params)
		def itemInstanceListTotal = itemService.getItemListSize(params)
		def measurementList = itemService?.measurementList()
		def taxList = itemService.getTaxList()
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), id])
            redirect(action: "create")
            return
        }

        [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal]
    }

    def update(Long id, Long version) {
        def itemInstance = Item.read(id)
		def appliedTaxList = []
		
		if(params?.taxName)
		appliedTaxList = Arrays.asList(params?.taxName);
	
		
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), id])
            redirect(action: "create")
            return
        }
		
		def taxList = itemService.getTaxList()
		def itemInstanceList = itemService.getItemList(params)
		def itemInstanceListTotal = itemService.getItemListSize(params)
		def measurementList = itemService?.measurementList()
		
		/*if(appliedTaxList.empty){
			flash.errorMessage = "Please select atleast one tax."
			render(view: "edit", model: [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance,  itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal, id: id])
			return
		}*/
		
		if(appliedTaxList?.size() > 3){
			flash.errorMessage = "You can not select more than 3 tax for Item."
			render(view: "edit", model: [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal, id: id])
			return
		}

        if (version != null) {
            if (itemInstance.version > version) {
                itemInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'item.label', default: 'Item')] as Object[],
                          "Another user has updated this Item while you were editing")
                render(view: "edit", model: [taxList : taxList, measurementList : measurementList, itemInstance: itemInstance,  itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal, id: id])
                return
            }
        }

		itemInstance?.tax?.clear()
		appliedTaxList.each{
			itemInstance.addToTax(Tax.read(it))
		}
		
        itemInstance.properties = params

        if (!itemInstance.save(flush: true)) {
			itemInstance.discard()
			itemInstanceList = itemService.getItemList(params)
			itemInstanceListTotal = itemService.getItemListSize(params)
			measurementList = itemService?.measurementList()
            render(view: "edit", model: [measurementList : measurementList, itemInstance: itemInstance, itemInstanceList: itemInstanceList, itemInstanceListTotal: itemInstanceListTotal, id: id])
            return
        }

        flash.message = message(code: 'default.updated.message')
        redirect(action: "create")
    }

    def delete(Long id) {
        def itemInstance = Item.get(id)
        if (!itemInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'item.label', default: 'Item'), id])
            redirect(action: "create")
            return
        }

        try {
			Item.withTransaction {
				itemInstance.delete(flush: true)
			}
            
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'item.label', default: 'Item'), id])
            redirect(action: "create")
        }
        catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "create", id: id)
        }
    }
}
