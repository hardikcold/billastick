package com.billing.controller

import org.apache.shiro.crypto.hash.Sha512Hash
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.springframework.web.multipart.commons.CommonsMultipartFile

import com.billing.domain.User
class ProfileController {

    static allowedMethods = [save: "POST", update: "POST"]
	def userService
	def grailsApplication
	def baseService
    def index() {
        redirect(action: "search", params: params)
    }
	

    def edit(Long id) {
        def userInstance = User.read(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message')
            redirect(action: "search")
            return
        }

        [userInstance: userInstance]
    }

    def update(Long id, Long version) {
		String fileLocation = ServletContextHolder.getServletContext().getRealPath('logos')//grailsApplication.config.logos.location.toString()
        def userInstance = User.read(id)
		CommonsMultipartFile orgLogo = (CommonsMultipartFile)request.getFile('logoFile')
		
		userInstance.logoFileContentType = orgLogo.getContentType()
		def oldFileName = userInstance?.logoFileName
		def oldFileUUID = userInstance?.logoUUID
		if(orgLogo.getOriginalFilename() == null || orgLogo.getOriginalFilename() == ''){
			userInstance.logoFileName = oldFileName
			userInstance.logoUUID = oldFileUUID
		}else{
			userInstance.logoFileName =  orgLogo.getOriginalFilename()
			userInstance.logoUUID = UUID.randomUUID().toString().replaceAll('-','')
		}
		
		int i = orgLogo.getOriginalFilename().lastIndexOf('.');
		if (i > 0) {
			userInstance?.logoFileExtension = orgLogo.getOriginalFilename().substring(i+1);
		}
		
		
		
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'User'), id])
            redirect(action: "search")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'customer.label', default: 'Customer')] as Object[],
                          "Another user has updated this Customer while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

		def dbPassword = userInstance.passwordHash
        userInstance.properties = params

         if (!userInstance.validate()) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }else{
			if(!dbPassword.equals(params.passwordHash)){
				userInstance.passwordHash = new Sha512Hash(params.passwordHash).toHex()
			}
			userInstance.save(flush : true)
			
			if(!orgLogo.empty) {
				new File( fileLocation).mkdirs()
				def uploadedFile = new File(  fileLocation + File.separatorChar + userInstance?.logoFileName)
				orgLogo.transferTo( uploadedFile)
				def newFile = new File(  fileLocation + File.separatorChar + userInstance?.logoUUID + "." + userInstance?.logoFileExtension)
				uploadedFile.renameTo(newFile);
				
			 }
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'customer.label', default: 'User'), userInstance.id])
        redirect(action: "edit", id: userInstance.id)
    }

}
