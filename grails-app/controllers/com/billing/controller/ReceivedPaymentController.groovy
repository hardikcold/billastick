package com.billing.controller

import java.text.NumberFormat;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.CreditNote
import com.billing.domain.Invoice
import com.billing.domain.ReceivedPayment
import com.billing.utility.Commons
import com.billing.utility.InvoiceStatus

class ReceivedPaymentController {

	def taxService
	def invoiceService
	def itemService
	def baseService
	def receivedPaymentService
    def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def receivedInstanceList = receivedPaymentService.getAllReceivedPaymentDetailsList(params)
		def receivedInstanceListTotal = receivedPaymentService.getAllReceivedPaymentDetailsListSize(params)
		if(request.xhr){
			render(template : 'dataList',  model : [receivedInstanceList :receivedInstanceList , receivedInstanceListTotal : receivedInstanceListTotal])
		}else{
			[receivedInstanceList :receivedInstanceList , receivedInstanceListTotal : receivedInstanceListTotal]
		}
	}

	def received() {
		def invoiceInstance = Invoice.read(params?.id)
		def customerCreditAmount = CreditNote.executeQuery("Select sum(balanceDue) from CreditNote cn where cn.customer = ? and cn.creditNoteStatus != ? and cn.creditNoteStatus != ?", [invoiceInstance.customer, InvoiceStatus.Closed, InvoiceStatus.Cancelled]).get(0)
		println "userCreditAmount :" + customerCreditAmount
		[invoiceInstance: invoiceInstance, customerCreditAmount : customerCreditAmount]
	}
	
	def edit(){
		def receivedPaymentInstance = ReceivedPayment.read(params?.id)
		[receivedPaymentInstance : receivedPaymentInstance]
	}
	
	def save(){
		def receivedPaymentInstance = new ReceivedPayment()
		def invoiceInstance = Invoice.read(params?.invoiceId)
		receivedPaymentInstance.invoice = invoiceInstance
		receivedPaymentInstance.user = baseService.getLoggedInUser()
		receivedPaymentInstance.notes = params?.notes
		receivedPaymentInstance.customer = invoiceInstance.customer
		if(params?.receivedAmount){
			receivedPaymentInstance.receivedAmount = params?.receivedAmount as BigDecimal
		}
		if(receivedPaymentInstance.receivedAmount > invoiceInstance.balanceDue){
			flash.errorMessage = "Received Amount should be less than or equals to Due Amount."
			render(view: "received", model: [invoiceInstance : invoiceInstance, receivedPaymentInstance : receivedPaymentInstance])
			return
		}
		receivedPaymentInstance.paymentMethod = params?.paymentMethod
		if(params?.paymentDate){
			receivedPaymentInstance.paymentDate = Date.parse("dd/MM/yy", params?.paymentDate)
		}
		if(receivedPaymentInstance.validate()){
			//invoiceService.saveInvoice(receivedPaymentInstance)
			receivedPaymentService.saveReceivedPayment(receivedPaymentInstance)
		}else{
			render(view: "received", model: [invoiceInstance : invoiceInstance, receivedPaymentInstance : receivedPaymentInstance])
			return
		}

		flash.message = message(code: 'default.created.message')
		redirect(action: "search", controller:"invoice")
	}
	
	def delete(){
		def receivedPaymentInstance =  ReceivedPayment.read(params?.id)
        if (!receivedPaymentInstance) {
            flash.message = message(code: 'default.not.found.message')
            redirect(action: "search", controller :"invoice")
            return
        }

        try {
            //receivedPaymentInstance.delete(flush: true)
			receivedPaymentService.deletePayment(receivedPaymentInstance)
            flash.message = message(code: 'default.deleted.message')
            redirect(action: "search", controller :"invoice")
        }
        catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "search", controller :"invoice")
        }
	}
}
