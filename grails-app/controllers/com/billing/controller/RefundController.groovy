package com.billing.controller

import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.CreditNote
import com.billing.domain.Invoice
import com.billing.domain.ReceivedPayment
import com.billing.domain.Refund

class RefundController {

	def taxService
	def invoiceService
	def itemService
	def baseService
	def refundService
    def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		def refundInstanceList = refundService.getAllRefundDetailsList(params)
		def refundInstanceListTotal = refundService.getAllRefundDetailsListSize(params)
		if(request.xhr){
			render(template : 'dataList',  model : [refundInstanceList :refundInstanceList , refundInstanceListTotal : refundInstanceListTotal])
		}else{
			[refundInstanceList :refundInstanceList , refundInstanceListTotal : refundInstanceListTotal]
		}
	}

	def create() {
		[invoiceInstance: Invoice.read(params?.id), refundInstance : new Refund(params)]
	}
	
	def edit(){
		def refundInstance = Refund.read(params?.id)
		[refundInstance : refundInstance]
	}
	
	def save(){
		def refundInstance = new Refund()
		def invoiceInstance = Invoice.read(params?.invoiceId)
		refundInstance.invoice = invoiceInstance
		refundInstance.user = baseService.getLoggedInUser()
		refundInstance.notes = params?.notes
		if(params?.refundAmount){
			refundInstance.refundAmount = params?.refundAmount as BigDecimal
		}
		
		if(refundInstance.refundAmount > Math.abs(invoiceInstance.totalBalanceDue)){
			flash.errorMessage = "Refund Amount should be less than or equals to Balance."
			render(view: "create", model: [id: invoiceInstance?.id, invoiceInstance : invoiceInstance, refundInstance : refundInstance])
			return
		}
		refundInstance.paymentMethod = params?.paymentMethod
		if(params?.refundDate){
			refundInstance.refundDate = Date.parse("dd/MM/yy", params?.refundDate)
		}
		if(refundInstance.validate()){
			//invoiceService.saveInvoice(receivedPaymentInstance)
			refundService.saveRefund(refundInstance)
		}else{
			render(view: "create", model: [id: invoiceInstance?.id,invoiceInstance : invoiceInstance, refundInstance : refundInstance])
			return
		}

		flash.message = message(code: 'default.created.message')
		redirect(action: "search", controller:'invoice')
	}
	
	def delete(){
		def refundInstance =  Refund.read(params?.id)
        if (!refundInstance) {
            flash.message = message(code: 'default.not.found.message')
            redirect(action: "search", controller:'invoice')
            return
        }

        try {
            //receivedPaymentInstance.delete(flush: true)
			refundService.deleteRefund(refundInstance)
            flash.message = message(code: 'default.deleted.message')
            redirect(action: "search", controller:'invoice')
        }
        catch (DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "search", controller:'invoice')
        }
	}
}
