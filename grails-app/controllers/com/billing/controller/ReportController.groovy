package com.billing.controller

class ReportController {

	def reportService
	def index(){
		if(params?.type.equals("invoice")){
			[templateName : 'reportInvoiceDetailForm', params : params]
		}else if(params?.type.equals("payment")){
			[templateName : 'reportInvoicePaymentTypeDetailForm', params : params]
		}else if(params?.type.equals("invoice")){
			[templateName : 'reportInvoiceDetailsForm', params : params]
		}else{
			[templateName : 'reportInvoiceDetailForm', params : params]
		}
	}
	
	def invoiceReportByType(){
		reportService.generateInvoiceListReportByInvoiceType(params)
	}
	
	def invoiceReportByPaymentType(){
		reportService.generateInvoiceListReportByPaymentType(params)
	}
	
	def renderPage(){
		if(params?.type.equals("invoice")){
		}else if(params?.type.equals("payment")){
		}else if(params?.type.equals("invoice")){
		}
	}
	
	
}
