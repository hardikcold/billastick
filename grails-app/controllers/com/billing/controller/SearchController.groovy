package com.billing.controller

class SearchController {

	def searchService
    def searchInvoice() {
		def invoiceInstanceList = searchService.getInvoiceDetailsList(params)
		def invoiceInstanceListTotal = searchService.getInvoiceDetailsSize(params)
		if(request.xhr){
			render(template:'invoiceDetailsList', model : [invoiceInstanceList : invoiceInstanceList, invoiceInstanceListTotal : invoiceInstanceListTotal, params : params])
		}else{
			[invoiceInstanceList : invoiceInstanceList, invoiceInstanceListTotal : invoiceInstanceListTotal, params : params]
		}
	}
	
	def searchCreditNote() {
		def creditNoteInstanceList = searchService.getCreditNoteDetailsList(params)
		def creditNoteInstanceListTotal = searchService.getCreditNoteDetailsSize(params)
		if(request.xhr){
			render(template:'creditNoteDetailsList', model : [creditNoteInstanceList : creditNoteInstanceList, creditNoteInstanceListTotal : creditNoteInstanceListTotal, params : params])
		}else{
			[creditNoteInstanceList : creditNoteInstanceList, creditNoteInstanceListTotal : creditNoteInstanceListTotal, params : params]
		}
	}
}
