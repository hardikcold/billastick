package com.billing.controller

import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Tax

class TaxController {

	def taxService
	def baseService
	static allowedMethods = [save: "POST", update: "POST"]

	def index() {
		redirect(action: "list", params: params)
	}

	def list(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		def taxInstanceList = taxService.getTaxList(params)
		def taxInstanceListTotal = taxService.getTaxListSize(params)
		render(template : 'dataList',  model : [taxInstanceList: taxInstanceList, taxInstanceListTotal: taxInstanceListTotal])
	}

	def create(Integer max) {
		def taxInstanceList = taxService.getTaxList(params)
		def taxInstanceListTotal = taxService.getTaxListSize(params)
		[taxInstance: new Tax(params), taxInstanceList : taxInstanceList, taxInstanceListTotal: taxInstanceListTotal]
	}

	def save() {
		def taxInstance = new Tax(params)
		taxInstance.user = baseService.getLoggedInUser()
		def taxInstanceList = taxService.getTaxList(params)
		def taxInstanceListTotal = taxService.getTaxListSize(params)
		if (!taxInstance.save(flush: true)) {
			render(view: "create", model: [taxInstance: taxInstance, taxInstanceList : taxInstanceList, taxInstanceListTotal: taxInstanceListTotal])
			return
		}

		flash.message = message(code: 'default.created.message')
		redirect(action: "create")
	}

	def show(Long id) {
		def taxInstance = Tax.get(id)
		if (!taxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
			redirect(action: "list")
			return
		}

		[taxInstance: taxInstance]
	}

	def edit(Long id) {
		def taxInstance = Tax.read(id)
		def taxInstanceList = taxService.getTaxList(params)
		def taxInstanceListTotal = taxService.getTaxListSize(params)
		if (!taxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
			redirect(action: "create")
			return
		}

		[taxInstance: taxInstance, taxInstanceList : taxInstanceList, taxInstanceListTotal : taxInstanceListTotal]
	}

	def update(Long id, Long version) {
		println "Tax.update() : params " + params
		def taxInstanceList = taxService.getTaxList(params)
		def taxInstanceListTotal = taxService.getTaxListSize(params)
		def taxInstance = Tax.read(id)
		if (!taxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
			redirect(action: "create")
			return
		}

		if (version != null) {
			if (taxInstance.version > version) {
				taxInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
						  [message(code: 'tax.label', default: 'Tax')] as Object[],
						  "Another user has updated this Tax while you were editing")
				render(view: "edit", model: [ taxInstance: taxInstance, taxInstanceList : taxInstanceList, taxInstanceListTotal : taxInstanceListTotal])
				return
			}
		}

		taxInstance.properties = params
		/*if(params?.isAdditional.equals("on")){
			println "On...."
			taxInstance.isAdditional = true
		}else{
			println "Off...."
			taxInstance.isAdditional = false
		}*/

		if (!taxInstance.save(flush: true)) {
			taxInstance.discard()
			taxInstanceList = taxService.getTaxList(params)
			taxInstanceListTotal = taxService.getTaxListSize(params)
			render(view: "edit", model: [taxInstance: taxInstance, taxInstanceList : taxInstanceList, taxInstanceListTotal : taxInstanceListTotal])
			return
		}

		flash.message = message(code: 'default.updated.message', args: [message(code: 'tax.label', default: 'Tax'), taxInstance.id])
		redirect(action: "create")
	}

	def delete(Long id) {
		def taxInstance = Tax.get(id)
		if (!taxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
			redirect(action: "create")
			return
		}

		try {
			taxInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'tax.label', default: 'Tax'), id])
			redirect(action: "create")
		}
		catch(DataIntegrityViolationException e) {
			flash.infoMessage = "Record can not be deleted, child reference is exsist."
			redirect(action: "create")
		}
	}
}
