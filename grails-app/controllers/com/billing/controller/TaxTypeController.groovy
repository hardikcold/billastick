package com.billing.controller

import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Tax
import com.billing.domain.TaxType

class TaxTypeController {

	def taxTypeService
	def baseService
	def itemService
    static allowedMethods = [save: "POST", update: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
		def taxTypeInstanceList = taxTypeService.getTaxTypeList(params)
		def taxTypeInstanceListTotal = taxTypeService.getTaxTypeListSize(params)
        render(template : 'dataList',  model : [taxTypeInstanceList: taxTypeInstanceList, taxTypeInstanceListTotal: taxTypeInstanceListTotal])
    }

    def create(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		def taxList = itemService.getTaxList()
		def taxTypeInstanceList = taxTypeService.getTaxTypeList(params)
		def taxTypeInstanceListTotal = taxTypeService.getTaxTypeListSize(params)
        [taxList : taxList, taxTypeInstance: new TaxType(params), taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal: taxTypeInstanceListTotal]
    }

    def save() {
		
		def appliedTaxList = []
		 if(params?.taxName)
			 appliedTaxList = Arrays.asList(params?.taxName);
		def taxTypeInstance = new TaxType(params)
		
		def taxList = itemService.getTaxList()
		taxTypeInstance.user = baseService.getLoggedInUser()
		def taxTypeInstanceList = taxTypeService.getTaxTypeList(params)
		def taxTypeInstanceListTotal = taxTypeService.getTaxTypeListSize(params)
		
		if(appliedTaxList.empty){
			flash.errorMessage = "Please select atleast one tax."
			render(view: "create", model: [taxList : taxList, taxTypeInstance: taxTypeInstance, taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal: taxTypeInstanceListTotal])
			return
		}
		
		appliedTaxList.each{
			 taxTypeInstance.addToTax(Tax.read(it))
		}
		
        if (!taxTypeInstance.save(flush: true)) {
            render(view: "create", model: [taxList : taxList, taxTypeInstance: taxTypeInstance, taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal: taxTypeInstanceListTotal])
            return
        }

        flash.message = message(code: 'default.created.message')
        redirect(action: "create")
    }


    def edit(Long id) {
        def taxTypeInstance = TaxType.read(id)
		def taxList = itemService.getTaxList()
		def taxTypeInstanceList = taxTypeService.getTaxTypeList(params)
		def taxTypeInstanceListTotal = taxTypeService.getTaxTypeListSize(params)
        if (!taxTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
            redirect(action: "create")
            return
        }

        [taxList : taxList, taxTypeInstance: taxTypeInstance, taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal : taxTypeInstanceListTotal]
    }

    def update(Long id, Long version) {
		def taxTypeInstance = TaxType.read(id)
		def appliedTaxList = []
		if(params?.taxName)
		appliedTaxList = Arrays.asList(params?.taxName);

		def taxList = itemService.getTaxList()
		def taxTypeInstanceList = taxTypeService.getTaxTypeList(params)
		def taxTypeInstanceListTotal = taxTypeService.getTaxTypeListSize(params)
		
		if(appliedTaxList.empty){
			flash.errorMessage = "Please select atleast one tax."
			render(view: "create", model: [taxList : taxList, taxTypeInstance: taxTypeInstance, taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal: taxTypeInstanceListTotal])
			return
		}
		
		taxTypeInstance?.tax?.clear()
		appliedTaxList.each{
			taxTypeInstance.addToTax(Tax.read(it))
		}
	   
        if (!taxTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
            redirect(action: "create")
            return
        }

        if (version != null) {
            if (taxTypeInstance.version > version) {
                taxTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'tax.label', default: 'Tax')] as Object[],
                          "Another user has updated this Tax while you were editing")
                render(view: "edit", model: [ taxList :taxList, taxTypeInstance: taxTypeInstance, taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal : taxTypeInstanceListTotal])
                return
            }
        }

        taxTypeInstance.properties = params

        if (!taxTypeInstance.save(flush: true)) {
			taxTypeInstance.discard()
			taxTypeInstanceList = taxTypeService.getTaxTypeList(params)
			taxTypeInstanceListTotal = taxTypeService.getTaxTypeListSize(params)
			taxList = itemService.getTaxList()
            render(view: "edit", model: [taxList : taxList ,taxTypeInstance: taxTypeInstance, taxTypeInstanceList : taxTypeInstanceList, taxTypeInstanceListTotal : taxTypeInstanceListTotal])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'tax.label', default: 'Tax'), taxTypeInstance.id])
        redirect(action: "create")
    }

    def delete(Long id) {
        def taxTypeInstance = TaxType.read(id)
        if (!taxTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tax.label', default: 'Tax'), id])
            redirect(action: "create")
            return
        }

        try {
			TaxType.withTransaction {
				taxTypeInstance.delete(flush: true)
			}
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'tax.label', default: 'Tax'), id])
            redirect(action: "create")
        }
        catch (DataIntegrityViolationException e) {
            flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "create")
        }
    }
}
