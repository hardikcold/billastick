package com.billing.controller

import org.apache.shiro.crypto.hash.Sha512Hash
import org.springframework.dao.DataIntegrityViolationException

import com.billing.domain.Role
import com.billing.domain.User
class UserController {

    static allowedMethods = [save: "POST", update: "POST"]
	def userService
	def baseService
    def index() {
        redirect(action: "search", params: params)
    }
	
	def search(Integer max){
		params.max = Math.min(max ?: 10, 100)
		println "Search User :" + params?.searchText
		def userInstanceList = userService.getUsersList(params)
		def userInstanceListTotal = userService.getUsersListSize(params)
		if(request.xhr)
			render(template : 'dataList',  model : [userInstanceList :userInstanceList , userInstanceListTotal : userInstanceListTotal])
		else	
			[userInstanceList :userInstanceList , userInstanceListTotal : userInstanceListTotal]
	}

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [userInstanceList: User.list(params), userInstanceListTotal: User.count()]
    }

    def create() {
        [userInstance: new User(params)]
    }

    def save() {
        def userInstance = new User(params)
		
		def userRole = Role.findByName("Company")
		userInstance.addToRoles(userRole)
		
        if (!userInstance.validate()) {
            render(view: "create", model: [userInstance: userInstance])
            return
        }else{
			userInstance.passwordHash = new Sha512Hash(userInstance.passwordHash).toHex()
			userService.createUser(userInstance)
        }

        flash.message = message(code: 'default.created.message')
        redirect(action: "search")
    }


    def edit(Long id) {
        def userInstance = User.read(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message')
            redirect(action: "search")
            return
        }

        [userInstance: userInstance]
    }

    def update(Long id, Long version) {
        def userInstance = User.read(id)
		//userInstance.passwordHash = new Sha512Hash(userInstance.passwordHash).toHex()
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'User'), id])
            redirect(action: "search")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'customer.label', default: 'Customer')] as Object[],
                          "Another user has updated this Customer while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

		def dbPassword = userInstance.passwordHash
        userInstance.properties = params

        if (!userInstance.validate()) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }else{
			if(!dbPassword.equals(params.passwordHash)){
				userInstance.passwordHash = new Sha512Hash(params.passwordHash).toHex()
			}
			def userRole = Role.findByName("Company")
			userInstance.addToRoles(userRole)
			userInstance.save(flush:true)
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'customer.label', default: 'User'), userInstance.id])
        redirect(action: "edit", id: userInstance.id)
    }

    def delete(Long id) {
        def userInstance = User.read(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'User'), id])
            redirect(action: "search")
            return
        }

        try {
            userInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'customer.label', default: 'User'), id])
            redirect(action: "search")
        }
        catch (DataIntegrityViolationException e) {
            flash.infoMessage = "Record can not be deleted, child reference is exsist."
            redirect(action: "search", id: id)
        }
    }
}
