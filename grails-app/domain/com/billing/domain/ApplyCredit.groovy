package com.billing.domain

import java.util.Date;

import com.billing.utility.PaymentMethod
import com.billing.utility.ReceivedPaymentStatus;

class ApplyCredit {

	BigDecimal creditAmount
	BigDecimal creditLeft
	Boolean status = true
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [creditNote : CreditNote, user : User, invoice : Invoice]
    static constraints = {
		creditNote nullable : true, blank : true
		user nullable : true, blank : true
		creditAmount nullable : false, blank : false
		invoice nullable : true, blank : true
    }
	
}
