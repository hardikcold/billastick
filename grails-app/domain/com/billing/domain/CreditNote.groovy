package com.billing.domain

import com.billing.utility.InvoiceStatus
import com.billing.utility.InvoiceType;
import com.billing.utility.PaymentType;


class CreditNote {

	Integer creditNoteNumber
	InvoiceStatus creditNoteStatus = InvoiceStatus.Open
	BigDecimal subTotal
	BigDecimal netTotal
	BigDecimal balanceDue = 0.0
	Integer refNumber
	Date lastUpdated
	Date dateCreated
	Date creditNoteDate
	Boolean status = Boolean.TRUE
	String customerNotes
	String termsConditions
	List invoiceDetails
	List taxTransactionDetails
	Boolean isPrinted = Boolean.FALSE
	
    static constraints = {
		creditNoteStatus nullable:true, blank : true
		refNumber nullable:true, blank : true
		creditNoteDate nullable:true, blank : true
		subTotal nullable:true, blank : true
		netTotal nullable:true, blank : true
		creditNoteNumber nullable:true, blank : true
		customerNotes nullable:true, blank : true
		termsConditions nullable:true, blank : true
		invoiceDetails nullable:true, blank : true
		balanceDue nullable:true, blank : true
		invoice nullable:true, blank : true
    }
	
	static belongsTo = [customer : Customer, user : User, invoice : Invoice]
	static hasMany = [invoiceDetails : InvoiceDetail, taxTransactionDetails : TaxTransactionDetails]
	
	static mapping = {
		invoiceDetails lazy: false
		taxTransactionDetails lazy: false
		invoiceDetails cascade: "all-delete-orphan"
		taxTransactionDetails cascade: "all-delete-orphan"
	 }
}
