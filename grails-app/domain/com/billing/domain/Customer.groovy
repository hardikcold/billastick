package com.billing.domain

import com.billing.utility.InvoiceStatus;
import com.billing.utility.PaymentType;
import com.billing.utility.ReceivedPaymentStatus;

class Customer {

	String customerName
	String customerEmail
	String address1
	String address2
	String city
	String state
	String officeNumber
	String mobileNumber
	Date dateCreated
	Date lastUpdated
	String tinNummber
	String panNummber
	String stNummber
	BigDecimal openingBalance
	
	static belongsTo = [user : User]
    static constraints = {
		customerName(nullable : false, blank : false)
		address1(nullable : false)
		city(nullable : false)
		state(nullable : false)
		customerEmail(nullable : true, blank : true)
		address2(nullable : true, blank : true)
		officeNumber(nullable : true, blank : true)
		mobileNumber(nullable : true, blank : true)
		tinNummber(nullable : true, blank : true)
		panNummber(nullable : true, blank : true)
		stNummber(nullable : true, blank : true)
		openingBalance(nullable : true, blank : true)
    }
	
	def getTotalDues(){
		if(id){
			def totalInvoicedAmountList = Invoice.executeQuery("select sum(totalBalanceDue) from Invoice i where i.customer.id = ? and i.invoiceStatus != ? and  i.paymentType = ?", [id, InvoiceStatus.Cancelled, PaymentType.Credit])
			def totalDueAmount =  totalInvoicedAmountList?.get(0) ?: 0.00
			
			return totalDueAmount
			/*def totalCreditNoteAmountList = CreditNote.executeQuery("select sum(netTotal) from CreditNote cn where cn.customer.id = ? and cn.creditNoteStatus != ?", [id, InvoiceStatus.Cancelled])
			def totalCreditNoteAmount =  totalCreditNoteAmountList?.get(0) ?: 0.00
			
			def totalReceivedPaymentAmountList = ReceivedPayment.executeQuery("select sum(receivedAmount) from ReceivedPayment rp where rp.customer.id = ? and rp.receviedPaymentStatus != ? and rp.invoice.paymentType = ?", [id, ReceivedPaymentStatus.Cancelled,  PaymentType.Credit])
			def totalReceivedAmount =  totalReceivedPaymentAmountList?.get(0) ?: 0.00
			
			def refundeddAmountList = Refund.executeQuery("select sum(refundAmount) from Refund r where r.customer.id = ? and r.status = ?", [id, true])
			def totalRefundAmount =  refundeddAmountList?.get(0) ?: 0.00
			
			def totalDues = totalInvoicedAmount - ( (totalCreditNoteAmount + totalReceivedAmount) - totalRefundAmount)
			
			return totalDues*/
			
			/*def totalDuesList = Invoice.executeQuery("select sum(netTotal) from Invoice i where i.customer.id = ? and i.invoiceStatus != ?", [id, InvoiceStatus.Cancelled])
			def totalDueAmount =  totalDuesList?.get(0) ?: 0.00
			return totalDueAmount*/
		}
	}
}
