package com.billing.domain

import java.math.BigDecimal;
import java.util.Date;

import com.billing.utility.InvoiceStatus;
import com.billing.utility.InvoiceType;
import com.billing.utility.PaymentType;


class Estimate {

	Integer estNumber
	InvoiceStatus estimateStatus = InvoiceStatus.Open
	BigDecimal subTotal
	BigDecimal netTotal
	BigDecimal paymentMade = 0.0
	BigDecimal balanceDue = 0.0
	boolean isCashMemo = false
	Date lastUpdated
	Date dateCreated
	Boolean status = Boolean.TRUE
	String customerNotes
	String termsConditions
	List estimateDetails
	List estimateTaxTransactionDetails
	Date estimateDate
	Date expireDate
	
	PaymentType paymentType = PaymentType.Credit
	InvoiceType invoiceType = InvoiceType.Retail
	Boolean isPrinted = Boolean.FALSE
	BigDecimal totalBalanceDue = 0.0
	
	static constraints = {
		estimateStatus nullable:true, blank : true
		estNumber nullable:true, blank : true
		customerNotes nullable:true, blank : true
		termsConditions nullable:true, blank : true
		subTotal nullable:true, blank : true
		netTotal nullable:true, blank : true
		estimateDetails nullable:true, blank : true
		estimateDate nullable:false, blank : false
		expireDate nullable:true, blank : true
	}
	
	static belongsTo = [customer : Customer, user : User]
	static hasMany = [estimateDetails : InvoiceDetail, estimateTaxTransactionDetails : TaxTransactionDetails]
	static mapping = {
		estimateDetails lazy: false
		estimateDetails cascade: "all-delete-orphan"
		
		estimateTaxTransactionDetails lazy: false
		estimateTaxTransactionDetails cascade: "all-delete-orphan"
	 }
}
