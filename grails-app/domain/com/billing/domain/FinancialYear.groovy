package com.billing.domain

import java.text.SimpleDateFormat

import javax.persistence.Transient

import com.billing.utility.InvoiceUtility

class FinancialYear {

	Date startDate
	Date endDate
	FinancialYear parentFinancialYear
	String status = "Current"
	static belongsTo = [user : User]
    static constraints = {
		parentFinancialYear nullable : true, blank : true
    }
	
	@Transient
	def getDisplayFiancialYear(){
		SimpleDateFormat sdf = new SimpleDateFormat(InvoiceUtility.DATE_FORMAT);
		return sdf.format(startDate) + " to " + sdf.format(endDate) 
	}
}
