package com.billing.domain

import com.billing.utility.InvoiceStatus
import com.billing.utility.InvoiceType
import com.billing.utility.PaymentType
import com.billing.utility.ReceivedPaymentStatus


class Invoice {

	Integer invoiceNumber
	Integer estimateRefNumber
	Date estimateDate
	Boolean estimate = false
	InvoiceStatus invoiceStatus = InvoiceStatus.Open
	BigDecimal subTotal
	BigDecimal netTotal
	BigDecimal paymentMade = 0.0
	BigDecimal balanceDue = 0.0
	boolean isCashMemo = false
	Date lastUpdated
	Date dateCreated
	Boolean status = Boolean.TRUE
	String customerNotes
	String termsConditions
	List invoiceDetails
	List taxTransactionDetails
	Date invoiceDate
	Date dueDate
	PaymentType paymentType = PaymentType.Credit
	InvoiceType invoiceType = InvoiceType.Retail
	Boolean isPrinted = Boolean.FALSE
	BigDecimal totalBalanceDue = 0.0
    static constraints = {
		invoiceStatus nullable:true, blank : true
		estimateRefNumber nullable:true, blank : true
		estimateDate nullable:true, blank : true
		subTotal nullable:true, blank : true
		netTotal nullable:true, blank : true
		invoiceNumber nullable:true, blank : true
		customerNotes nullable:true, blank : true
		termsConditions nullable:true, blank : true
		invoiceDetails nullable:true, blank : true
		invoiceDate nullable:false, blank : false
		dueDate nullable:true, blank : true
		balanceDue nullable:true, blank : true
		paymentMade nullable:true, blank : true
    }
	
	static belongsTo = [customer : Customer, user : User]
	static hasMany = [invoiceDetails : InvoiceDetail, taxTransactionDetails : TaxTransactionDetails]
	
	static mapping = {
		invoiceDetails lazy: false
		taxTransactionDetails lazy: false
		invoiceDetails cascade: "all-delete-orphan"
		taxTransactionDetails cascade: "all-delete-orphan"
	 }
	
/*	def getNetBalanceDue(){
		if(id){ 
			def invoiceInstance = Invoice.read(id)
			def receiptsAmount = ReceivedPayment.executeQuery("select sum(receivedAmount) from ReceivedPayment rp where rp.invoice = ? and rp.receviedPaymentStatus != ?", [invoiceInstance, ReceivedPaymentStatus.Cancelled])
			
			def creditAppliedAmount = CreditNote.executeQuery("select sum(netTotal) from CreditNote cn where cn.invoice = ? and cn.creditNoteStatus = ?", [invoiceInstance, InvoiceStatus.Open])
			def totalReceived = (receiptsAmount?.get(0) ?: 0.00) + ( creditAppliedAmount?.get(0) ?: 0.00)
			
			def refundAmount = Refund.executeQuery("select sum(refundAmount) from Refund r where r.invoice = ? and r.status = ?", [invoiceInstance, true])
			def totalRefundAmount = refundAmount?.get(0) ?: 0.00
			
			return netTotal - (totalReceived - totalRefundAmount)
		}
	}
*/	
	def getPaymentDone(){
		if(id){
			def invoiceInstance = Invoice.read(id)
			def receivedPaymentAmount = ReceivedPayment.executeQuery("select sum(receivedAmount) from ReceivedPayment rp where rp.invoice = ? and rp.receviedPaymentStatus != ?", [invoiceInstance, ReceivedPaymentStatus.Cancelled])
			if(receivedPaymentAmount?.get(0)){
				return true
			}else{
				return false
			}
		}
	}
	/*def getUpdatedInvoiceStatus(){
		println "netBalanceDue :" + netBalanceDue
		if(netBalanceDue == 0){
			invoiceStatus = InvoiceStatus.Invoiced
		}else if(netBalanceDue == netTotal){
			invoiceStatus = InvoiceStatus.Open
		}else if(netBalanceDue < netTotal && netBalanceDue > 0){
			invoiceStatus = InvoiceStatus.Overdue
		}else if(netBalanceDue < 0){
			invoiceStatus = InvoiceStatus.Refundable
		}
		
		println "invoiceStatus :" + invoiceStatus 
	}*/
	/*def beforeInsert() {
		if(paymentType == PaymentType.Cash){
			invoiceStatus = InvoiceStatus.Invoiced
			balanceDue = 0.00
		}
	 }
	def beforeUpdate(){
		if(balanceDue > 0){
			invoiceStatus = InvoiceStatus.Overdue
		}else if(balanceDue == 0){
			invoiceStatus = InvoiceStatus.Invoiced
		}
	}*/
}
