package com.billing.domain

import java.util.Date;

class InvoiceDetail {
	BigDecimal rate = 0.0
	BigDecimal quantity = 0.0
	//BigDecimal taxAmount
	BigDecimal discountRate = 0.0
	BigDecimal total = 0.0
	Date lastUpdated
	Date dateCreated
	Boolean status = true
	
	
	
	static belongsTo = [item : Item, taxType : TaxType, estimate : Estimate, invoice : Invoice,  tax1 : Tax, tax2 : Tax, tax3 : Tax]
	
	//static hasMany = [taxTransactionDetails : TaxTransactionDetails]
    static constraints = {
		estimate (nullable : true, blank : true)
		invoice (nullable : true, blank : true)
		rate (nullable : false, blank : false)
		quantity (nullable : false, blank : false)
		taxType (nullable : true, blank : true)
		tax1 (nullable : true, blank : true)
		tax2 (nullable : true, blank : true)
		tax3 (nullable : true, blank : true)
		item (nullable : false, blank : false)
		//taxTransactionDetails(nullable : true, blank : true)
    }
}
