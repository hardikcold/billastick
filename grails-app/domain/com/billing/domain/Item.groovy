package com.billing.domain

import java.util.Date;

class Item {
	String itemName
	BigDecimal rate
	String description
	Measurement measurement
	Date lastUpdated
	Date dateCreated
	Boolean status = true
	List tax
	
	static belongsTo = [user : User]
	static hasMany = [tax : Tax]
	static constraints = {
		description nullable : true, blank : true
		itemName nullable : false, blank : false
		tax nullable : true, blank : true
		rate nullable : false, blank : false
		//rate nullable : false, blank : false
		//tax nullable : true, blank : true
		measurement nullable : false, blank : false
	}
}
