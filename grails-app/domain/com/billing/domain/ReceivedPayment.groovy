package com.billing.domain

import java.util.Date;

import com.billing.utility.PaymentMethod
import com.billing.utility.ReceivedPaymentStatus;

class ReceivedPayment {

	BigDecimal receivedAmount
	BigDecimal bankCharges = 0.0
	Date paymentDate
	PaymentMethod paymentMethod = PaymentMethod.Cash
	ReceivedPaymentStatus receviedPaymentStatus = ReceivedPaymentStatus.Received
	String notes
	Boolean status = true
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [invoice : Invoice, customer : Customer, user : User]
    static constraints = {
		invoice nullable : true, blank : true
		user nullable : true, blank : true
		customer nullable : true, blank : true
		bankCharges nullable : true, blank : true
		receivedAmount nullable : false, blank : false
		notes nullable : true, blank : true
    }
}
