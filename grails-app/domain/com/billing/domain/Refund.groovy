package com.billing.domain

import java.util.Date;

import com.billing.utility.PaymentMethod
import com.billing.utility.ReceivedPaymentStatus;

class Refund {

	BigDecimal refundAmount
	Date refundDate
	PaymentMethod paymentMethod = PaymentMethod.Cash
	String referenceNumber
	String notes
	Boolean status = true
	Date dateCreated
	Date lastUpdated
	
	static belongsTo = [creditNote : CreditNote, customer : Customer, user : User, invoice : Invoice]
    static constraints = {
		creditNote nullable : true, blank : true
		user nullable : true, blank : true
		customer nullable : true, blank : true
		refundAmount nullable : false, blank : false
		refundDate nullable : false, blank : false
		referenceNumber nullable : true, blank : true
		notes nullable : true, blank : true
    }
}
