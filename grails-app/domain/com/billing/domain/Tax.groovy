package com.billing.domain

import java.util.Date;

class Tax {

	String taxName
	BigDecimal taxRate
	//String displayName
	Date dateCreated
	Date lastUpdated
	Tax taxParentId
	Boolean isTaxOnTax = false
	Boolean isAdditional = false
	Boolean status = true
	
	static belongsTo = [user : User]
    static constraints = {
		taxName nullable : false, blank : false
		//displayName nullable : false, blank : false
		taxRate nullable : false, blank : false
		taxParentId nullable : true, blank : true
    }
	
	def getName(){
		return taxName +"@" + taxRate + "%"
	}
	
	def getReportDisplayName(){
		if(taxName.length() > 3){
			return taxName.substring(0, 3) + "[" +taxRate + "]"
		} else{
			return taxName + "[" +taxRate + "]"
		}
	}
}
