package com.billing.domain

import java.util.Date;


class TaxTransactionDetails {
	Tax tax
	BigDecimal taxAmount
	Date lastUpdated
	Date dateCreated
	
	static belongsTo = [invoice : Invoice, estimate : Estimate]
    static constraints = {
		tax(nullable : true, blank: true)
		taxAmount(nullable : true, blank: true)
		invoice (nullable : true, blank : true)
		estimate (nullable : true, blank : true)
    }
}
