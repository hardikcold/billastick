package com.billing.domain

import java.util.Date;

class TaxType {
	String taxTypeName
	String description
	Date lastUpdated
	Date dateCreated
	Boolean status = true
	
	static belongsTo = [user : User]
	static hasMany = [tax : Tax]
	static constraints = {
		description nullable : true, blank : true
		//rate nullable : false, blank : false
		//tax nullable : true, blank : true
		taxTypeName nullable : false, blank : false
	}
}
