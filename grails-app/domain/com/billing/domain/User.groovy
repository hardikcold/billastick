package com.billing.domain

import java.util.Date;

import javax.persistence.Transient;

class User implements Serializable {
    String username
    String passwordHash
	String organizationName
	String firstName
	String lastName
	String address1
	String address2
	String city
	String state
	String officeNumber
	String mobileNumber
	Date dateCreated
	Date lastUpdated
	String tinNummber
	String panNummber
	String stNummber
	String cstNummber
	Date lastLoginDate = new Date()
	byte[] organizationLogo
	
	String logoUUID
	String logoFileContentType
	String logoFileName
	String logoFileExtension
	
	boolean enabled = true
	Date expiredDate
	Integer numberInvoiceAllow = 25
	boolean accountExpired = false
	boolean accountLocked = false
	boolean passwordExpired = false
	
    static hasMany = [ roles: Role, permissions: String ]

    static constraints = {
        username(nullable: false, blank: false, unique: true, email: true)
		passwordHash blank: false
		organizationName blank : false, nullable : false
		firstName blank : false, nullable : false
		lastName blank : false, nullable : false
		mobileNumber blank : false, nullable : false
		officeNumber(nullable:true, blank : true)
		organizationLogo(size:0..2000000)
		address1(nullable:true, blank : true)
		expiredDate(nullable:true, blank : true)
		address2(nullable:true, blank : true)
		city(nullable:true, blank : true)
		state(nullable:true, blank : true)
		organizationLogo(nullable:true, blank : true)
		tinNummber(nullable : true, blank : true)
		panNummber(nullable : true, blank : true)
		stNummber(nullable : true, blank : true)
		cstNummber(nullable : true, blank : true)
		
		logoUUID(nullable: true, blank: true)
		logoFileContentType(nullable: true, blank: true)
		logoFileName(nullable: true, blank: true)
		logoFileExtension(nullable: true, blank: true)
    }
	
	@Transient
	def getDisplayName(){
		return firstName + " " + lastName
	}
	
	@Transient
	def getRemainingInvoiceCount(){
		if(id){
		
			def invoiceCreatedCount = Invoice.executeQuery("select count(*) from Invoice i where i.user.id = ?", [id])
			return numberInvoiceAllow - (invoiceCreatedCount?.get(0) ?: 0)
		}
	}
}
