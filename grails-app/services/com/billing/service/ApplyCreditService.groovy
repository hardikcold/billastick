package com.billing.service

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.ApplyCredit
import com.billing.domain.CreditNote
import com.billing.domain.GeneralLedger
import com.billing.domain.Invoice
import com.billing.domain.ReceivedPayment
import com.billing.exception.BillingException
import com.billing.utility.InvoiceStatus
import com.billing.utility.PaymentType

class ApplyCreditService{

	def baseService
	boolean transactional = true
	def receivedPaymentService
	
	@Transactional(readOnly = true)
	def getAllAppliedCreditDetailsListByInvoice(params, invoiceInstance){
			return CreditNote.findAllByUserAndInvoiceAndCreditNoteStatus(baseService.getLoggedInUser(),  invoiceInstance, InvoiceStatus.Open, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}

	@Transactional(readOnly = true)
	def getAllAppliedCreditDetailsListSizeByInvoice(params, invoiceInstance){
		return CreditNote.findAllByUserAndInvoiceAndCreditNoteStatus(baseService.getLoggedInUser(),  invoiceInstance, InvoiceStatus.Open, [sort: "dateCreated", order: params?.order?:'desc'])?.size() ?: 0
	}
	
	@Transactional(readOnly = true)
	def getAllInvoicedCreditDetailsListByCreditNote(params, creditNoteInstance){
			return ApplyCredit.findAllByUserAndCreditNoteAndStatus(baseService.getLoggedInUser(),  creditNoteInstance, true, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}

	@Transactional(readOnly = true)
	def getAllInvoicedCreditDetailsListSizeByCreditNote(params, creditNoteInstance){
		return ApplyCredit.findAllByUserAndCreditNoteAndStatus(baseService.getLoggedInUser(),  creditNoteInstance, true, [sort: "dateCreated", order: params?.order?:'desc'])?.size() ?: 0
	}
	
	def saveApplyCredit(def invoiceNumberList, def amountList, def creditNoteInstance){
		def applyCreditInstance = null;
		amountList.eachWithIndex{creditAmount, index->
			creditAmount = creditAmount as BigDecimal 
			if(creditAmount > 0){
				println "Amount :" + creditAmount + "Invoice :" + invoiceNumberList.get(index)
				 applyCreditInstance = new ApplyCredit()
				 def invoiceInstance = Invoice.read(invoiceNumberList.get(index))
				 applyCreditInstance.creditAmount = creditAmount as BigDecimal
				 applyCreditInstance.user = baseService.getLoggedInUser()
				 applyCreditInstance.creditLeft = applyCreditInstance.creditAmount
				 //Deduct adjusted amount from credit note balance
				 applyCreditInstance.creditNote = creditNoteInstance
				 applyCreditInstance.creditNote.balanceDue = applyCreditInstance.creditNote.balanceDue - applyCreditInstance.creditAmount
				 if(applyCreditInstance.creditNote?.balanceDue > 0){
					 applyCreditInstance.creditNote?.creditNoteStatus = InvoiceStatus.Overdue
				 }else{
					 applyCreditInstance.creditNote?.creditNoteStatus = InvoiceStatus.Closed
				 }
				 applyCreditInstance.creditNote.save(flush:true)
				 
				 //Deduct adjusted amount from invoice due balance
				 applyCreditInstance.invoice = invoiceInstance
				 applyCreditInstance.invoice.balanceDue = applyCreditInstance.invoice.balanceDue - applyCreditInstance.creditAmount
				 if(applyCreditInstance?.invoice?.balanceDue > 0){
					 applyCreditInstance?.invoice?.invoiceStatus = InvoiceStatus.Overdue
				 }else{
					 applyCreditInstance?.invoice?.invoiceStatus = InvoiceStatus.Closed
				 }
				 applyCreditInstance.invoice.save(flush:true)
				 
				 println "applyCreditInstance  Error :" + applyCreditInstance.validate()
				 applyCreditInstance.errors.allErrors.each{
					 println it
				 }
				 applyCreditInstance.save(flush:true)
				 
			}
		}
	}
	
	
	def saveUseCredit(def creditNoteNumberList, def amountList, def invoiceInstance){
		
		def applyCreditInstance = null;
		ApplyCredit.withTransaction {
			amountList.eachWithIndex{creditAmount, index->
				creditAmount = creditAmount as BigDecimal
				if(creditAmount > 0){
					println "creditNoteNumberList :" + creditNoteNumberList
					println "Amount :" + creditAmount + "Invoice :" + creditNoteNumberList.get(index)
					 applyCreditInstance = new ApplyCredit()
					 def creditNoteInstance = CreditNote.read(creditNoteNumberList.get(index))
					 applyCreditInstance.creditAmount = creditAmount as BigDecimal
					 applyCreditInstance.user = baseService.getLoggedInUser()
					 applyCreditInstance.creditLeft = applyCreditInstance.creditAmount
					 //Deduct adjusted amount from credit note balance
					 applyCreditInstance.creditNote = creditNoteInstance
					 applyCreditInstance.creditNote.balanceDue = applyCreditInstance.creditNote.balanceDue - applyCreditInstance.creditAmount
					 if(applyCreditInstance.creditNote?.balanceDue > 0){
						 applyCreditInstance.creditNote?.creditNoteStatus = InvoiceStatus.Overdue
					 }else{
						 applyCreditInstance.creditNote?.creditNoteStatus = InvoiceStatus.Closed
					 }
					 applyCreditInstance.creditNote.save(flush:true)
					 
					 //Deduct adjusted amount from invoice due balance
					 applyCreditInstance.invoice = invoiceInstance
					 applyCreditInstance.invoice.balanceDue = applyCreditInstance.invoice.balanceDue - applyCreditInstance.creditAmount
					 if(applyCreditInstance?.invoice?.balanceDue > 0){
						 applyCreditInstance?.invoice?.invoiceStatus = InvoiceStatus.Overdue
					 }else{
						 applyCreditInstance?.invoice?.invoiceStatus = InvoiceStatus.Closed
					 }
					 applyCreditInstance.invoice.save(flush:true)
					 
					 println "applyCreditInstance  Error :" + applyCreditInstance.validate()
					 applyCreditInstance.errors.allErrors.each{
						 println it
					 }
					 applyCreditInstance.save(flush:true)
					 
				}
			}
		}
	}
	
	
	def saveInvoice(def invoiceInstance)throws BillingException{
		log.debug "Going to save invoice...."
			if(invoiceInstance?.paymentType == PaymentType.Cash){
				invoiceInstance?.invoiceStatus = InvoiceStatus.Closed
				invoiceInstance?.balanceDue = 0.00
				invoiceInstance.save(flush:true)
				saveToReceivedPayment(new ReceivedPayment(), invoiceInstance)
			}else{
				invoiceInstance.save(flush:true)
			}
			saveToGeneralLedger(new GeneralLedger(), invoiceInstance)
	}
	
	def updateInvoice(def invoiceInstance){
		invoiceInstance.save(flush:true)
		def generalLedgerInstance = GeneralLedger.findByInvoice(invoiceInstance)
		saveToGeneralLedger(generalLedgerInstance, invoiceInstance)
	}
	
	def saveToReceivedPayment(def receivedPaymentInstance, def invoiceInstance){
		receivedPaymentInstance.receivedAmount = invoiceInstance?.netTotal
		receivedPaymentInstance.paymentDate = new Date()
		receivedPaymentInstance.customer = invoiceInstance.customer
		receivedPaymentInstance.user = invoiceInstance.user
		receivedPaymentInstance?.invoice = invoiceInstance
		receivedPaymentInstance.save(flush:true)
	}
	def saveToGeneralLedger(def generalLedgerInstance, def invoiceInstance){
		generalLedgerInstance.paymentType = invoiceInstance.paymentType
		generalLedgerInstance.amount = invoiceInstance?.netTotal
		generalLedgerInstance.invoice = invoiceInstance
		generalLedgerInstance.customer = invoiceInstance?.customer
		generalLedgerInstance.user = baseService.getLoggedInUser()
		generalLedgerInstance.save(flush : true)
	}
	
	def deleteAppliedCredit(def appliedCreditInstance){
		appliedCreditInstance.creditNote.balanceDue = appliedCreditInstance.creditNote.balanceDue + appliedCreditInstance.creditAmount
		appliedCreditInstance.invoice.balanceDue = appliedCreditInstance.invoice.balanceDue + appliedCreditInstance.creditAmount
		appliedCreditInstance.creditNote.save(flush:true)
		appliedCreditInstance.invoice.save(flush:true)
		appliedCreditInstance.status = false
		appliedCreditInstance.save(flush:true)
	}
	
}
