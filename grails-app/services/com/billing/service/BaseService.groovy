package com.billing.service

import org.apache.shiro.SecurityUtils
import org.springframework.transaction.annotation.Transactional

import com.billing.domain.CreditNote
import com.billing.domain.ReceivedPayment
import com.billing.domain.Refund
import com.billing.domain.User
import com.billing.utility.InvoiceStatus
import com.billing.utility.ReceivedPaymentStatus

class BaseService {

	@Transactional(readOnly = true)
	def getLoggedInUser(){
		def userInstance = User.findByUsername(SecurityUtils.subject?.principal)
		return userInstance
	}
	
	def updateInvoiceStautsAndBalanceDue(def invoiceInstance){
		//def invoiceInstance = Invoice.read(id)
		def receiptsAmount = ReceivedPayment.executeQuery("select sum(receivedAmount) from ReceivedPayment rp where rp.invoice = ? and rp.receviedPaymentStatus != ?", [invoiceInstance, ReceivedPaymentStatus.Cancelled])
		
		def creditAppliedAmount = CreditNote.executeQuery("select sum(netTotal) from CreditNote cn where cn.invoice = ? and cn.creditNoteStatus = ?", [invoiceInstance, InvoiceStatus.Open])
		def totalReceived = (receiptsAmount?.get(0) ?: 0.00) + ( creditAppliedAmount?.get(0) ?: 0.00)
		
		def refundAmount = Refund.executeQuery("select sum(refundAmount) from Refund r where r.invoice = ? and r.status = ?", [invoiceInstance, true])
		def totalRefundAmount = refundAmount?.get(0) ?: 0.00
		
		def balanceDue =  invoiceInstance.netTotal - (totalReceived - totalRefundAmount)
		invoiceInstance?.totalBalanceDue = balanceDue
		println "balanceDue :" + balanceDue
		println "Net Balance : " + invoiceInstance?.netTotal
		if(invoiceInstance?.invoiceStatus == InvoiceStatus.Cancelled){
			invoiceInstance?.invoiceStatus = InvoiceStatus.Cancelled
		}else if(balanceDue == 0){
			invoiceInstance?.invoiceStatus = InvoiceStatus.Closed
		}else if(balanceDue == invoiceInstance?.netTotal){
			invoiceInstance?.invoiceStatus = InvoiceStatus.Open
		}else if(balanceDue < invoiceInstance?.netTotal && balanceDue > 0){
			invoiceInstance?.invoiceStatus = InvoiceStatus.Overdue
		}else if(balanceDue < 0){
			invoiceInstance?.invoiceStatus = InvoiceStatus.Refundable
		}
		
		invoiceInstance.save()
	}
}