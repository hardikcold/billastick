package com.billing.service

import org.apache.commons.lang.math.NumberUtils
import org.springframework.transaction.annotation.Transactional

import com.billing.domain.ApplyCredit;
import com.billing.domain.CreditNote
import com.billing.domain.InvoiceDetail
import com.billing.domain.Item
import com.billing.domain.Refund
import com.billing.domain.Tax
import com.billing.domain.TaxTransactionDetails
import com.billing.domain.TaxType
import com.billing.exception.BillingException
import com.billing.utility.InvoiceStatus

class CreditNoteService{

	def baseService
	boolean transactional = true
	def receivedPaymentService
	def invoiceService
	
	@Transactional(readOnly = true)
	def getCreditNoteList(params) {
		if(params?.searchText){
			return CreditNote.findAllByUserAndCreditNoteNumberAndCreditNoteStatusNotEqual(baseService.getLoggedInUser(),params?.searchText, InvoiceStatus.Cancelled,  [max : params?.max, offset : params?.offset, sort: "creditNoteNumber", order: params?.order?:'desc'])
		}else{
			return CreditNote.findAllByUserAndCreditNoteStatusNotEqual(baseService.getLoggedInUser(), InvoiceStatus.Cancelled,[max : params?.max, offset : params?.offset, sort: "creditNoteNumber", order: params?.order?:'desc'])
		}
	}
	
	@Transactional(readOnly = true)
	def getCreditNoteListSize(params){
		if(params?.searchText){
			return CreditNote.findAllByUserAndCreditNoteNumberAndCreditNoteStatusNotEqual(baseService.getLoggedInUser(), params?.searchText, InvoiceStatus.Cancelled)?.size()
		}else{
			return CreditNote.findAllByUserAndCreditNoteStatusNotEqual(baseService.getLoggedInUser(), InvoiceStatus.Cancelled)?.size()
		}
	}
	
	@Transactional(readOnly = true)
	def getCreditNoteNumber(){
		def creditNoteNumber =  CreditNote.executeQuery("select max(creditNoteNumber) from CreditNote cn where cn.user = ?", [baseService.getLoggedInUser()])[0]
		if(!creditNoteNumber){
			creditNoteNumber = 1
			return creditNoteNumber
		}else{
			return creditNoteNumber + 1
		}
	}
	

	
	def saveCreditNote(def creditNoteInstance,def params)throws BillingException{
		//Save Invoice Details
		CreditNote.withTransaction {
			def quantityList = Arrays.asList(params?.quantity)
			def itemList = Arrays.asList(params?.items)
			def rateList = Arrays.asList(params?.rate)
			def discountList = Arrays.asList(params?.discount)
			def tax1List = Arrays.asList(params?.tax1)
			def tax2List = Arrays.asList(params?.tax2)
			def tax3List = Arrays.asList(params?.tax3)
			def totalList = Arrays.asList(params?.total)
			def taxIdList = Arrays.asList(params?.taxId)
			def taxTotalList = Arrays.asList(params?.taxTotal)
			
	
			for(int i = 0; i< quantityList.size(); i++){
				if(NumberUtils.toDouble(totalList.get(i)) > 0){
					def invoiceDetailInstance = new InvoiceDetail()
					def itemInstance = Item.read(itemList?.get(i))
					def taxInstance1 = Tax.read(tax1List?.get(i))
					def taxInstance2 = Tax.read(tax2List?.get(i))
					def taxInstance3 = Tax.read(tax3List?.get(i))
		
					invoiceDetailInstance.item = itemInstance
					invoiceDetailInstance.quantity = NumberUtils.toDouble(quantityList?.get(i))
					invoiceDetailInstance.rate = NumberUtils.toDouble(rateList?.get(i))
					invoiceDetailInstance.discountRate = NumberUtils.toDouble(discountList?.get(i))
					invoiceDetailInstance.total = NumberUtils.toDouble(totalList?.get(i))
					invoiceDetailInstance.tax1 = taxInstance1
					invoiceDetailInstance.tax2 = taxInstance2
					invoiceDetailInstance.tax3 = taxInstance3
					creditNoteInstance.addToInvoiceDetails(invoiceDetailInstance)
				}
	
			}
	
			//Save taxTransactions details
			for(int i = 0; i < taxTotalList.size(); i++ ){
				def taxTransactionDetails = new TaxTransactionDetails()
				taxTransactionDetails.taxAmount =  NumberUtils.toDouble(taxTotalList?.get(i))
				taxTransactionDetails.tax =  Tax.read(taxIdList.get(i))
				creditNoteInstance.addToTaxTransactionDetails(taxTransactionDetails)
	
			}
			/*creditNoteInstance.invoice.balanceDue = creditNoteInstance.invoice.balanceDue - creditNoteInstance.netTotal
			if(creditNoteInstance.invoice.balanceDue < 0){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Refundable
			}else if(creditNoteInstance.invoice.balanceDue == 0){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Closed
			}else if(creditNoteInstance.invoice.balanceDue > 0){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Overdue
			}else if(creditNoteInstance.invoice.balanceDue == creditNoteInstance.invoice.netTotal){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Open
			}
			
			
			creditNoteInstance.invoice.save()*/
			creditNoteInstance.save()
			baseService.updateInvoiceStautsAndBalanceDue(creditNoteInstance.invoice)
		}
			//saveToGeneralLedger(new GeneralLedger(), creditNoteInstance)
	}
	
	def updateCreditNote(def creditNoteInstance){
		CreditNote.withTransaction  {
			creditNoteInstance.save(flush:true)
			baseService.updateInvoiceStautsAndBalanceDue(creditNoteInstance.invoice)
		}
		//def generalLedgerInstance = GeneralLedger.findByInvoice(creditNoteInstance)
		//saveToGeneralLedger(generalLedgerInstance, invoiceInstance)
	}
	
	def deleteCreditNote(def creditNoteInstance){
		CreditNote.withTransaction  {
			/*def refundInstance = Refund.findByCreditNoteAndStatus(creditNoteInstance, true)
			if(refundInstance){
				throw new BillingException("Refund is already made, credit note cannot be deleted.")
			}*/

			/*creditNoteInstance.invoice.balanceDue = creditNoteInstance.invoice.balanceDue + creditNoteInstance.netTotal 
			if(creditNoteInstance.invoice.balanceDue < 0){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Refundable
			}else if(creditNoteInstance.invoice.balanceDue == 0){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Closed
			}else if(creditNoteInstance.invoice.balanceDue > 0){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Overdue
			}else if(creditNoteInstance.invoice.balanceDue == creditNoteInstance.invoice.netTotal){
				creditNoteInstance.invoice.invoiceStatus = InvoiceStatus.Open
			}
			creditNoteInstance.save()*/
			creditNoteInstance.creditNoteStatus = InvoiceStatus.Cancelled
			creditNoteInstance.save()
			baseService.updateInvoiceStautsAndBalanceDue(creditNoteInstance.invoice)
		}
	}
	
	@Transactional(readOnly = true)
	def getCreditNoteListByCustomer(def invoiceInstance){
		def creditNoteList = CreditNote.findAllByCustomerAndUserAndCreditNoteStatusNotEqualAndCreditNoteStatusNotEqual(invoiceInstance?.customer, baseService.getLoggedInUser(), InvoiceStatus.Cancelled, InvoiceStatus.Closed)
		return creditNoteList
	}
	
	@Transactional(readOnly = true)
	def getTotalRefundedAmount(creditNoteInstance){
		def refundAmount = Refund.executeQuery("select sum(refundAmount) from Refund r where r.creditNote = ? and r.status = ?", [creditNoteInstance, true])
		return refundAmount?.get(0) ?: 0.00
	}
	
	@Transactional(readOnly = true)
	def getTotalCreditUsedAmount(creditNoteInstance){
		def creditUsedAmountAmount = ApplyCredit.executeQuery("select sum(creditAmount) from ApplyCredit ac where ac.creditNote = ? and ac.status = ?", [creditNoteInstance, true])
		return creditUsedAmountAmount?.get(0) ?: 0.00
	}
}
