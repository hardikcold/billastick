package com.billing.service

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.CreditNote;
import com.billing.domain.Customer
import com.billing.domain.Invoice;
import com.billing.domain.ReceivedPayment;
import com.billing.utility.InvoiceStatus;
import com.billing.utility.ReceivedPaymentStatus;

class CustomerService {

	def baseService 
	
	@Transactional(readOnly = true)
    def getCustomerList(params) {
		if(params?.searchText){
			println "In Search..."
			return Customer.findAllByUserAndCustomerNameLike(baseService.getLoggedInUser(),"%${params?.searchText}%", [max : params?.max, offset : params?.offset, sort: "customerName", order: params?.order])
		}else{
			return Customer.findAllByUser(baseService.getLoggedInUser(),[max : params?.max, offset : params?.offset, sort: "customerName", order: params?.order])
		}
    }
	
	@Transactional(readOnly = true)
	def getInvoiceListByCustomer(params, customerInstance){
		def invoiceList = Invoice.findAllByUserAndCustomerAndInvoiceStatusNotEqual(baseService.getLoggedInUser(), customerInstance, InvoiceStatus.Cancelled, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
		return invoiceList
	}
	
	@Transactional(readOnly = true)
	def getInvoiceListSizeByCustomer(params, customerInstance){
		def invoiceListSize = Invoice.findAllByUserAndCustomerAndInvoiceStatusNotEqual(baseService.getLoggedInUser(), customerInstance, InvoiceStatus.Cancelled)?.size()
		return invoiceListSize
		
	}
	
	@Transactional(readOnly = true)
	def getCustomerListSize(params){
		if(params?.searchText != null || params?.searchText != ''){
			return Customer.findAllByUserAndCustomerNameLike(baseService.getLoggedInUser(), "%${params?.searchText}%")?.size()
		}else{	
			return Customer.findAllByUser(baseService.getLoggedInUser())?.size()
		}
	}
	
	@Transactional(readOnly = true)
	def getCustomerById(def id){
		return Customer.findById(id)
	}
	
	@Transactional(readOnly = true)
	def getCustomerListByUser(){
		return Customer.findAllByUser(baseService.getLoggedInUser())
	}
	
	@Transactional(readOnly = true)
	def getCreditNoteListByCustomer(params, customerInstance){
			return CreditNote.findAllByUserAndCustomerAndCreditNoteStatusNotEqual(baseService.getLoggedInUser(),  customerInstance, InvoiceStatus.Cancelled, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}

	@Transactional(readOnly = true)
	def getCreditNoteListSizeByCustomer(params, customerInstance){
		return CreditNote.findAllByUserAndCustomerAndCreditNoteStatusNotEqual(baseService.getLoggedInUser(),  customerInstance, InvoiceStatus.Cancelled)?.size() ?: 0
	}
	
	
	@Transactional(readOnly = true)
	def getReceivedPaymentListByCustomer(params, customerInstance){
			return ReceivedPayment.findAllByUserAndCustomerAndReceviedPaymentStatus(baseService.getLoggedInUser(),  customerInstance, ReceivedPaymentStatus.Received, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}
	
	@Transactional(readOnly = true)
	def getReceivedPaymentListSizeByCustomer(params, customerInstance){
			return ReceivedPayment.findAllByUserAndCustomerAndReceviedPaymentStatus(baseService.getLoggedInUser(),  customerInstance, ReceivedPaymentStatus.Received)?.size() ?: 0
	}
	
}
