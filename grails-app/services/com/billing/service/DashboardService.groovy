package com.billing.service

import groovy.time.TimeCategory

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.Invoice
import com.billing.domain.ReceivedPayment
import com.billing.utility.Constants
import com.billing.utility.InvoiceStatus
import com.billing.utility.ReceivedPaymentStatus

class DashboardService {

	def baseService
	def getReceiptListByFrequency(def todayDate, def todayEndDate, def startDate, def resultList, def frequencyType){
		def recordList = new ArrayList()
		if(!Constants.Frequency.YEARLY.equals(frequencyType)){
			startDate = todayDate.toCalendar()
		}
		if(Constants.Frequency.WEEKLY.equals(frequencyType)){
			startDate.add(Calendar.WEEK_OF_MONTH, -1)
		}else if(Constants.Frequency.MONTHLY.equals(frequencyType)){
			startDate.add(Calendar.MONTH, -1)
		}else if(Constants.Frequency.QUARTERLY.equals(frequencyType)){
			startDate.add(Calendar.MONTH, -3)
		}
				
		if(!Constants.Frequency.YEARLY.equals(frequencyType)){
			resultList = resultList.findAll{recordInstance->
				recordInstance.paymentDate >= startDate.getTime() && recordInstance.paymentDate <= todayEndDate
			}
		}
		recordList.add(startDate.getTime())
		recordList.add(todayEndDate)
		recordList.add(resultList?.receivedAmount?.sum()?:0.00)
		return recordList
	}
	
	@Transactional(readOnly = true)
	def getTotalReceiptsByDate(){
		def todayDate = new Date().clearTime()
		def todayEndDate
		use(TimeCategory) {
			todayEndDate = todayDate + 23.hours + 59.minutes + 59.seconds
		}

		def startDate = todayDate.toCalendar() 
		startDate.add(Calendar.YEAR, -1)
		println "StartDate :" + startDate.getTime()  + " End Date :" + todayEndDate
		//def resultList = ReceivedPayment.findAllByPaymentDateGreaterThanEqualsAndPaymentDateLessThanEqualsAndReceviedPaymentStatusAndUser(startDate.getTime(), todayEndDate, ReceivedPaymentStatus.Received, baseService.getLoggedInUser())
		def resultList = ReceivedPayment.createCriteria().list{
			//findAllByInvoiceDateGreaterThanEqualsAndInvoiceDateLessThanEqualsAndInvoiceStatusOrInvoiceStatus(startDate.getTime(), todayDate, InvoiceStatus.Overdue, InvoiceStatus.Open)
			and{
				ge("paymentDate", startDate.getTime())
				le("paymentDate", todayEndDate)
				eq("user", baseService.getLoggedInUser())
				eq("receviedPaymentStatus", ReceivedPaymentStatus.Received)
			}
			
		}
		println "resultList Receipts :" + resultList
		def receiptsMap = [:]
		receiptsMap.put(Constants.Frequency.TODAY, getReceiptListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.TODAY))
		receiptsMap.put(Constants.Frequency.WEEKLY, getReceiptListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.WEEKLY))
		receiptsMap.put(Constants.Frequency.MONTHLY,  getReceiptListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.MONTHLY))
		receiptsMap.put(Constants.Frequency.QUARTERLY, getReceiptListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.QUARTERLY))
		receiptsMap.put(Constants.Frequency.YEARLY, getReceiptListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.YEARLY))
		return receiptsMap
		
	}
	
	def getSalesListByFrequency(def todayDate, def todayEndDate, def startDate, def resultList, def frequencyType){
		def recordList = new ArrayList()
		if(!Constants.Frequency.YEARLY.equals(frequencyType)){
			startDate = todayDate.toCalendar()
		}
		if(Constants.Frequency.WEEKLY.equals(frequencyType)){
			startDate.add(Calendar.WEEK_OF_MONTH, -1)
		}else if(Constants.Frequency.MONTHLY.equals(frequencyType)){
			startDate.add(Calendar.MONTH, -1)
		}else if(Constants.Frequency.QUARTERLY.equals(frequencyType)){
			startDate.add(Calendar.MONTH, -3)
		}
				
		if(!Constants.Frequency.YEARLY.equals(frequencyType)){
			resultList = resultList.findAll{recordInstance->
				recordInstance.invoiceDate >= startDate.getTime() && recordInstance.invoiceDate <= todayEndDate
			}
		}
		recordList.add(startDate.getTime())
		recordList.add(todayEndDate)
		recordList.add(resultList?.netTotal?.sum()?:0.00)
		return recordList
	}
	
	@Transactional(readOnly = true)
	def getTotalSalesByDate(){
		def todayDate = new Date().clearTime()
		def todayEndDate
		use(TimeCategory) {
			todayEndDate = todayDate + 23.hours + 59.minutes + 59.seconds
		}

		def startDate = todayDate.toCalendar()
		startDate.add(Calendar.YEAR, -1)
		def resultList = Invoice.findAllByInvoiceDateGreaterThanEqualsAndInvoiceDateLessThanEqualsAndInvoiceStatusNotEqualAndUser(startDate.getTime(), todayEndDate, InvoiceStatus.Cancelled, baseService.getLoggedInUser())
		def receiptsMap = [:]
		receiptsMap.put(Constants.Frequency.TODAY, getSalesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.TODAY))
		receiptsMap.put(Constants.Frequency.WEEKLY, getSalesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.WEEKLY))
		receiptsMap.put(Constants.Frequency.MONTHLY,  getSalesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.MONTHLY))
		receiptsMap.put(Constants.Frequency.QUARTERLY, getSalesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.QUARTERLY))
		receiptsMap.put(Constants.Frequency.YEARLY, getSalesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.YEARLY))
		return receiptsMap
		
	}
	
	
	def getDuesListByFrequency(def todayDate, def todayEndDate, def startDate, def resultList, def frequencyType){
		def recordList = new ArrayList()
		if(!Constants.Frequency.YEARLY.equals(frequencyType)){
			startDate = todayDate.toCalendar()
		}
		if(Constants.Frequency.WEEKLY.equals(frequencyType)){
			startDate.add(Calendar.WEEK_OF_MONTH, -1)
		}else if(Constants.Frequency.MONTHLY.equals(frequencyType)){
			startDate.add(Calendar.MONTH, -1)
		}else if(Constants.Frequency.QUARTERLY.equals(frequencyType)){
			startDate.add(Calendar.MONTH, -3)
		}
				
		if(!Constants.Frequency.YEARLY.equals(frequencyType)){
			resultList = resultList.findAll{recordInstance->
				recordInstance.invoiceDate >= startDate.getTime() && recordInstance.invoiceDate <= todayEndDate
			}
		}
		recordList.add(startDate.getTime())
		recordList.add(todayEndDate)
		recordList.add(resultList?.totalBalanceDue?.sum()?:0.00)
		return recordList
	}
	
	
	@Transactional(readOnly = true)
	def getTotalDuesByDate(){
		def todayDate = new Date().clearTime()
		def todayEndDate
		use(TimeCategory) {
			todayEndDate = todayDate + 23.hours + 59.minutes + 59.seconds
		}

		def startDate = todayDate.toCalendar()
		startDate.add(Calendar.YEAR, -1)
		def resultList = Invoice.createCriteria().list{
			//findAllByInvoiceDateGreaterThanEqualsAndInvoiceDateLessThanEqualsAndInvoiceStatusOrInvoiceStatus(startDate.getTime(), todayDate, InvoiceStatus.Overdue, InvoiceStatus.Open)
			and{
				ge("invoiceDate", startDate.getTime())
				le("invoiceDate", todayEndDate)
				eq("user", baseService.getLoggedInUser())
			}
			or{
				eq("invoiceStatus", InvoiceStatus.Overdue)
				eq("invoiceStatus", InvoiceStatus.Open)
			}
		}
		def receiptsMap = [:]
		receiptsMap.put(Constants.Frequency.TODAY, getDuesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.TODAY))
		receiptsMap.put(Constants.Frequency.WEEKLY, getDuesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.WEEKLY))
		receiptsMap.put(Constants.Frequency.MONTHLY,  getDuesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.MONTHLY))
		receiptsMap.put(Constants.Frequency.QUARTERLY, getDuesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.QUARTERLY))
		receiptsMap.put(Constants.Frequency.YEARLY, getDuesListByFrequency(todayDate, todayEndDate, startDate, resultList, Constants.Frequency.YEARLY))
		return receiptsMap
		
	}
	
	
}
