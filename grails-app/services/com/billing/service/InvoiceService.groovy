package com.billing.service

import org.apache.commons.lang.math.NumberUtils
import org.springframework.transaction.annotation.Transactional

import com.billing.domain.ApplyCredit
import com.billing.domain.CreditNote;
import com.billing.domain.Estimate
import com.billing.domain.GeneralLedger
import com.billing.domain.Invoice
import com.billing.domain.InvoiceDetail
import com.billing.domain.Item
import com.billing.domain.ReceivedPayment
import com.billing.domain.Refund;
import com.billing.domain.Tax
import com.billing.domain.TaxTransactionDetails
import com.billing.domain.TaxType
import com.billing.exception.BillingException
import com.billing.utility.InvoiceStatus
import com.billing.utility.PaymentType
import com.billing.utility.ReceivedPaymentStatus

class InvoiceService{

	def baseService
	boolean transactional = true
	def receivedPaymentService
	
	@Transactional(readOnly = true)
	def getEstimateList(params) {
		if(params?.searchText){
			return Estimate.findAllByUserAndEstNumber(baseService.getLoggedInUser(),params?.searchText, [max : params?.max, offset : params?.offset, sort: "estNumber", order: params?.order?:'desc'])
		}else{
			//Estimate.findAllByUser(baseService.getLoggedInUser())
			return Estimate.findAllByUser(baseService.getLoggedInUser(),[max : params?.max, offset : params?.offset, sort: "estNumber", order: params?.order?:'desc'])
		}
	}
	
	@Transactional(readOnly = true)
	def getEtimateListSize(params){
		if(params?.searchText){
			return Estimate.findAllByUserAndEstNumber(baseService.getLoggedInUser(), params?.searchText)?.size()
		}else{
			return Estimate.findAllByUser(baseService.getLoggedInUser())?.size()
		}
	}
	
	@Transactional(readOnly = true)
	def getEstNumber(){
		def estNumber =  Estimate.executeQuery("select max(estNumber) from Estimate")[0]
		if(!estNumber){
			estNumber = 1
			return estNumber
		}else{
			return estNumber + 1
		}
	}
	
	
	
	@Transactional(readOnly = true)
	def getInvoiceList(params) {
		if(params?.searchText){
			return Invoice.findAllByUserAndInvoiceNumberAndInvoiceStatusNotEqual(baseService.getLoggedInUser(),params?.searchText, InvoiceStatus.Cancelled,  [max : params?.max, offset : params?.offset, sort: "invoiceNumber", order: params?.order?:'desc'])
		}else{
			return Invoice.findAllByUserAndInvoiceStatusNotEqual(baseService.getLoggedInUser(), InvoiceStatus.Cancelled,[max : params?.max, offset : params?.offset, sort: "invoiceNumber", order: params?.order?:'desc'])
		}
	}
	
	@Transactional(readOnly = true)
	def getInvoiceListSize(params){
		if(params?.searchText){
			return Invoice.findAllByUserAndInvoiceNumberAndInvoiceStatusNotEqual(baseService.getLoggedInUser(), params?.searchText, InvoiceStatus.Cancelled)?.size()
		}else{
			return Invoice.findAllByUserAndInvoiceStatusNotEqual(baseService.getLoggedInUser(), InvoiceStatus.Cancelled)?.size()
		}
	}
	
	@Transactional(readOnly = true)
	def getInvoiceNumber(){
		def invoiceNumber =  Invoice.executeQuery("select max(invoiceNumber) from Invoice i where i.user = ?", [baseService.getLoggedInUser()])[0]
		if(!invoiceNumber){
			invoiceNumber = 1
			return invoiceNumber
		}else{
			return invoiceNumber + 1
		}
	}
	
	@Transactional(readOnly = true)
	def getInvoiceListByCustomer(def creditNoteInstance){
		def invoiceList = Invoice.findAllByCustomerAndUserAndInvoiceStatusNotEqualAndInvoiceStatusNotEqual(creditNoteInstance?.customer, baseService.getLoggedInUser(), InvoiceStatus.Cancelled, InvoiceStatus.Closed)
		return invoiceList
	}
	
	@Transactional(readOnly = true)
	def isInvoiceLimitExceed(){
		def isInvoiceLimitExceed = false
		def userInstance = baseService.getLoggedInUser()
		println "userInstance invoice allow :" + userInstance?.numberInvoiceAllow
		def invoiceInstanceListTotalForInvoiceValidation = Invoice.findAllByUserAndInvoiceStatusNotEqual(baseService.getLoggedInUser(), InvoiceStatus.Cancelled)?.size()
		if(invoiceInstanceListTotalForInvoiceValidation >= userInstance?.numberInvoiceAllow){
			isInvoiceLimitExceed = true
		}
		return isInvoiceLimitExceed
	}
	
	
	
	def saveInvoice(def invoiceInstance, def params, def generalLedgerInstance)throws BillingException{
		Invoice.withTransaction {
		def quantityList = Arrays.asList(params?.quantity)
		def itemList = Arrays.asList(params?.items)
		def rateList = Arrays.asList(params?.rate)
		def discountList = Arrays.asList(params?.discount)
		def tax1List = Arrays.asList(params?.tax1)
		def tax2List = Arrays.asList(params?.tax2)
		def tax3List = Arrays.asList(params?.tax3)
		def totalList = Arrays.asList(params?.total)
		def taxIdList = Arrays.asList(params?.taxId)
		def taxTotalList = Arrays.asList(params?.taxTotal)
		
		//Save Invoice Details
		for(int i = 0; i< quantityList.size(); i++){
			if(NumberUtils.toDouble(totalList.get(i)) > 0){
			def invoiceDetailInstance = new InvoiceDetail()
			def itemInstance = Item.read(itemList?.get(i))
			def taxInstance1 = Tax.read(tax1List?.get(i))
			def taxInstance2 = Tax.read(tax2List?.get(i))
			def taxInstance3 = Tax.read(tax3List?.get(i))

			invoiceDetailInstance.item = itemInstance
			invoiceDetailInstance.quantity = NumberUtils.toDouble(quantityList?.get(i))
			invoiceDetailInstance.rate = NumberUtils.toDouble(rateList?.get(i))
			invoiceDetailInstance.discountRate = NumberUtils.toDouble(discountList?.get(i))
			invoiceDetailInstance.total = NumberUtils.toDouble(totalList?.get(i))
			invoiceDetailInstance.tax1 = taxInstance1
			invoiceDetailInstance.tax2 = taxInstance2
			invoiceDetailInstance.tax3 = taxInstance3
			invoiceInstance.addToInvoiceDetails(invoiceDetailInstance)
			}

		}

		//Save taxTransactions details
		for(int i = 0; i < taxTotalList.size(); i++ ){
			def taxTransactionDetails = new TaxTransactionDetails()
			taxTransactionDetails.taxAmount =  NumberUtils.toDouble(taxTotalList?.get(i))
			taxTransactionDetails.tax =  Tax.read(taxIdList.get(i))
			invoiceInstance.addToTaxTransactionDetails(taxTransactionDetails)

		}
		
		log.debug "Going to save invoice...."
			invoiceInstance.save(flush:true)
			baseService.updateInvoiceStautsAndBalanceDue(invoiceInstance)
			//saveToGeneralLedger(generalLedgerInstance, invoiceInstance)
		}
	}
	
	
	def saveEstimate(def estimateInstance, def params)throws BillingException{
		Estimate.withTransaction {
		def quantityList = Arrays.asList(params?.quantity)
		def itemList = Arrays.asList(params?.items)
		def rateList = Arrays.asList(params?.rate)
		def discountList = Arrays.asList(params?.discount)
		def tax1List = Arrays.asList(params?.tax1)
		def tax2List = Arrays.asList(params?.tax2)
		def tax3List = Arrays.asList(params?.tax3)
		def totalList = Arrays.asList(params?.total)
		def taxIdList = Arrays.asList(params?.taxId)
		def taxTotalList = Arrays.asList(params?.taxTotal)
		
		//Save Invoice Details
		for(int i = 0; i< quantityList.size(); i++){
			if(NumberUtils.toDouble(totalList.get(i)) > 0){
			def invoiceDetailInstance = new InvoiceDetail()
			def itemInstance = Item.read(itemList?.get(i))
			def taxInstance1 = Tax.read(tax1List?.get(i))
			def taxInstance2 = Tax.read(tax2List?.get(i))
			def taxInstance3 = Tax.read(tax3List?.get(i))

			invoiceDetailInstance.item = itemInstance
			invoiceDetailInstance.quantity = NumberUtils.toDouble(quantityList?.get(i))
			invoiceDetailInstance.rate = NumberUtils.toDouble(rateList?.get(i))
			invoiceDetailInstance.discountRate = NumberUtils.toDouble(discountList?.get(i))
			invoiceDetailInstance.total = NumberUtils.toDouble(totalList?.get(i))
			invoiceDetailInstance.tax1 = taxInstance1
			invoiceDetailInstance.tax2 = taxInstance2
			invoiceDetailInstance.tax3 = taxInstance3
			estimateInstance.addToEstimateDetails(invoiceDetailInstance)
			}

		}

		//Save taxTransactions details
		for(int i = 0; i < taxTotalList.size(); i++ ){
			def taxTransactionDetails = new TaxTransactionDetails()
			taxTransactionDetails.taxAmount =  NumberUtils.toDouble(taxTotalList?.get(i))
			taxTransactionDetails.tax =  Tax.read(taxIdList.get(i))
			estimateInstance.addToEstimateTaxTransactionDetails(taxTransactionDetails)

		}
		
		log.debug "Going to save invoice...."
			estimateInstance.save(flush:true)
			//saveToGeneralLedger(generalLedgerInstance, invoiceInstance)
		}
	}
	
	def updateInvoice(def invoiceInstance){
		Invoice.withTransaction {
		invoiceInstance.save(flush:true)
		//def generalLedgerInstance = GeneralLedger.findByInvoice(invoiceInstance)
		//saveToGeneralLedger(generalLedgerInstance, invoiceInstance)
		baseService.updateInvoiceStautsAndBalanceDue(invoiceInstance)
		}
	}
	
	def saveToReceivedPayment(def receivedPaymentInstance, def invoiceInstance){
		receivedPaymentInstance.receivedAmount = invoiceInstance?.netTotal
		receivedPaymentInstance.paymentDate = new Date()
		receivedPaymentInstance.customer = invoiceInstance.customer
		receivedPaymentInstance.user = invoiceInstance.user
		receivedPaymentInstance?.invoice = invoiceInstance
		receivedPaymentInstance.save(flush:true)
	}
	def saveToGeneralLedger(def generalLedgerInstance, def invoiceInstance){
		generalLedgerInstance.paymentType = invoiceInstance.paymentType
		generalLedgerInstance.amount = invoiceInstance?.netTotal
		generalLedgerInstance.invoice = invoiceInstance
		generalLedgerInstance.customer = invoiceInstance?.customer
		generalLedgerInstance.user = baseService.getLoggedInUser()
		generalLedgerInstance.save(flush : true)
	}
	
	def deleteInvoice(def invoiceInstance){
		Invoice.withTransaction  {
			println "Delete Invoice....."
			def receivedPaymentInstance = ReceivedPayment.findByInvoiceAndReceviedPaymentStatus(invoiceInstance, ReceivedPaymentStatus.Received)
			if(receivedPaymentInstance){
				throw new BillingException("Received Payment is already made, invoice cannot be deleted.")
			}
			
			def appliedCreditInstance = CreditNote.findByInvoiceAndCreditNoteStatus(invoiceInstance, InvoiceStatus.Open)
			if(appliedCreditInstance){
				throw new BillingException("Credit Note is already created against invoice, Invoice cannot be deleted.")
			}
			
			invoiceInstance.invoiceStatus = InvoiceStatus.Cancelled
			invoiceInstance.save(flush:true)
			
			//baseService.updateInvoiceStautsAndBalanceDue(invoiceInstance)
		}
	}
	
	
	@Transactional(readOnly = true)
	def getTotalReceiptsAmount(invoiceInstance){
		def receiptsAmount = ReceivedPayment.executeQuery("select sum(receivedAmount) from ReceivedPayment rp where rp.invoice = ? and rp.receviedPaymentStatus != ?", [invoiceInstance, ReceivedPaymentStatus.Cancelled])
		return receiptsAmount?.get(0) ?: 0.00
	}
	
	@Transactional(readOnly = true)
	def getTotalCreditAppliedAmount(invoiceInstance){
		def creditAppliedAmount = CreditNote.executeQuery("select sum(netTotal) from CreditNote cn where cn.invoice = ? and cn.creditNoteStatus = ?", [invoiceInstance, InvoiceStatus.Open])
		return creditAppliedAmount?.get(0) ?: 0.00
	}
	
	@Transactional(readOnly = true)
	def getRefundedAmount(invoiceInstance){
		def refundeddAmount = Refund.executeQuery("select sum(refundAmount) from Refund r where r.invoice = ? and r.status = ?", [invoiceInstance, true])
		return refundeddAmount?.get(0) ?: 0.00
	}
	
	
}
