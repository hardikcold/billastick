package com.billing.service

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.Item
import com.billing.domain.Measurement
import com.billing.domain.Tax
import com.billing.domain.TaxType

class ItemService {

	def baseService
	@Transactional(readOnly = true)
    def getItemList(params) {
		return Item.findAllByUser(baseService.getLoggedInUser(), [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
    }
	@Transactional(readOnly = true)
	def getItemListSize(params){
		return Item.findAllByUser(baseService.getLoggedInUser())?.size()
	}
	@Transactional(readOnly = true)
	def getTaxList(){
		return Tax.findAllByUser(baseService.getLoggedInUser())
	}
	@Transactional(readOnly = true)
	def measurementList(){
		return Measurement.findAllByUser(baseService.getLoggedInUser())
	}
	
	@Transactional(readOnly = true)
	def getTaxTypeListByUser(){
		return TaxType.findAllByUser(baseService.getLoggedInUser())
	}
	
	@Transactional(readOnly = true)
	def getItemById(def id){
		return Item.findById(id)
	}
	
	@Transactional(readOnly = true)
	def getAllItemByUser(){
		return Item.findAllByUser(baseService.getLoggedInUser())
	}
}
