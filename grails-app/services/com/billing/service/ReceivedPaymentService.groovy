package com.billing.service

import groovy.time.TimeCategory;

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.Invoice
import com.billing.domain.ReceivedPayment
import com.billing.utility.Constants;
import com.billing.utility.InvoiceStatus
import com.billing.utility.ReceivedPaymentStatus

class ReceivedPaymentService {

	def baseService
	def getAllInvoiceByBalanceDue(){
		//Invoice.findAllByIsCashMemoAndBalanceDueGreaterThanAndUser(false, 0, baseService.getLoggedInUser())
		def invoiceCriteria = Invoice.createCriteria()
		def invoiceList = invoiceCriteria.list (max: 10, offset: 10) {
			and {
				eq("isCashMemo", false)
				gt("balanceDue", 0)
				eq("user", baseService.getLoggedInUser())
				or{
					eq("invoiceStatus", InvoiceStatus.Open)
					eq("invoiceStatus", InvoiceStatus.Overdue)
				}
				eq("status", Boolean.TRUE)
			}
			//order("holderLastName", "desc")
		}
	}
	
	@Transactional(readOnly = true)
	def getAllReceivedPaymentDetailsList(params){
		def paymentCriteria = ReceivedPayment.createCriteria()
		def resultList = paymentCriteria.list(max:params.max,offset: params.offset?:0) {
			and {
					eq("user",baseService.getLoggedInUser())
					eq("receviedPaymentStatus", ReceivedPaymentStatus.Received)
					if(params?.searchText){
						createAlias("customer", "c")
						like("c.customerName", "%${params?.searchText}%")
					}
					
				}
				order("dateCreated", "${params?.order}")
			}
			
		return resultList
	}
	
	@Transactional(readOnly = true)
	def getAllReceivedPaymentDetailsListSize(params){
		def paymentCriteria = ReceivedPayment.createCriteria()
		def resultListSize = paymentCriteria.count() {
			and {
					eq("user",baseService.getLoggedInUser())
					eq("receviedPaymentStatus", ReceivedPaymentStatus.Received)
					if(params?.searchText){
						createAlias("customer", "c")
						like("c.customerName", "%${params?.searchText}%")
					}
					
				}
				order("dateCreated", "${params?.order}")
			}
		
		return resultListSize?:0
	}
	
	@Transactional(readOnly = true)
	def getAllReceivedPaymentDetailsListByInvoice(params, invoiceInstance){
			return ReceivedPayment.findAllByUserAndReceviedPaymentStatusAndInvoice(baseService.getLoggedInUser(), ReceivedPaymentStatus.Received, invoiceInstance, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}

	@Transactional(readOnly = true)
	def getAllReceivedPaymentDetailsListSizeByInvoice(params, invoiceInstance){
		return ReceivedPayment.findAllByUserAndReceviedPaymentStatusAndInvoice(baseService.getLoggedInUser(), ReceivedPaymentStatus.Received, invoiceInstance, [sort: "dateCreated", order: params?.order?:'desc'])?.size() ?: 0
	}

	def saveReceivedPayment(def receivedPaymentInstance){
		ReceivedPayment.withTransaction {
			receivedPaymentInstance.save(flush:true)
			baseService.updateInvoiceStautsAndBalanceDue(receivedPaymentInstance.invoice)
		}

	}
	
	def deletePayment(def receivedPaymentInstance){
		ReceivedPayment.withTransaction {
			/*receivedPaymentInstance?.invoice?.balanceDue = receivedPaymentInstance?.invoice?.balanceDue  +  receivedPaymentInstance.receivedAmount
			if(receivedPaymentInstance?.invoice?.netTotal == receivedPaymentInstance?.invoice?.balanceDue){
				receivedPaymentInstance?.invoice?.invoiceStatus = InvoiceStatus.Open
			}else if(receivedPaymentInstance?.invoice?.netTotal > receivedPaymentInstance?.invoice?.balanceDue){
				receivedPaymentInstance?.invoice?.invoiceStatus = InvoiceStatus.Overdue
			}else if (receivedPaymentInstance?.invoice?.balanceDue == 0){
				receivedPaymentInstance?.invoice?.invoiceStatus = InvoiceStatus.Closed
			}*/
			receivedPaymentInstance.receviedPaymentStatus = ReceivedPaymentStatus.Cancelled
			receivedPaymentInstance.save(flush:true)
			
			baseService.updateInvoiceStautsAndBalanceDue(receivedPaymentInstance.invoice)
		}
	}
}
