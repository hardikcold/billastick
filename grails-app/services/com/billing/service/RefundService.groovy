package com.billing.service

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.CreditNote
import com.billing.domain.ReceivedPayment
import com.billing.domain.Refund
import com.billing.utility.InvoiceStatus
import com.billing.utility.ReceivedPaymentStatus

class RefundService {

	def baseService
	def getAllInvoiceByBalanceDue(){
		//Invoice.findAllByIsCashMemoAndBalanceDueGreaterThanAndUser(false, 0, baseService.getLoggedInUser())
		def creditNoteCriteria = CreditNote.createCriteria()
		def creditNoteList = creditNoteCriteria.list (max: 10, offset: 10) {
			and {
				eq("isCashMemo", false)
				gt("balanceDue", 0)
				eq("user", baseService.getLoggedInUser())
				or{
					eq("invoiceStatus", InvoiceStatus.Open)
					eq("invoiceStatus", InvoiceStatus.Overdue)
				}
				eq("status", Boolean.TRUE)
			}
			//order("holderLastName", "desc")
		}
	}
	
	@Transactional(readOnly = true)
	def getAllRefundDetailsList(params){
			//return ReceivedPayment.findAllByUserAndCustomerNameLike(baseService.getLoggedInUser(),"%${params?.searchText}%", [max : params?.max, offset : params?.offset, sort: "customerName", order: params?.order])
			return Refund.findAllByUserAndStatus(baseService.getLoggedInUser(), true, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}

	@Transactional(readOnly = true)
	def getAllRefundDetailsListSize(params){
		//return ReceivedPayment.findAllByUserAndCustomerNameLike(baseService.getLoggedInUser(),"%${params?.searchText}%", [max : params?.max, offset : params?.offset, sort: "customerName", order: params?.order])
		return Refund.findAllByUserAndStatus(baseService.getLoggedInUser(), true,[max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])?.size() ?: 0
	}
	
	@Transactional(readOnly = true)
	def getAllRefundDetailsListByInvoice(params, creditNoteInstance){
			return Refund.findAllByUserAndStatusAndInvoice(baseService.getLoggedInUser(), true, creditNoteInstance, [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
	}

	@Transactional(readOnly = true)
	def getAllRefundDetailsListSizeByInvoice(params, creditNoteInstance){
		return Refund.findAllByUserAndStatusAndInvoice(baseService.getLoggedInUser(), true, creditNoteInstance, [sort: "dateCreated", order: params?.order?:'desc'])?.size() ?: 0
	}
	
	def saveRefund(def refundInstance){
		Refund.withTransaction {
			refundInstance.save(flush:true)
			baseService.updateInvoiceStautsAndBalanceDue(refundInstance.invoice)
		}

	}
	
	def deleteRefund(def refundInstance){
		Refund.withTransaction {
			refundInstance.status = false
			refundInstance.save(flush:true)
			baseService.updateInvoiceStautsAndBalanceDue(refundInstance.invoice)
		}
	}
}
