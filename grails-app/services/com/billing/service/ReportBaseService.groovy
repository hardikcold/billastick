package com.billing.service
import javax.servlet.ServletOutputStream

import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource
import net.sf.jasperreports.engine.export.JRCsvExporter
import net.sf.jasperreports.engine.export.JRHtmlExporter
import net.sf.jasperreports.engine.export.JRPdfExporter
import net.sf.jasperreports.engine.export.JRRtfExporter
import net.sf.jasperreports.engine.export.JRXlsExporter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter

import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.codehaus.groovy.grails.web.util.WebUtils

class ReportBaseService {
	
	def propertyService
	def response
	def baseService
	def grailsApplication
	def webUtilsObj
	
	ReportBaseService(){
		
	}
	
	
	/**
	 * this method generate the report based on reportData,reportParamters
	 * 
	 * This methos will thow the exception if reportData,reportParameters and reportName are not 
	 * properly set.
	 * 
	 * 
	 */
	def generateReport(def args){
		log.debug "Generate Report"
		webUtilsObj = WebUtils.retrieveGrailsWebRequest()
		def imagePath = ServletContextHolder.getServletContext().getRealPath('images')
		def subReportPath = ServletContextHolder.getServletContext().getRealPath('reports')
		//def image = ""//imagePath+ File.separator + "middle-line-report.jpg"
		//def propertyLogo = ""//selectedProperty?.logoTransaction?.path
		//args.reportParameters.put("IMAGE", image)
		args.reportParameters.put("SUBREPORT_DIR", subReportPath + File.separator)
		//args.reportParameters.put("DATE_FORMAT", Constants.DATE_FORMAT)
		//args.reportParameters.put("CUR_FORMAT", CurrencyFormatUtils.getCurrencyNumberFormat(false))
		//args.reportParameters.put("CUR_SYMBOL", CurrencyFormatUtils.getCurrencySymbol())
		def ds = new JRMapCollectionDataSource(args.reportData)
		def jasperFilePath = ""
		jasperFilePath = subReportPath + File.separator +  args.reportName + ".jasper"
		def file = new File(jasperFilePath) 
		if (file.exists()) {
			def report
			if(ds != null){
				report = JasperFillManager.fillReport(jasperFilePath,args?.reportParameters,ds)
			}
			fillReport(report:report,reportFormat:args.reportFormat,reportName:args.reportName, writeToDisk: args.writeToDisk, displayName : args.displayName)
		}
	}
	
	
	/*def generateReport(def invoiceDataList, def fileName){
		def utils = WebUtils.retrieveGrailsWebRequest()
		def request = utils.getCurrentRequest()
		def response = utils.getCurrentResponse()
		String path = request.getRealPath(File.separator) + "reports" + File.separator
		println "Path ::::" + path
		JRBeanCollectionDataSource beanColDataSource = 	new JRBeanCollectionDataSource(invoiceDataList);
		//def beanColDataSource = 	new JRMapCollectionDataSource(masterDataList);
		InputStream inputStream = new FileInputStream (path + "invoice.jrxml")
		def jasperDesign = JRXmlLoader.load(inputStream);
		def jasperReport = JasperCompileManager.compileReport(jasperDesign);

		def parameterMap = new HashMap()
		parameterMap.put("SUBREPORT_DIR", path)
		def jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, beanColDataSource);

		def temp_file = File.createTempFile("invoice",".pdf")
		JasperExportManager.exportReportToPdfFile(jasperPrint, temp_file.absolutePath);

		response.setContentType("application/pdf")  //<-- you'll have to handle this dynamically at some point
		response.setHeader("Content-disposition", "attachment;filename=${fileName}")
		response.outputStream << temp_file.newInputStream() //<-- binary stream copy to client
	}*/
	/**
	 * This method fill the report
	 * 
	 *  @param report 
	 */
	
	def fillReport(def args){
		def byteArrayOutputStream = new ByteArrayOutputStream()
		def reportDetails = getReportParamters(args.reportFormat)
		def exporter = reportDetails.exporter 
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, args.report)
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM,byteArrayOutputStream)
		exporter.exportReport()
		if(args.writeToDisk) {
			log.debug "Going to write disk"
			return writeToTempLocation(reportDetails:reportDetails,byteArrayOutputStream:byteArrayOutputStream,reportName:args.reportName, displayName : args.displayName)
		} else {
			writeToResponse(reportDetails:reportDetails,byteArrayOutputStream:byteArrayOutputStream,reportName:args.reportName, displayName : args.displayName)
		}
	}
	
	
	/**
	 * This method write the report to response object
	 * 
	 * @param params map that contains
	 * the reportDetails and byteArrayOutputStream 
	 * 
	 */
	
	def writeToResponse(def params){
		response = webUtilsObj.getCurrentResponse()
		response.setContentType(params.reportDetails.contentType)
		def fileName = params.displayName?:params.reportName
		response.setHeader("Content-Disposition", "attachment;filename=\"${ fileName + params.reportDetails.extension}\"")
		ServletOutputStream outputStream = response.getOutputStream()
		outputStream.write(params.byteArrayOutputStream.toByteArray())
		outputStream.flush()
		outputStream.close()
	}
	
	
	/**
	* Returns a report map object that contains the report extension, report Exporter and mimTye. 
	*
	* @param  Report Format 
	* @return report map object that contains the report extension, report Exporter and mimTye.
	*/
	
	def getReportParamters(def reportType){
		def exporter
		def extension
		def contentType
		if(JasperExportFormat.PDF_FORMAT.equals(reportType)){
			exporter =  new JRPdfExporter()
			extension = ".pdf"
			contentType = "application/pdf"
		}else if(JasperExportFormat.XLS_FORMAT.equals(reportType)){
			exporter = new JRXlsExporter()
			exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,true);
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,Boolean.TRUE);
			extension = ".xls"
			contentType = "application/vnd.ms-excel"
		}else if(JasperExportFormat.CSV_FORMAT.equals(reportType)){
			exporter = new JRCsvExporter()
			extension = ".csv"
			contentType = "text/csv"
		}else if(JasperExportFormat.RTF_FORMAT.equals(reportType)){
			exporter = new JRRtfExporter()
			extension = ".rtf"
			contentType = "application/rtf"
		}else{
			exporter = new JRHtmlExporter()
			extension = ".html"
			contentType = "application/html"
		}
		["exporter":exporter,"extension":extension,"contentType":contentType]
	} 
	
	def writeToTempLocation(def params) {
		//def session = WebUtils.retrieveGrailsWebRequest().getCurrentRequest().getSession().getId().trim()
		def session = System.currentTimeMillis() + ""
		String path = baseService.applicationSetting.file?.upload?.location?.temp+ File.separator + baseService.getLogedInUser().getId() + File.separator +session+File.separator+'INVOICE'+ File.separator
		FileUtils.createFolder(path)
		def fileName = params.displayName?:params.reportName
		def filePath = path.toString() + fileName + params.reportDetails.extension 
		FileOutputStream fos = new FileOutputStream(filePath)
		fos.write(params.byteArrayOutputStream.toByteArray())
		fos.flush()
		fos.close()
		return new File(filePath)
	}
	
}
