package com.billing.service

import java.text.DecimalFormat
import java.text.SimpleDateFormat

import org.codehaus.groovy.grails.plugins.jasper.JasperExportFormat
import org.codehaus.groovy.grails.web.context.ServletContextHolder

import com.billin.dto.InvoiceDetailsDTO
import com.billing.domain.CreditNote
import com.billing.domain.Estimate
import com.billing.domain.Invoice
import com.billing.utility.Constants
import com.billing.utility.InvoiceType
import com.billing.utility.NumberToWords
import com.billing.utility.PaymentType
import com.billing.utility.ReportFormat



class ReportService extends ReportBaseService{

	def taxTypeService
	def invoiceService
	def taxService
	def baseService
	def grailsApplication
	
	def getInvoiceListByInvoiceType(params){
		def invoiceCriteria = Invoice.createCriteria()
		def resultsList = invoiceCriteria.list () {
			and {
				if(params?.fromDate){
					println params.fromDate
					ge("invoiceDate", getDateFromString(params?.fromDate))
				}
				if(params?.toDate){
					le("invoiceDate", getDateFromString(params?.toDate))
				}
				if(params?.invoiceType){
					eq("invoiceType",  params?.invoiceType as InvoiceType)
				}
				eq("user", baseService.getLoggedInUser())
			}
			order("dateCreated", "asc")
		}
		
		return resultsList
	}
	
	
	def getInvoiceListByPaymentType(params){
		println "params?.paymentType :" + params?.paymentType
		def invoiceCriteria = Invoice.createCriteria()
		def resultsList = invoiceCriteria.list () {
			and {
				if(params?.fromDate){
					println params.fromDate
					ge("invoiceDate", getDateFromString(params?.fromDate))
				}
				if(params?.toDate){
					le("invoiceDate", getDateFromString(params?.toDate))
				}
				if(params?.paymentType){
					eq("paymentType",  params?.paymentType as PaymentType)
				}
				eq("user", baseService.getLoggedInUser())
			}
			order("dateCreated", "asc")
		}
		
		return resultsList
	}
	
	
	def static Date getDateFromString(String dateString) {
		if(!dateString)
			return null
			
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT)
		sdf.setLenient(false)
		sdf.parse(dateString)
	}
	
	
	def generateInvoiceListReportByInvoiceType(params){
		def invoiceMap = new HashMap()
		def invoiceDataList = new ArrayList()
		def detailsList = []
		def detailsMap = [:]

		invoiceMap.put("title", "Invoice List")
		def user = baseService.getLoggedInUser()
		if(params?.invoiceType.equals(InvoiceType.Retail.toString())){
			invoiceMap.put("title", "Retail Invoice List")
		}else if(params?.invoiceType.equals(InvoiceType.Tax.toString())){
			invoiceMap.put("title", "Tax Invoice List")
		}
		invoiceMap.put("invoiceNumberColumnTitle", "Invoice #")
		invoiceMap.put("organizationName", user?.organizationName)
		invoiceMap.put("organizationAddress1", user.address1)
		invoiceMap.put("organizationAddress2", user.address2 != null ? "," +user.address2 : null)
		invoiceMap.put("organizationCity", "," + user.city )
		invoiceMap.put("organizationPhone", user.mobileNumber)
		//invoiceMap.put("companyTin", invoiceInstance.user.tinNummber != null ? "TIN # :" + invoiceInstance.user.tinNummber : invoiceInstance.user.tinNummber)
		invoiceMap.put("companyPan", user.tinNummber != null ? "PAN # :" + user.tinNummber : user.tinNummber)
		//invoiceMap.put("companySt", invoiceInstance.user.stNummber != null ? "ST # :" + invoiceInstance.user.stNummber : invoiceInstance.user.stNummber)
		invoiceMap.put("companyCst", user.cstNummber != null ? "CST # :" + user.cstNummber : user.cstNummber)
		
		
		def invoiceList = getInvoiceListByInvoiceType(params)
		invoiceList.each{invoiceInstance->
			detailsMap = new HashMap()
			detailsMap.put("invoiceNumber", invoiceInstance?.invoiceNumber)
			detailsMap.put("invoiceDate", invoiceInstance?.invoiceDate)
			detailsMap.put("customerName", invoiceInstance?.customer?.customerName)
			detailsMap.put("tinNumber", invoiceInstance.customer?.tinNummber ?: "")
			detailsMap.put("subTotal", invoiceInstance?.subTotal)
			
			def taxSummaryList = getTaxSummaryListByInvoiceOrCreditNoteOrEstimate(invoiceInstance, false)
			detailsMap.put("tax", taxSummaryList?.regularTax?.sum() ?: 0.00)
			detailsMap.put("additionalTax", taxSummaryList?.additionalTax?.sum() ?: 0.00)
			detailsMap.put("netTotal", invoiceInstance?.netTotal)
			detailsList.add(detailsMap)
		}
		
		invoiceMap.put("detailsList", detailsList)
		invoiceDataList.add(invoiceMap)
		def reportFormat 
		
		generateReport(reportName : "invoiceDetailsByInvoiceType",reportData : invoiceDataList, reportFormat:getReportFormat(params?.reportFormat), displayName : "${params?.invoiceType}InvoiceDetails", writeToDisk : false, reportParameters : new HashMap())
	}
	
	
	def generateInvoiceListReportByPaymentType(params){
		println "Payment Params :" + params
		def invoiceMap = new HashMap()
		def invoiceDataList = new ArrayList()
		def detailsList = []
		def detailsMap = [:]

		invoiceMap.put("title", "Invoice List")
		def user = baseService.getLoggedInUser()
		if(params?.paymentType.equals(PaymentType.Cash.toString())){
			invoiceMap.put("title", "Cash Invoice List")
		}else if(params?.paymentType.equals(PaymentType.Credit.toString())){
			invoiceMap.put("title", "Credit Invoice List")
		}
		invoiceMap.put("invoiceNumberColumnTitle", "Invoice #")
		invoiceMap.put("organizationName", user?.organizationName)
		invoiceMap.put("organizationAddress1", user.address1)
		invoiceMap.put("organizationAddress2", user.address2 != null ? "," +user.address2 : null)
		invoiceMap.put("organizationCity", "," + user.city )
		invoiceMap.put("organizationPhone", user.mobileNumber)
		//invoiceMap.put("companyTin", invoiceInstance.user.tinNummber != null ? "TIN # :" + invoiceInstance.user.tinNummber : invoiceInstance.user.tinNummber)
		invoiceMap.put("companyPan", user.tinNummber != null ? "PAN # :" + user.tinNummber : user.tinNummber)
		//invoiceMap.put("companySt", invoiceInstance.user.stNummber != null ? "ST # :" + invoiceInstance.user.stNummber : invoiceInstance.user.stNummber)
		invoiceMap.put("companyCst", user.cstNummber != null ? "CST # :" + user.cstNummber : user.cstNummber)
		
		
		def invoiceList = getInvoiceListByPaymentType(params)
		println "invoiceList :::" + invoiceList
		invoiceList.each{invoiceInstance->
			detailsMap = new HashMap()
			detailsMap.put("invoiceNumber", invoiceInstance?.invoiceNumber)
			detailsMap.put("invoiceDate", invoiceInstance?.invoiceDate)
			detailsMap.put("customerName", invoiceInstance?.customer?.customerName)
			detailsMap.put("tinNumber", invoiceInstance.customer?.tinNummber ?: "")
			detailsMap.put("subTotal", invoiceInstance?.subTotal)
			
			def taxSummaryList = getTaxSummaryListByInvoiceOrCreditNoteOrEstimate(invoiceInstance, false)
			detailsMap.put("tax", taxSummaryList?.regularTax?.sum() ?: 0.00)
			detailsMap.put("additionalTax", taxSummaryList?.additionalTax?.sum() ?: 0.00)
			detailsMap.put("netTotal", invoiceInstance?.netTotal)
			detailsList.add(detailsMap)
		}
		
		invoiceMap.put("detailsList", detailsList)
		invoiceDataList.add(invoiceMap)
		def reportFormat
		
		generateReport(reportName : "invoiceDetailsByInvoiceType",reportData : invoiceDataList, reportFormat:getReportFormat(params?.reportFormat), displayName : "${params?.paymentType}InvoiceDetails", writeToDisk : false, reportParameters : new HashMap())
	}
	
	def generateCreditNote(def params){
		def creditNoteInstance = CreditNote.read(params?.id)
		def invoiceMap = new HashMap()
		def invoiceDataList = new ArrayList()
		
		//User Details
		invoiceMap.put("title", "CREDIT NOTE")
		invoiceMap.put("organizationName", creditNoteInstance?.user?.organizationName)
		invoiceMap.put("organizationAddress1", creditNoteInstance.user.address1)
		invoiceMap.put("organizationAddress2", "," +creditNoteInstance.user.address2)
		invoiceMap.put("organizationCity", "," + creditNoteInstance.user.city )
		invoiceMap.put("organizationPhone", creditNoteInstance.user.mobileNumber)
		//invoiceMap.put("companyTin", invoiceInstance.user.tinNummber != null ? "TIN # :" + invoiceInstance.user.tinNummber : invoiceInstance.user.tinNummber)
		invoiceMap.put("companyPan", creditNoteInstance.user.tinNummber != null ? "PAN # :" + creditNoteInstance.user.tinNummber : creditNoteInstance.user.tinNummber)
		//invoiceMap.put("companySt", invoiceInstance.user.stNummber != null ? "ST # :" + invoiceInstance.user.stNummber : invoiceInstance.user.stNummber)
		invoiceMap.put("companyCst", creditNoteInstance.user.cstNummber != null ? "CST # :" + creditNoteInstance.user.cstNummber : creditNoteInstance.user.cstNummber)
		//Customer Details
		invoiceMap.put("customerName", creditNoteInstance.customer.customerName)
		invoiceMap.put("address1", creditNoteInstance.customer.address1)
		//invoiceMap.put("address2", invoiceInstance.customer.address2)
		invoiceMap.put("city", creditNoteInstance.customer.city)
		invoiceMap.put("customerTin", creditNoteInstance.customer.tinNummber != null ? "TIN # :" + creditNoteInstance.customer.tinNummber : creditNoteInstance.customer.tinNummber)
		//invoiceMap.put("customerPan", invoiceInstance.customer.panNummber != null ? "PAN # :" + invoiceInstance.customer.panNummber : invoiceInstance.customer.panNummber)
		invoiceMap.put("customerSt", creditNoteInstance.customer.stNummber != null ? "ST # :" +  creditNoteInstance.customer.stNummber : creditNoteInstance.customer.stNummber)
		
		//Invoice Details
		invoiceMap.put("creditNoteNumber", creditNoteInstance?.creditNoteNumber?.toString())
		invoiceMap.put("invoiceDate", creditNoteInstance?.creditNoteDate)
		invoiceMap.put("invoiceRefNumber", creditNoteInstance?.invoice?.invoiceNumber?.toString())
		invoiceMap.put("amount", creditNoteInstance?.netTotal?.toString())

		List invoiceDetailsList = getDetailsListByInvoiceOrCreditNote(creditNoteInstance)

		//Tax Details for subreport of taxdetails
		def taxDetailsList = getTaxDetailsListByInvoiceOrCreditNote(creditNoteInstance)

		//Tax Summary
		def taxSummaryList = getTaxSummaryListByInvoiceOrCreditNoteOrEstimate(creditNoteInstance, false)
		
		invoiceMap.put("invoiceDetailsList", invoiceDetailsList)
		invoiceMap.put("taxDetailsList", taxDetailsList)
		invoiceMap.put("invoiceTaxSummaryList", taxSummaryList)
		invoiceMap.put("subTotal", creditNoteInstance.subTotal)
		def totalCreditAppliedAmount =   invoiceService.getTotalCreditAppliedAmount(creditNoteInstance)
		def totalReceiptsAmount = invoiceService.getTotalReceiptsAmount(creditNoteInstance)
		
		invoiceMap.put("netTotal", creditNoteInstance.netTotal)
		invoiceMap.put("numberToWord", NumberToWords.getWordFromDigit(creditNoteInstance.balanceDue))
		
		creditNoteInstance.isPrinted = true
		creditNoteInstance.save(flush : true)
		
		invoiceDataList.add(invoiceMap)

		generateReport(reportName : "creditNote",reportData : invoiceDataList, reportFormat:JasperExportFormat.PDF_FORMAT, displayName : "CN_" + creditNoteInstance.creditNoteNumber.toString(), writeToDisk : false, reportParameters : new HashMap())
	}
	
	def generateInvoice(def params){
		def invoiceInstance = Invoice.read(params?.id)
		def invoiceMap = new HashMap()
		def invoiceDataList = new ArrayList()
		
		//User Details
		invoiceMap.put("title", "INVOICE")
		invoiceMap.put("organizationName", invoiceInstance?.user?.organizationName)
		invoiceMap.put("organizationAddress1", invoiceInstance.user.address1)
		invoiceMap.put("organizationAddress2", "," +invoiceInstance.user.address2)
		invoiceMap.put("organizationCity", "," + invoiceInstance.user.city )
		invoiceMap.put("organizationPhone", invoiceInstance.user.mobileNumber)
		
		String fileLocation = ServletContextHolder.getServletContext().getRealPath('logos') //grailsApplication.config.logos.location.toString()
		String logo = fileLocation + File.separatorChar + invoiceInstance?.user?.logoUUID + '.' +   invoiceInstance?.user?.logoFileExtension
		invoiceMap.put("logoLocation", logo?:"")
		
		//invoiceMap.put("companyTin", invoiceInstance.user.tinNummber != null ? "TIN # :" + invoiceInstance.user.tinNummber : invoiceInstance.user.tinNummber)
		invoiceMap.put("companyPan", invoiceInstance.user.tinNummber != null ? "PAN # :" + invoiceInstance.user.tinNummber : invoiceInstance.user.tinNummber)
		//invoiceMap.put("companySt", invoiceInstance.user.stNummber != null ? "ST # :" + invoiceInstance.user.stNummber : invoiceInstance.user.stNummber)
		invoiceMap.put("companyCst", invoiceInstance.user.cstNummber != null ? "CST # :" + invoiceInstance.user.cstNummber : invoiceInstance.user.cstNummber)
		//Customer Details
		invoiceMap.put("customerName", invoiceInstance.customer.customerName)
		invoiceMap.put("address1", invoiceInstance.customer.address1)
		//invoiceMap.put("address2", invoiceInstance.customer.address2)
		invoiceMap.put("city", invoiceInstance.customer.city)
		invoiceMap.put("customerTin", invoiceInstance.customer.tinNummber != null ? "TIN # :" + invoiceInstance.customer.tinNummber : invoiceInstance.customer.tinNummber)
		//invoiceMap.put("customerPan", invoiceInstance.customer.panNummber != null ? "PAN # :" + invoiceInstance.customer.panNummber : invoiceInstance.customer.panNummber)
		invoiceMap.put("customerSt", invoiceInstance.customer.stNummber != null ? "ST # :" +  invoiceInstance.customer.stNummber : invoiceInstance.customer.stNummber)
		
		//Invoice Details
		invoiceMap.put("invoiceNumber", invoiceInstance?.invoiceNumber?.toString())
		if(invoiceInstance?.estimateRefNumber == null || invoiceInstance?.estimateRefNumber.toString().equals("0")){ 
			invoiceMap.put("estimateRefNumber", "-")
		}else{
			invoiceMap.put("estimateRefNumber", invoiceInstance?.estimateRefNumber?.toString())
		}
		invoiceMap.put("invoiceDate", invoiceInstance?.invoiceDate)
		invoiceMap.put("invoiceType", invoiceInstance?.invoiceType?.toString().toUpperCase())
		invoiceMap.put("dueDate", invoiceInstance?.dueDate?:new Date())
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy")
		def estimateDate = null
		if(invoiceInstance?.estimateDate){
			estimateDate = dateFormat.format(invoiceInstance?.estimateDate)
		} 
		invoiceMap.put("estimateDate",  estimateDate ?: "-")
		invoiceMap.put("amount", invoiceInstance?.netTotal?.toString())

		List invoiceDetailsList = getDetailsListByInvoiceOrCreditNote(invoiceInstance)

		//Tax Details for subreport of taxdetails
		def taxDetailsList = getTaxDetailsListByInvoiceOrCreditNote(invoiceInstance)

		//Tax Summary
		def taxSummaryList = getTaxSummaryListByInvoiceOrCreditNoteOrEstimate(invoiceInstance, false)
		
		invoiceMap.put("invoiceDetailsList", invoiceDetailsList)
		invoiceMap.put("taxDetailsList", taxDetailsList)
		invoiceMap.put("invoiceTaxSummaryList", taxSummaryList)
		invoiceMap.put("subTotal", invoiceInstance.subTotal)
		def totalCreditAppliedAmount =   invoiceService.getTotalCreditAppliedAmount(invoiceInstance)
		def totalReceiptsAmount = invoiceService.getTotalReceiptsAmount(invoiceInstance)
		
		invoiceMap.put("netTotal", invoiceInstance.netTotal)
		invoiceMap.put("numberToWord", NumberToWords.getWordFromDigit(invoiceInstance.balanceDue))
		
		invoiceInstance.isPrinted = true
		invoiceInstance.save(flush : true)
		
		invoiceDataList.add(invoiceMap)

		generateReport(reportName : "invoice",reportData : invoiceDataList, reportFormat:JasperExportFormat.PDF_FORMAT, displayName : "Invoice_" + invoiceInstance.invoiceNumber.toString(), writeToDisk : false, reportParameters : new HashMap())
	}
	
	
	def generateEstimate(def params){
		def estimateInstance = Estimate.read(params?.id)
		def invoiceMap = new HashMap()
		def invoiceDataList = new ArrayList()
		
		//User Details
		invoiceMap.put("title", "ESTIMATE")
		invoiceMap.put("organizationName", estimateInstance?.user?.organizationName)
		invoiceMap.put("organizationAddress1", estimateInstance.user.address1)
		invoiceMap.put("organizationAddress2", "," +estimateInstance.user.address2)
		invoiceMap.put("organizationCity", "," + estimateInstance.user.city )
		invoiceMap.put("organizationPhone", estimateInstance.user.mobileNumber)
		invoiceMap.put("companyPan", estimateInstance.user.tinNummber != null ? "PAN # :" + estimateInstance.user.tinNummber : estimateInstance.user.tinNummber)
		invoiceMap.put("companyCst", estimateInstance.user.cstNummber != null ? "CST # :" + estimateInstance.user.cstNummber : estimateInstance.user.cstNummber)
		//Customer Details
		invoiceMap.put("customerName", estimateInstance.customer.customerName)
		invoiceMap.put("address1", estimateInstance.customer.address1)
		invoiceMap.put("city", estimateInstance.customer.city)
		invoiceMap.put("customerTin", estimateInstance.customer.tinNummber != null ? "TIN # :" + estimateInstance.customer.tinNummber : estimateInstance.customer.tinNummber)
		invoiceMap.put("customerSt", estimateInstance.customer.stNummber != null ? "ST # :" +  estimateInstance.customer.stNummber : estimateInstance.customer.stNummber)
		
		//Invoice Details
		invoiceMap.put("invoiceNumber", estimateInstance?.estNumber?.toString())
		invoiceMap.put("invoiceDate", estimateInstance?.estimateDate)
		invoiceMap.put("invoiceType", estimateInstance?.invoiceType?.toString().toUpperCase())
		invoiceMap.put("dueDate", estimateInstance?.expireDate?:new Date())
		invoiceMap.put("amount", estimateInstance?.netTotal?.toString())

		List invoiceDetailsList = getDetailsListByEstimate(estimateInstance)

		//Tax Details for subreport of taxdetails
		def taxDetailsList = getTaxDetailsListByEstimate(estimateInstance)

		//Tax Summary
		def taxSummaryList = getTaxSummaryListByInvoiceOrCreditNoteOrEstimate(estimateInstance, true)
		
		invoiceMap.put("invoiceDetailsList", invoiceDetailsList)
		invoiceMap.put("taxDetailsList", taxDetailsList)
		invoiceMap.put("invoiceTaxSummaryList", taxSummaryList)
		invoiceMap.put("subTotal", estimateInstance.subTotal)
		
		invoiceMap.put("netTotal", estimateInstance.netTotal)
		invoiceMap.put("numberToWord", NumberToWords.getWordFromDigit(estimateInstance.netTotal))
		
		estimateInstance.isPrinted = true
		estimateInstance.save(flush : true)
		
		invoiceDataList.add(invoiceMap)

		generateReport(reportName : "estimate",reportData : invoiceDataList, reportFormat:JasperExportFormat.PDF_FORMAT, displayName : "Estimate_" + estimateInstance.estNumber.toString(), writeToDisk : false, reportParameters : new HashMap())
	}
	
	def getTaxSummaryListByInvoiceOrCreditNoteOrEstimate(def invoiceInstance, def isEstimate){
		DecimalFormat formatNumber = new DecimalFormat(Constants.DECIMAL_FORMAT);
		def taxSummaryList = new ArrayList()
		def taxList = taxService.getAllTaxList();
		
		def appliedTaxList = new ArrayList()
		def invoiceDetailList = (isEstimate == false ? invoiceInstance.invoiceDetails : invoiceInstance.estimateDetails)//InvoiceDetail.findAllByInvoice(invoiceInstance)
		def resultList = new ArrayList()
		//Find appliedTaxList on Items
		invoiceDetailList.eachWithIndex{invoiceDetailsInstance, i->
			def itemisedTaxList = new ArrayList()
			//def resultMap = new HashMap()
			def invoiceDetailsDTO = new InvoiceDetailsDTO()
			if(invoiceDetailsInstance?.tax1){
				itemisedTaxList.add(invoiceDetailsInstance?.tax1)
			}
			if(invoiceDetailsInstance?.tax2){
				itemisedTaxList.add(invoiceDetailsInstance?.tax2)
			}
			if(invoiceDetailsInstance?.tax3){
				itemisedTaxList.add(invoiceDetailsInstance?.tax3)
			}
			appliedTaxList.add(itemisedTaxList)
			String mapKey = ""
			itemisedTaxList.sort{it.id}
			invoiceDetailsDTO.taxList = itemisedTaxList
			invoiceDetailsDTO.invoiceDetailsInstance = invoiceDetailsInstance
			resultList.add(invoiceDetailsDTO)
		}
		resultList = resultList.groupBy{it.taxList}
		def taxSummaryMap = new HashMap()
		
		resultList.each{invoiceDetailsDTOInstance->
				def basicAmount = invoiceDetailsDTOInstance?.value?.invoiceDetailsInstance?.total?.sum()
				taxSummaryMap.put("basicAmount", basicAmount)
				def subTaxSummaryMap = new HashMap()
				if(basicAmount > 0){
					subTaxSummaryMap = new HashMap()
					subTaxSummaryMap.put("basicAmount", basicAmount)
					String taxName = ""
					def itemisedTaxList = invoiceDetailsDTOInstance.key
					//If there is tax assigned to item than add tax summary else add as other item to taxSummaryList
					if(invoiceDetailsDTOInstance.key){
						itemisedTaxList.eachWithIndex{taxInstance, index->
							index = index + 1
							if(itemisedTaxList.size() != index){
								taxName += taxInstance.reportDisplayName + ","
							}else{
								taxName += taxInstance.reportDisplayName
							}
							subTaxSummaryMap.put("taxName", taxName)
							if(taxInstance?.isAdditional){
								if(taxInstance?.taxRate && basicAmount){
									def addTaxAmount = formatNumber.format((basicAmount * taxInstance?.taxRate)/100) as BigDecimal
									addTaxAmount = (subTaxSummaryMap.get("additionalTax") ?: 0) + addTaxAmount
									subTaxSummaryMap.put("additionalTax", addTaxAmount)
								}
							}else{
								if(taxInstance?.taxRate && basicAmount){
									def regularTaxAmount = formatNumber.format((basicAmount * taxInstance?.taxRate)/100) as BigDecimal
									regularTaxAmount = (subTaxSummaryMap.get("regularTax") ?: 0) + regularTaxAmount 
									subTaxSummaryMap.put("regularTax", regularTaxAmount)
								}
							}
							if(subTaxSummaryMap.get("additionalTax") == null){
								subTaxSummaryMap.put("additionalTax", 0.00)
							}
							if(subTaxSummaryMap.get("basicAmount") == null){
								subTaxSummaryMap.put("basicAmount", 0.00)
							}
							if(subTaxSummaryMap.get("regularTax") == null){
								subTaxSummaryMap.put("regularTax", 0.00)
							}
						}
					}else{
						subTaxSummaryMap.put("taxName", "Other Item")
						subTaxSummaryMap.put("regularTax", 0.00)
						subTaxSummaryMap.put("additionalTax", 0.00)
					}
					taxSummaryList.add(subTaxSummaryMap)
				}
		}
		
		return taxSummaryList
	}

	def getTaxDetailsListByInvoiceOrCreditNote(def invoiceInstance){
		DecimalFormat formatNumber = new DecimalFormat(Constants.DECIMAL_FORMAT);
		def taxDetailsMap = [:]
		def taxDetailsList = new ArrayList()
		invoiceInstance.taxTransactionDetails.each{taxTransactionDetailsInstance->
			taxDetailsMap = new HashMap();
			if(taxTransactionDetailsInstance?.taxAmount > 0){
				taxDetailsMap.put("taxAmount", formatNumber.format(taxTransactionDetailsInstance?.taxAmount) as BigDecimal)
				taxDetailsMap.put("taxName", taxTransactionDetailsInstance?.tax?.taxName + "[" + taxTransactionDetailsInstance?.tax?.taxRate+"]")
				taxDetailsList.add(taxDetailsMap)
			}
		}
		return taxDetailsList
	}
	
	def getTaxDetailsListByEstimate(def estimateInstance){
		DecimalFormat formatNumber = new DecimalFormat(Constants.DECIMAL_FORMAT);
		def taxDetailsMap = [:]
		def taxDetailsList = new ArrayList()
		estimateInstance.estimateTaxTransactionDetails.each{taxTransactionDetailsInstance->
			taxDetailsMap = new HashMap();
			if(taxTransactionDetailsInstance?.taxAmount > 0){
				taxDetailsMap.put("taxAmount", formatNumber.format(taxTransactionDetailsInstance?.taxAmount) as BigDecimal)
				taxDetailsMap.put("taxName", taxTransactionDetailsInstance?.tax?.taxName + "[" + taxTransactionDetailsInstance?.tax?.taxRate+"]")
				taxDetailsList.add(taxDetailsMap)
			}
		}
		return taxDetailsList
	}
	
	def getDetailsListByEstimate(def estimateInstance){
		List invoiceDetailsList = new ArrayList();
		Map invoiceDetailsMap = null;

		//Item Details for subreport of invoice details
		estimateInstance.estimateDetails.each{detailInstance->
			invoiceDetailsMap = new HashMap();
			invoiceDetailsMap.put("itemName", detailInstance.item.itemName);
			invoiceDetailsMap.put("itemUnit", detailInstance.item.measurement.measurementCode);
			invoiceDetailsMap.put("itemPrice", detailInstance.rate);
			invoiceDetailsMap.put("itemQuantity", detailInstance.quantity);
			invoiceDetailsMap.put("itemDiscount", detailInstance.discountRate);
			invoiceDetailsMap.put("total", detailInstance.total);
			invoiceDetailsList.add(invoiceDetailsMap);
		}
		invoiceDetailsMap = new HashMap();
		invoiceDetailsList.add(invoiceDetailsMap);
		return invoiceDetailsList
	}
	
	def getDetailsListByInvoiceOrCreditNote(def invoiceInstance){
		List invoiceDetailsList = new ArrayList();
		Map invoiceDetailsMap = null;

		//Item Details for subreport of invoice details
		invoiceInstance.invoiceDetails.each{detailInstance->
			invoiceDetailsMap = new HashMap();
			invoiceDetailsMap.put("itemName", detailInstance.item.itemName);
			invoiceDetailsMap.put("itemUnit", detailInstance.item.measurement.measurementCode);
			invoiceDetailsMap.put("itemPrice", detailInstance.rate);
			invoiceDetailsMap.put("itemQuantity", detailInstance.quantity);
			invoiceDetailsMap.put("itemDiscount", detailInstance.discountRate);
			invoiceDetailsMap.put("total", detailInstance.total);
			invoiceDetailsList.add(invoiceDetailsMap);
		}
		invoiceDetailsMap = new HashMap();
		invoiceDetailsList.add(invoiceDetailsMap);
		return invoiceDetailsList
	}
	
	def getReportFormat(def reportFormat){
		reportFormat = reportFormat as ReportFormat
		if(reportFormat.equals(ReportFormat.Pdf)){
			return JasperExportFormat.PDF_FORMAT
		}else if(reportFormat.equals(ReportFormat.Excel)){
			return JasperExportFormat.XLS_FORMAT
		}
	}
}

