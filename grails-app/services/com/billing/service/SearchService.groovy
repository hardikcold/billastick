package com.billing.service

import java.text.SimpleDateFormat

import org.apache.commons.lang.StringUtils;

import com.billing.domain.CreditNote;
import com.billing.domain.Invoice
import com.billing.utility.Constants
import com.billing.utility.InvoiceStatus

class SearchService {

	def baseService
    def getInvoiceDetailsList(params){
		def invoiceCriteria = Invoice.createCriteria()
		def resultsList = invoiceCriteria.list (max: params?.max?:10, offset: params?.offset?:0) {
			and {
				println "From :" + params?.fromDate
				if(params?.fromDate &&  params?.fromDate != "null"){
					ge("invoiceDate", getDateFromString(params?.fromDate))
				}
				if(params?.toDate &&  params?.toDate != "null"){
					println "To From :" + params.fromDate.class
					le("invoiceDate", getDateFromString(params?.toDate))
				}
				if(params?.invoiceStatus &&  params?.invoiceStatus != "null"){
					eq("invoiceStatus",  params?.invoiceStatus as InvoiceStatus)
				}
				eq("user", baseService.getLoggedInUser())
			}
			order("invoiceDate", params?.order?:"desc")
		}
		return resultsList
	}
	
	def getInvoiceDetailsSize(params){
		def invoiceCriteria = Invoice.createCriteria()
		def resultsSize = invoiceCriteria.count() {
			and {
				if(params?.fromDate &&  params?.fromDate != "null"){
					ge("invoiceDate", getDateFromString(params?.fromDate))
				}
				if(params?.toDate &&  params?.toDate != "null"){
					le("invoiceDate", getDateFromString(params?.toDate))
				}
				if(params?.invoiceStatus &&  params?.invoiceStatus != "null"){
					eq("invoiceStatus", params?.invoiceStatus as InvoiceStatus)
				}
				eq("user", baseService.getLoggedInUser())
			}
			order("invoiceDate", params?.order?:"desc")
		}
		return resultsSize
	}
	
	
	def getCreditNoteDetailsList(params){
		def creditNoteCriteria = CreditNote.createCriteria()
		def resultsList = creditNoteCriteria.list (max: params?.max?:10, offset: params?.offset?:0) {
			and {
				println "From :" + params?.fromDate
				if(params?.fromDate &&  params?.fromDate != "null"){
					ge("creditNoteDate", getDateFromString(params?.fromDate))
				}
				if(params?.toDate &&  params?.toDate != "null"){
					println "To From :" + params.fromDate.class
					le("creditNoteDate", getDateFromString(params?.toDate))
				}
				eq("user", baseService.getLoggedInUser())
			}
			order("creditNoteDate", params?.order?:"desc")
		}
		return resultsList
	}
	
	def getCreditNoteDetailsSize(params){
		def creditNoteCriteria = CreditNote.createCriteria()
		def resultsSize = creditNoteCriteria.count() {
			and {
				if(params?.fromDate &&  params?.fromDate != "null"){
					ge("creditNoteDate", getDateFromString(params?.fromDate))
				}
				if(params?.toDate &&  params?.toDate != "null"){
					le("creditNoteDate", getDateFromString(params?.toDate))
				}
				eq("user", baseService.getLoggedInUser())
			}
			order("creditNoteDate", params?.order?:"desc")
		}
		return resultsSize
	}
	
	
	def static Date getDateFromString(String dateString) {
		if(!dateString)
			return null
			
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT)
		sdf.setLenient(false)
		sdf.parse(dateString)
	}
	
}
