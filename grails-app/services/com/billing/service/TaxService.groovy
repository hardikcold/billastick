package com.billing.service

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.FinancialYear
import com.billing.domain.Tax
import com.billing.domain.TaxType

class TaxService {

	def baseService
	
	@Transactional(readOnly = true)
    def getTaxList(params) {
		return Tax.findAllByUser(baseService?.getLoggedInUser(), [max : params?.max, offset : params?.offset, sort: "taxName", order: params?.order])
    }
	
	@Transactional(readOnly = true)
	def getAllTaxList() {
		return Tax.findAllByUser(baseService?.getLoggedInUser())
	}
	
	@Transactional(readOnly = true)
	def getTaxListSize(params){
		return Tax.findAllByUser(baseService?.getLoggedInUser())?.size()
	}
	
	@Transactional(readOnly = true)
	def getFinanacialYearList(){
		return FinancialYear.findAllByUser(baseService?.getLoggedInUser())
	}
	
	@Transactional(readOnly = true)
	def getTaxTypeById(def id){
		return TaxType.findById(id)
	}
}
