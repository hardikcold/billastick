package com.billing.service

import org.springframework.transaction.annotation.Transactional

import com.billing.domain.FinancialYear
import com.billing.domain.Tax
import com.billing.domain.TaxType

class TaxTypeService {

	def baseService
	
	@Transactional(readOnly = true)
    def getTaxTypeList(params) {
		return TaxType.findAllByUser(baseService?.getLoggedInUser(), [max : params?.max, offset : params?.offset, sort: "taxTypeName", order: params?.order])
    }
	
	@Transactional(readOnly = true)
	def getAllTaxTypeList() {
		return TaxType.findAllByUser(baseService?.getLoggedInUser())
	}
	
	@Transactional(readOnly = true)
	def getTaxTypeListSize(params){
		return TaxType.findAllByUser(baseService?.getLoggedInUser())?.size()
	}
	
}
