package com.billing.service

import org.apache.shiro.crypto.hash.Sha512Hash
import org.springframework.transaction.annotation.Transactional

import com.billing.domain.Customer
import com.billing.domain.Item
import com.billing.domain.Measurement
import com.billing.domain.Role
import com.billing.domain.Tax
import com.billing.domain.TaxType
import com.billing.domain.User


class UserService {
	
	def baseService
	def createRole(){
		def roleInstance = new Role(name: "Admin")
		addPermission(roleInstance)
		roleInstance = new Role(name:"Company")
		addPermission(roleInstance)
	}
	
	def addPermission(def roleInstance){
		roleInstance.addToPermissions("auth:*")
		roleInstance.addToPermissions("customer:*")
		roleInstance.addToPermissions("dashboard:*")
		roleInstance.addToPermissions("estimate:*")
		roleInstance.addToPermissions("invoice:*")
		roleInstance.addToPermissions("item:*")
		roleInstance.addToPermissions("measurement:*")
		roleInstance.addToPermissions("receivedPayment:*")
		roleInstance.addToPermissions("tax:*")
		roleInstance.addToPermissions("taxType:*")
		roleInstance.addToPermissions("creditNote:*")
		roleInstance.addToPermissions("refund:*")
		roleInstance.addToPermissions("profile:*")
		roleInstance.addToPermissions("applyCredit:*")
		roleInstance.addToPermissions("report:*")
		roleInstance.addToPermissions("search:*")
		
		if(roleInstance.name == "Admin"){
			roleInstance.addToPermissions("user:*")
		}
		
		roleInstance.save()
	}
	def createUser(userInstance){
		Calendar defaultTrialExpiredDate = Calendar.getInstance()
		defaultTrialExpiredDate.add(Calendar.MONTH, 1)
		userInstance.expiredDate = defaultTrialExpiredDate.getTime()
		userInstance.save(flush:true)
	}

    def createDefaultUser() {
		createRole()
		
		def adminRole = Role.findByName("Admin")
		def admin = new User(username: "admin@billastick.com", passwordHash: new Sha512Hash("admin").toHex(),organizationName : "Hardik Tech Admin", firstName : "Hardik Admin", lastName : "Shah", address1: 'B/7 Shree Appartment', address2: 'Highway', city : 'Kalol', mobileNumber : '9898095867')
		admin.addToRoles(adminRole)
		admin.save()
	   
		def userRole = Role.findByName("Company")
		def user = new User(username: "hardikcold@gmail.com", passwordHash: new Sha512Hash("hardikcold").toHex(), organizationName : "Hardik Tech User", firstName : "Hardik User", lastName : "Shah",  address1: 'B/7 Shree Appartment', address2: 'Highway', city : 'Kalol', mobileNumber : '9898095867')
		user.addToRoles(userRole)
		user.save()
		
		generateDefaultTax(user)
		
		//def dateList = InvoiceUtility.getFinancialYearFromStartDate("1-4-2013")
		//def finacialYear = FinancialYear.findByUserAndStatus(admin, 'Current') ?: new FinancialYear(startDate : dateList.get(0), endDate : dateList.get(1))
		//finacialYear.user = admin
		//finacialYear.save(flush : true)
    }
	
	def generateDefaultTax(def user){
		def taxInstance1 = new Tax()
		taxInstance1.taxName = "VAT"
		taxInstance1.taxRate = 12.5
		taxInstance1.user = user
		taxInstance1.save(flush:true)
		
		def taxInstance2 = new Tax()
		taxInstance2.taxName = "VAT"
		taxInstance2.taxRate = 4
		taxInstance2.user = user
		taxInstance2.save(flush:true)
		
		def taxInstance3 = new Tax()
		taxInstance3.taxName = "Additional"
		taxInstance3.taxRate = 4
		taxInstance3.isAdditional = true
		taxInstance3.user = user
		taxInstance3.save(flush:true)
		
		def taxInstance4 = new Tax()
		taxInstance4.taxName = "Additional"
		taxInstance4.taxRate = 1
		taxInstance4.isAdditional = true
		taxInstance4.user = user
		taxInstance4.save(flush:true)
		
		def taxInstance5 = new Tax()
		taxInstance5.taxName = "ST"
		taxInstance5.taxRate = 12
		taxInstance5.user = user
		taxInstance5.save(flush:true)
		
		def taxTypeInstance1 = new TaxType()
		taxTypeInstance1.taxTypeName = "Vat 12.5 & Add 4"
		taxTypeInstance1.user = user
		taxTypeInstance1.addToTax(taxInstance1)
		taxTypeInstance1.addToTax(taxInstance3)
		taxTypeInstance1.save(flush:true)
		
		def taxTypeInstance2 = new TaxType()
		taxTypeInstance2.taxTypeName = "Vat 4 & Add 1"
		taxTypeInstance2.user = user
		taxTypeInstance2.addToTax(taxInstance2)
		taxTypeInstance2.addToTax(taxInstance4)
		taxTypeInstance2.save(flush:true)
		
		def taxTypeInstance3 = new TaxType()
		taxTypeInstance3.taxTypeName = "Service Tax"
		taxTypeInstance3.user = user
		taxTypeInstance3.addToTax(taxInstance5)
		taxTypeInstance3.save(flush:true)
		
		def measureInstance1 = new Measurement()
		measureInstance1?.measurementName = "Bags"
		measureInstance1?.measurementCode = "BG"
		measureInstance1.user = user
		measureInstance1.save(flush:true)
		
		def measureInstance2 = new Measurement()
		measureInstance2?.measurementName = "Numbers"
		measureInstance2?.measurementCode = "Nos"
		measureInstance2.user = user
		measureInstance2.save(flush:true)
		
		/*def itemInstance1 = new Item()
		itemInstance1.itemName = "Item 1"
		itemInstance1.measurement = measureInstance1
		itemInstance1.user = user
		itemInstance1.taxType = taxTypeInstance1
		itemInstance1.save(flush : true)
		
		def itemInstance2 = new Item()
		itemInstance2.itemName = "Item 2"
		itemInstance2.measurement = measureInstance2
		itemInstance2.user = user
		itemInstance2.taxType = taxTypeInstance2
		itemInstance2.save(flush : true)
		
		def itemInstance3 = new Item()
		itemInstance3.itemName = "Item 3"
		itemInstance3.measurement = measureInstance2
		itemInstance3.user = user
		itemInstance3.taxType = taxTypeInstance3
		itemInstance3.save(flush : true)*/
		
		def customerInstance1 = new Customer()
		customerInstance1.customerName = "Devars Shah"
		customerInstance1.address1 = "Shrinagar Socity"
		customerInstance1.city = "Kalol"
		customerInstance1.state = "Gujarat"
		customerInstance1.user = user
		customerInstance1.save(flush : true)
		
		def customerInstance2 = new Customer()
		customerInstance2.customerName = "Ravi Patel"
		customerInstance2.address1 = "Rajdhani Apt"
		customerInstance2.city = "Kalol"
		customerInstance2.state = "Gujarat"
		customerInstance2.user = user
		customerInstance2.save(flush : true)
		
	}
	
	
	@Transactional(readOnly = true)
	def getUsersList(params) {
		if(params?.searchText){
			println "In Search..."
			return User.findAllByUsernameLike("%${params?.searchText}%", [max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
		}else{
			return User.findAll([max : params?.max, offset : params?.offset, sort: "dateCreated", order: params?.order?:'desc'])
		}
	}
	
	@Transactional(readOnly = true)
	def getUsersListSize(params){
		if(params?.searchText){
			return User.findAllByUsernameLike("%${params?.searchText}%")?.size()
		}else{
			return User.count()
		}
	}
	
	
}
