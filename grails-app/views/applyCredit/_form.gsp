 <%@ page import="com.billing.domain.Customer" %>
<%@ page import="com.billing.utility.PaymentMethod" %>
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Credit Note#" /></label>
     <div class="controls">
         <input type="hidden" name="creditNoteNumber" id="creditNoteNumber" value="${creditNoteInstance?.creditNoteNumber}" />
			<input type="hidden" name="creditNoteId" id="creditNoteId" value="${creditNoteInstance?.id}" />
			${creditNoteInstance?.creditNoteNumber}
     </div>
 </div>
 
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer" /></label>
     <div class="controls">
        <input type="hidden" name="customerId" id="customerId" value="${creditNoteInstance?.customer?.id}" />
			<g:link id="${creditNoteInstance?.customer?.id}" controller="customer" action="edit"> ${creditNoteInstance?.customer?.customerName} </g:link>
     </div>
 </div>
 
  <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Available Credits" /></label>
     <div class="controls">
        ${creditNoteInstance?.balanceDue}
     </div>
 </div>
 
 
 <div class="widget-content nopadding" id="invoiceListDiv">
	<table class="table table-bordered data-table" id="invoiceTable">
		 <tr>
            <td>Invoice#</td>
            <td>Invoice Date</td>
            <td>Invoice Amount</td>
            <td>Invoice Balance</td>
            <td>Amount to Credit</td>
        </tr>
         <g:if test="${invoiceList?.size() > 0}">
	         <g:each in="${invoiceList}" var="invoiceInstance" status="i">	
				<tr class="gradeX">
	                <td width="10%">${invoiceInstance?.invoiceNumber}<g:hiddenField name="invoiceNumber" value="${invoiceInstance?.invoiceNumber}"/></td>           
	            	<td width="10%"><g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/></td>
	            	<td width="10%">${invoiceInstance?.netTotal} </td>
	            	<td width="10%">${invoiceInstance?.balanceDue}</td>
					<td width="60%"><input type="text" id="creditAmount" name="creditAmount" value="0.00" style="text-align: right;width: 200px;"/></td>
				</tr>
			</g:each>	
		</g:if>
		<g:else>
			<tr class="gradeX">
				<td colspan="5" style="text-align: center">No record found</td>
			</tr>
		</g:else>	
	</table>
</div>		
  	
