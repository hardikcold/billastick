 <%@ page import="com.billing.domain.Customer" %>
<%@ page import="com.billing.utility.PaymentMethod" %>
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice#" /></label>
     <div class="controls">
         <input type="hidden" name="invoiceNumber" id="invoiceNumber" value="${invoiceInstance?.invoiceNumber}" />
			<input type="hidden" name="invoiceId" id="invoiceId" value="${invoiceInstance?.id}" />
			${invoiceInstance?.invoiceNumber}
     </div>
 </div>
 
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer" /></label>
     <div class="controls">
        <input type="hidden" name="customerId" id="customerId" value="${invoiceInstance?.customer?.id}" />
			<g:link id="${invoiceInstance?.customer?.id}" controller="customer" action="edit"> ${invoiceInstance?.customer?.customerName} </g:link>
     </div>
 </div>
 
  <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice Balance" /></label>
     <div class="controls">
        ${invoiceInstance?.balanceDue}
     </div>
 </div>
 
  <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice Date" /></label>
     <div class="controls">
        <g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/>
     </div>
 </div>
 
 
 <div class="widget-content nopadding" id="creditNoteListDiv">
	<table class="table table-bordered data-table" id="creditNoteTable">
		 <tr>
            <td>Credit Note#</td>
            <td>Credit Note Date</td>
            <td>Credit Amount</td>
            <td>Credit Balance</td>
            <td>Amount to Credit</td>
        </tr>
         <g:if test="${creditNoteList?.size() > 0}">
	         <g:each in="${creditNoteList}" var="creditNoteInstance" status="i">	
				<tr class="gradeX">
	                <td width="10%">${creditNoteInstance?.creditNoteNumber}<g:hiddenField name="creditNoteNumber" value="${creditNoteInstance?.creditNoteNumber}"/></td>           
	            	<td width="10%" style="text-align: right"><g:formatDate format='dd/MM/yyyy' date='${creditNoteInstance?.creditNoteDate}'/></td>
	            	<td width="10%" style="text-align: right">${creditNoteInstance?.netTotal} </td>
	            	<td width="10%" style="text-align: right">${creditNoteInstance?.balanceDue}</td>
					<td width="60%"><input type="text" id="creditAmount" name="creditAmount" value="0.00" style="text-align: right;width: 200px;"/></td>
				</tr>
			</g:each>	
		</g:if>
		<g:else>
			<tr class="gradeX">
				<td colspan="5" style="text-align: center">No record found</td>
			</tr>
		</g:else>	
	</table>
</div>		
  	
