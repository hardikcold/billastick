<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="login" />
  <title>Forgot Password</title>
</head>
<body>
  <g:if test="${flash.message}">
	<div class="alert alert-error">
         <button class="close" data-dismiss="alert">×</button>
         <strong>Error!</strong> ${flash.message }
     </div>
  </g:if>
   <g:if test="${flash.successMessage}">
	<div class="alert alert-success">
         <button class="close" data-dismiss="alert">×</button>
         <strong>Error!</strong> ${flash.successMessage }
     </div>
  </g:if>
         <form id="recoverform" action="forgotPassword" class="form-vertical">
         	 <div class="control-group normal_text"><img src="${resource(dir: 'img', file: 'billastick_logo.png')}"></div>
			<p class="normal_text" style="color: white">Enter your e-mail address below and we will send you instructions <br/><font color="#FF6633">how to recover a password.</font></p>
	
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                     </div>
                 </div>
            
             <div class="form-actions">
                 <input type="button" data-dismiss="modal" onclick="window.location.href = '${createLink(action: 'login', controller: 'auth')}'" class="btn btn-warning" value="${message(code: 'default.button.reset.label', default: '&laquo;Back to login')}" />
                 <span class="pull-right"><input type="submit" class="btn btn-info" value="Recover" /></span>
             </div>
         </form>
         
</body>
</html>

