<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="login" />
  <title>Login</title>
</head>
<body>
  <g:if test="${flash.message}">
	<div class="alert alert-error">
         <button class="close" data-dismiss="alert">×</button>
         <strong>Error!</strong> ${flash.message }
     </div>
  </g:if>
   <g:if test="${flash.successMessage}">
	<div class="alert alert-success">
         <button class="close" data-dismiss="alert">×</button>
         <strong>Success!</strong> ${flash.successMessage }
     </div>
  </g:if>
	 <form id="loginform" class="form-vertical" action="signIn">
	 <div class="control-group normal_text"><img src="${resource(dir: 'img', file: 'billastick_logo.png')}"></div>
             <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" name="username" id="username" value="${username}"/>
                     </div>
                 </div>
             </div>
             <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-lock"></i></span><input type="password" placeholder="Password" name="password" id="password"/>
                     </div>
                 </div>
             </div>
             <div class="form-actions">
                 <input type="button" data-dismiss="modal" onclick="window.location.href = '${createLink(action: 'forgotPassword', controller: 'auth')}'" class="btn btn-warning" value="${message(code: 'default.button.reset.label', default: 'Forgot Password?')}" />
                 <span class="pull-right"><input type="submit" class="btn btn-success" value="Login" /></span>
                 <span class="pull-right"><input type="button" data-dismiss="modal" onclick="window.location.href = '${createLink(action: 'register', controller: 'auth')}'" class="btn btn-info" value="${message(code: 'default.button.reset.label', default: 'Register')}" /> &nbsp;</span>
             </div>
         </form>
         
          <script type="text/javascript">
			$(document).ready(function(){
				
				// Form Validation
			    $("#loginform").validate({
					rules:{
						username:{
							required:true
						},
						password:{
							required:true
						}
					},
					messages:{
						username:"Username cannot be blank.",
						password:"Password cannot be blank.",
					},
					errorClass: "help-inline",
					errorElement: "span",
					highlight:function(element, errorClass, validClass) {
						$(element).parents('.control-group').addClass('error');
					},
					unhighlight: function(element, errorClass, validClass) {
						$(element).parents('.control-group').removeClass('error');
					}
				});
			});
				
			</script>
</body>
</html>

