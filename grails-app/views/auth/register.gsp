<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="layout" content="login" />
  <title>Register</title>
</head>
<body>
  <g:if test="${flash.message}">
	<div class="alert alert-error">
         <button class="close" data-dismiss="alert">×</button>
         <strong>Error!</strong> ${flash.message }
     </div>
  </g:if>
   <g:if test="${flash.successMessage}">
	<div class="alert alert-success">
         <button class="close" data-dismiss="alert">×</button>
         <strong>Success!</strong> ${flash.successMessage }
     </div>
  </g:if>
	 <form id="registerForm" class="form-vertical" action="registerUser">
	 <div class="control-group normal_text"><img src="${resource(dir: 'img', file: 'billastick_logo.png')}"></div>
	  <p class="normal_text" style="color: white">Enter following details <font color="#FF6633">for register to Billastick.</font></p>
             <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" name="username" id="username" value="${userInstance?.username}" />
                         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="username"></g:fieldError></span>
                     </div>
                 </div>
             </div>
             <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-lock"></i></span><input type="password" placeholder="Password" name="passwordHash" id="passwordHash"/>
                         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="passwordHash"></g:fieldError></span>
                     </div>
                 </div>
             </div>
             
              <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-user"></i></span><input type="text" placeholder="Organization Name" name="organizationName" id="organizationName" value="${userInstance?.organizationName}"/>
                         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="organizationName"></g:fieldError></span>
                     </div>
                 </div>
             </div>
              <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-tasks"></i></span><input type="text" placeholder="First Name" name="firstName" id="firstName" value="${userInstance?.firstName}"/>
                          <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="firstName"></g:fieldError></span>
                     </div>
                 </div>
             </div>
              <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-tasks"></i></span><input type="text" placeholder="Last Name" name="lastName" id="lastName" value="${userInstance?.lastName}"/>
                         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="lastName"></g:fieldError></span>
                     </div>
                 </div>
             </div>
              <div class="control-group">
                 <div class="controls">
                     <div class="main_input_box">
                         <span class="add-on"><i class="icon-tasks"></i></span><input type="text" placeholder="Mobile Number" name="mobileNumber" id="mobileNumber" value="${userInstance?.mobileNumber}"/>
                         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="mobileNumber"></g:fieldError></span>
                     </div>
                 </div>
             </div>
             
             
             <div class="form-actions">
                 <input type="button" data-dismiss="modal" onclick="window.location.href = '${createLink(action: 'login', controller: 'auth')}'" class="btn btn-warning" value="${message(code: 'default.button.reset.label', default: '&laquo;Back to login')}" />
                 <span class="pull-right"><input type="submit" class="btn btn-success" value="Register" /></span>
             </div>
         </form>
         
<script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#registerForm").validate({
			rules:{
				username:{
					required:true
				},
				passwordHash:{
					required:true
				},
				organizationName:{
					required:true
				},
				firstName:{
					required:true
				},
				lastName:{
					required:true
				},
				mobileNumber:{
					required:true
				}
			},
			messages:{
				username:"Username cannot be blank.",
				passwordHash:"Password cannot be blank.",
				organizationName : "Organization Name cannot be blank.",
				firstName : "First Name cannot be blank.",
				lastName : "Last Name cannot be blank.",
				mobileNumber : "Mobile Number cannot be blank."
				
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>          
</body>
</html>

