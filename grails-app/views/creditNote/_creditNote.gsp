<div class="content">

	<div class="page-header">
		<div class="icon">
			<span class="ico-dollar"></span>
		</div>
		<h3>
			Credit Note # ${creditNoteInstance?.creditNoteNumber} 
		</h3>
	</div>

	<div class="row-fluid">
		<div class="span12">

			<div class="block">
				<div class="data invoice">

					<div class="row-fluid">
						<div class="span3">
							<address>
								<strong>${creditNoteInstance?.user?.displayName}</strong>
								<g:if test="${creditNoteInstance?.user?.address1}">
								<br> ${creditNoteInstance?.user?.address1}
								</g:if>
								<g:if test="${creditNoteInstance?.user?.address2}">
								<br> ${creditNoteInstance?.user?.address2}
								</g:if>
								<g:if test="${creditNoteInstance?.user?.city}">
								<br>${creditNoteInstance?.user?.city}
								</g:if>
								<g:if test="${creditNoteInstance?.user?.mobileNumber}">
								<br>Mobile:	${creditNoteInstance?.user?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3">
							<address>
								<strong>${creditNoteInstance?.customer?.customerName}</strong>
								<g:if test="${creditNoteInstance?.customer?.address1}">
								<br> ${creditNoteInstance?.customer?.address1}
								</g:if>
								<g:if test="${creditNoteInstance?.customer?.address2}">
								<br> ${creditNoteInstance?.customer?.address2} 
								</g:if>
								<g:if test="${creditNoteInstance?.customer?.city}">
								<br>${creditNoteInstance?.customer?.city}
								</g:if>
								<g:if test="${creditNoteInstance?.customer?.mobileNumber}">
								<br>Phone :  ${creditNoteInstance?.customer?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3"></div>
						<div class="span3">
								<strong>Credit Note Date:</strong> <g:formatDate format='dd/MM/yyyy' date='${creditNoteInstance?.creditNoteDate}'/><br>
							<div class="highlight">
								<strong>Credit Note Amount:</strong> ${creditNoteInstance?.netTotal}  <em>INR</em>
							</div>
						</div>
					</div>

					<h4>Description</h4>
					<table class="table" width="100%">
						<thead>
							<tr>
								<th width="60%" style="text-align: left">Description</th>
								<th width="10%" style="text-align: right">Price</th>
								<th width="10%" style="text-align: right">Quantity</th>
								<th width="10%" style="text-align: right">Discount</th>
								<th width="10%" style="text-align: right">Tax1</th>
								<th width="10%" style="text-align: right">Tax2</th>
								<th width="10%" style="text-align: right">Tax3</th>
								<th width="10%" style="text-align: right">Total</th>
							</tr>
						</thead>
						<tbody>
						<g:each in="${creditNoteInstance?.invoiceDetails}" var="detailsInstance">
							<tr>
								<td>${detailsInstance?.item?.itemName }</td>
								<td style="text-align: right">${detailsInstance?.rate}</td>
								<td style="text-align: right">${detailsInstance?.quantity}</td>
								<td style="text-align: right">${detailsInstance?.discountRate}%</td>
								<td style="text-align: right">${detailsInstance?.tax1 == null ? 'N/A' : detailsInstance?.tax1?.taxName + '@'+ detailsInstance?.tax1?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.tax2 == null ? 'N/A' : detailsInstance?.tax2?.taxName + '@'+ detailsInstance?.tax2?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.tax3 == null ? 'N/A' : detailsInstance?.tax3?.taxName + '@'+ detailsInstance?.tax3?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.total }</td>
							</tr>
						</g:each>	
						</tbody>
					</table>

					<div class="row-fluid">
						<div class="span9"></div>
						<div class="span3">
							<table class="table" width="100%">
								<tr ><td><span><b>Sub Total:</b></span></td><td style="text-align: right">${creditNoteInstance?.subTotal}</td></tr>
								<g:each in="${creditNoteInstance?.taxTransactionDetails}" var="taxTransactionInstance">
									<tr ><td><span>${taxTransactionInstance?.tax?.taxName + '@'+ taxTransactionInstance?.tax?.taxRate + '%'}:</span></td><td style="text-align: right">${taxTransactionInstance?.taxAmount}</td></tr>	
								</g:each>
								<tr class="highlight"><td><span><b>Net Total:</b></span></td><td style="text-align: right"> ${creditNoteInstance?.netTotal}</td></tr>
							</table>	
						</div>
					</div>

				</div>

			</div>

		</div>
	</div>

</div>