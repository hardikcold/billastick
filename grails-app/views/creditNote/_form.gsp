<%@page import="grails.converters.JSON"%>
<%@ page import="com.billing.domain.Item" %>
<%@ page import="com.billing.domain.Tax" %>
<%@ page import="com.billing.domain.Customer" %>
<%@ page import="com.billing.utility.PaymentType" %>
<%@ page import="com.billing.utility.InvoiceType" %>
<script>
function doCalculate(rowIndex, appliedTaxIdList, isTaxChange){
	var discount = 0, total = 0, rate = 0, quantity = 0, taxRate = 0, taxRateList, subTotal = 0, netTotal = 0;
	//var value = obj.value
	quantity =  $('#quantity_' + rowIndex).val();
	rate =  $('#rate_' + rowIndex).val();
	if($('#item_' + rowIndex).val() == ""){
		rate = "0.00";
	}
	discount =  $('#discount_' + rowIndex).val();
	//Total of particular ROW
	total = (quantity * rate) - ((quantity * rate * discount)/100);
	//alert(total);
	$('#total_' + rowIndex).val(total.toFixed(2));// Sub = 160 , 0
	$('input[id^="total"]').each(function(){ 
		subTotal = subTotal + Number($(this).val()) //alert(Number($(this).val()));
	});

	var taxList = jQuery.parseJSON($('#hiddenTaxList_' + rowIndex).val());
	//Calculate Tax Amount for TaxType Change custom to default
	var newAppliedTaxIdList = new Array();
	
	if(isTaxChange == "true"){
		for(var i=0;i<appliedTaxIdList.length;i++){
			if(taxList != null){
				for(var j=0;j<taxList.length;j++){
					var jsonObj = taxList[j]
					if(jsonObj != null){
						if(appliedTaxIdList[i] == jsonObj.id){
							newAppliedTaxIdList[i] = jsonObj
							break;
						}
					}
				}
			}
		}
		appliedTaxIdList = newAppliedTaxIdList;
	}
	
	for(var i=0;i<appliedTaxIdList.length;i++){
		var jsonObj = appliedTaxIdList[i]
		if(jsonObj != null){
			var taxRate = parseFloat(jsonObj.taxRate)
			var taxAmount = (parseFloat(total) * taxRate)/100;
			$('#hiddenTaxDetails_' + jsonObj.id +"_"+ rowIndex).val(taxAmount.toFixed(2))
		}
	}

	//Display total tax to particular type of tax
	if(taxList != null){
		for(var i=0;i<taxList.length;i++){
			var taxTotal = 0; 
			var taxObj = taxList[i];
			if(taxObj != null){
				var idTxt = "hiddenTaxDetails_" + taxObj.id
				$('input[id^='+idTxt + ']').each(function(){ 
					taxTotal = taxTotal + Number($(this).val()) //alert(Number($(this).val()));
				});
				$('#taxTotal_' + taxObj.id).val(taxTotal.toFixed(2));
				$('#divTaxTotal_' + taxObj.id).html(taxTotal.toFixed(2));
			}
		}
	}

	//Set value for SubTotal
	$('#subTotal').html(subTotal.toFixed(2));
	$('#hiddenSubTotal').val(parseFloat(subTotal.toFixed(2)));

	//Set value for NetTotal
	netTotal = parseFloat($('#hiddenSubTotal').val())
	$('input[id^="taxTotal_"]').each(function(){ 
		netTotal = netTotal + Number($(this).val()) //alert(Number($(this).val()));
	});
	$('#netTotal').html(netTotal.toFixed(2));
	$('#hiddenNetTotal').val(netTotal.toFixed(2));

}

//On Type of Discount, Quantity and Rate input revised all amount
function calculateOnType(obj){
	var rowIndex = obj.id.split("_")[1];
	//var appliedTaxIdList = jQuery.parseJSON($('#hiddenAppliedTaxList_' + rowIndex).val());
	//doCalculate(rowIndex, appliedTaxIdList, "false")
	calulateTaxWithoutRemoteCall(rowIndex)
}

function onChangeItem(data){
	var rowIndex = 0;
	rowIndex = data.elementId.split("_")[1];
	//Set Default Tax Type selected 
	$("#tax1_" + rowIndex ).val(data.taxId1);
	$("#tax2_" + rowIndex ).val(data.taxId2);
	$("#tax3_" + rowIndex ).val(data.taxId3);
	$("#rate_" + rowIndex ).val(data.itemRate.toFixed(2));
	
	//Set All hiddenTaxDetails to 0[START]
	$('#hiddenTaxList_' + rowIndex).val(JSON.stringify(data.taxList))
	resetTaxDetailsForRow(rowIndex);
	//Set All hiddenTaxDetails to 0[END]
	
	$('#hiddenAppliedTaxList_' + rowIndex).val(JSON.stringify(data.appliedTaxIdList))
	var appliedTaxIdList = jQuery.parseJSON($('#hiddenAppliedTaxList_' + rowIndex).val());
	doCalculate(rowIndex, appliedTaxIdList, "false")
}

function resetTaxDetailsForRow(rowIndex){
	//Set All hiddenTaxDetails to 0[START]
	//$('#hiddenTaxList_' + rowIndex).val(JSON.stringify(data.taxList))
	var taxList = jQuery.parseJSON($('#hiddenTaxList_' + rowIndex).val());
	if(taxList != null){
		for(var i=0;i<taxList.length;i++){
			var taxObj = taxList[i];
			$('#hiddenTaxDetails_' + taxObj.id +"_"+ rowIndex).val("0")
		}
	}
	//Set All hiddenTaxDetails to 0[END]
}
function onChangeTax(data){
	var rowTotal = 0;
	rowIndex = data.elementId.split("_")[1];
	$('#hiddenTaxList_' + rowIndex).val(JSON.stringify(data.taxList))
	calulateTaxWithoutRemoteCall(rowIndex)
}

function calulateTaxWithoutRemoteCall(rowIndex){
	var tax1, tax2, tax3;

	tax1 = $('#tax1_' + rowIndex).val();
	tax2 = $('#tax2_' + rowIndex).val();
	tax3 = $('#tax3_' + rowIndex).val();
	
	resetTaxDetailsForRow(rowIndex);

	var appliedTaxIdList = new Array();
	appliedTaxIdList[0] = tax1
	appliedTaxIdList[1] = tax2
	appliedTaxIdList[2] = tax3
	doCalculate(rowIndex, appliedTaxIdList , "true")
}
</script>



 <div class="control-group  fieldcontain ${hasErrors(bean: creditNoteInstance, field: 'creditNoteNumber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Credit Note#" /></label>
     <div class="controls">
         <input type="text" name="creditNoteNumber" readonly="readonly" value="${creditNoteInstance?.id == null ? (creditNoteNumber?:creditNoteInstance?.creditNoteNumber) : creditNoteInstance?.creditNoteNumber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${creditNoteInstance}" field="creditNoteNumber"></g:fieldError></span>
     </div>
 </div>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: creditNoteInstance, field: 'customer', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer" /></label>
     <div class="controls">
         ${invoiceInstance?.customer?.customerName}
         <g:hiddenField name="customerId" value="${invoiceInstance?.customer?.id }"/>
     </div>
 </div>
 
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice Date" /></label>
     <div class="controls">
         <g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/>
         <g:hiddenField name="invoiceDate" value="${invoiceInstance?.invoiceDate}"/>
     </div>
 </div>
 
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Balance Due" /></label>
     <div class="controls">
         ${invoiceInstance?.totalBalanceDue}
         <g:hiddenField name="invoiceBalanceDue" value="${invoiceInstance?.totalBalanceDue}"/>
     </div>
 </div>
 
  <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice#" /></label>
     <div class="controls">
         ${invoiceInstance?.invoiceNumber}
         <g:hiddenField name="invoiceId" value="${invoiceInstance?.id}"/>
     </div>
 </div>
 
 <div class="control-group fieldcontain ${hasErrors(bean: creditNoteInstance, field: 'creditNoteDate', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Credit Note Date" /></label>
     <div class="controls">
         <input type="text" class="datepicker" readonly="readonly" id="creditNoteDate" name="creditNoteDate" size="10" value="<g:formatDate format='dd/MM/yyyy' date='${creditNoteInstance?.creditNoteDate}'/>"/>
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${creditNoteInstance}" field="creditNoteDate"></g:fieldError></span>
     </div>
 </div>
 <script>
 $( ".datepicker" ).datepicker({
		minDate : "<g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/>",
		dateFormat : 'dd/mm/yy',
		showOn: "button",
		 buttonImage: "${resource(dir: 'images', file: 'calendar.png')}",
		 buttonImageOnly: true,
		 changeMonth: true,
		 changeYear: true,
	});
 </script>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: creditNoteInstance, field: 'customerNotes', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer Notes" /></label>
     <div class="controls">
         <g:textArea name="customerNotes" value="${creditNoteInstance?.customerNotes}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${creditNoteInstance}" field="customerNotes"></g:fieldError></span>
     </div>
 </div>
 
<div class="widget-content nopadding" id="measurementListDiv">
	<table class="table table-bordered data-table" id="itemTable">
		 <tr>
            <td>Item</td>
            <td>Quantity</td>
            <td>Rate</td>
            <td>Discount(%)</td>
            <td style="text-align: center">Tax1</td>
            <td style="text-align: center">Tax2</td>
            <td style="text-align: center">Tax3</td>
            <td style="text-align: right">Total</td>
            <td>&nbsp;</td>
        </tr>
         <g:if test="${creditNoteInstance?.id == null}">
			<tr class="gradeX">
				<td>
					<span class="control-group"><g:select from="${itemList}" noSelection="['':'--Select--']" style="width:200px;" name="items" optionKey="id" optionValue="itemName" id="item_1" 
           			   onChange="${remoteFunction(action: 'getItemDetails',
                       onSuccess : 'onChangeItem(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></span>
                </td>
                <td width="5%"><span class="control-group"><input type="text" id="quantity_1" name="quantity" value="1.00" style="text-align: right;width: 100px;" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></span></td>           
            	<td width="5%"><span class="control-group"><input type="text" id="rate_1" name="rate" value="1.00" style="text-align: right;width: 100px;" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></span></td>
				<td width="5%"><input type="text" id="discount_1" name="discount" style="text-align: right;width: 70px;" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>
				<td width="5%">
					<g:select from="${taxList}" noSelection="['':'--Select--']" style="width:150px;" name="tax1" optionKey="id" optionValue="name" id="tax1_1" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>
                <td width="5%">
					<g:select from="${taxList}" noSelection="['':'--Select--']" style="width:150px;" name="tax2" optionKey="id" optionValue="name" id="tax2_1" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>
                 <td width="5%">
					<g:select from="${taxList}" noSelection="['':'--Select--']" style="width:150px;" name="tax3" optionKey="id" optionValue="name" id="tax3_1" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>    
				<td width="5%"><input type="text" id="total_1"  name="total" value="0.00" readonly="readonly" style="text-align: right;width: 100px;"/></td>
	            <td width="2%"><input type="button" class="btn btn-mini btn-danger" id="delPOIbutton_1" value="Delete" onclick="deleteRow(this)"/></td>
	            <td>&nbsp;
	            <g:hiddenField name="hiddenTaxValue" id="hiddenTaxValue_1" value=""/>
	            <g:hiddenField name="hiddenAppliedTaxList" id="hiddenAppliedTaxList_1" value=""/>
				<g:hiddenField name="hiddenTaxList" id="hiddenTaxList_1" value=""/>
				<g:each in="${taxList}" var="taxInstance">
	            	<g:hiddenField name="hiddenTaxDetails" id="hiddenTaxDetails_${taxInstance?.id}_1" value=""/>
	            </g:each>	
	            </td>
			</tr>
		</g:if>
		<g:else>
        	<g:each in="${creditNoteInstance?.invoiceDetails}" status="i" var="invoiceDetailsInstance">
        	<g:if test="${i==0}">
        		<tr>
			        <td><span class="control-group"><g:select from="${itemList}" value="${invoiceDetailsInstance?.item?.id}" noSelection="['':'--Select--']" style="width:200px;" name="items" optionKey="id" optionValue="itemName" id="item_1" 
			        onChange="${remoteFunction(action: 'getItemDetails',
			                       onSuccess : 'onChangeItem(data)',
			                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></span>
			          </td>
		            <td width="5%"><input size="10" type="text" id="quantity_1" value="${invoiceDetailsInstance?.quantity}" name="quantity" style="text-align: right;width: 100px" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>           
		            <td width="5%"><input size="10" type="text" id="rate_1" value="${invoiceDetailsInstance?.rate}" name="rate" style="text-align: right;width: 100px" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>
					<td width="5%"><input size="05" type="text" id="discount_1" value="${invoiceDetailsInstance?.discountRate}" name="discount" style="text-align: right;width: 70px" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>
					 <td width="5%"><g:select from="${taxList}" value="${invoiceDetailsInstance?.tax1?.id}" noSelection="['':'--Select--']" style="width:150px;" name="tax1" optionKey="id" optionValue="name" id="tax1_1" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>
               		 <td width="5%"><g:select from="${taxList}" value="${invoiceDetailsInstance?.tax2?.id}" noSelection="['':'--Select--']" style="width:150px;" name="tax2" optionKey="id" optionValue="name" id="tax2_1" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>
                	<td width="5%"><g:select from="${taxList}" value="${invoiceDetailsInstance?.tax3?.id}" noSelection="['':'--Select--']" style="width:150px;" name="tax3" optionKey="id" optionValue="name" id="tax3_1" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>  
		                       
					<td width="5%"><input size="25" type="text" id="total_1" value="${invoiceDetailsInstance?.total}" name="total" value="0.00" readonly="readonly" style="text-align: right;width: 100px"/></td>
		            <td width="2%"><input type="button" class="btn btn-mini btn-danger" id="delPOIbutton_1" value="Delete" onclick="deleteRow(this)"/></td>
		            <td>&nbsp;
		             <g:hiddenField name="hiddenTaxValue" id="hiddenTaxValue_1" value=""/>
	            	<g:hiddenField name="hiddenAppliedTaxList" id="hiddenAppliedTaxList_1" value=""/>
					<g:hiddenField name="hiddenTaxList" id="hiddenTaxList_1" value="${taxList as JSON}"/>
					<g:each in="${taxList}" var="taxInstance">
		            	<g:hiddenField name="hiddenTaxDetails" id="hiddenTaxDetails_${taxInstance?.id}_1" value=""/>
		            </g:each>	
		            </td>
	            </tr>
	          </g:if>
	          <g:else>
	          	<tr>
		          	<td><span class="control-group"><g:select from="${itemList}" value="${invoiceDetailsInstance?.item?.id}" noSelection="['':'--Select--']" style="width:200px;" name="items" optionKey="id" optionValue="itemName" id="item_1${i+1}" 
		          	onChange="${remoteFunction(action: 'getItemDetails',
			                       onSuccess : 'onChangeItem(data)',
			                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}" ></g:select></span>
			         </td>
		            <td width="5%"><input size="10" type="text" id="quantity_1${i+1}" value="${invoiceDetailsInstance?.quantity}" name="quantity" style="text-align: right;width: 100px" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>           
		            <td width="5%"><input size="10" type="text" id="rate_1${i+1}" value="${invoiceDetailsInstance?.rate}" name="rate" style="text-align: right;width: 100px" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>
					<td width="5%"><input size="05" type="text" id="discount_1${i+1}" value="${invoiceDetailsInstance?.discountRate}" name="discount" style="text-align: right;width: 70px" onkeyup="calculateOnType(this);onlyDecimal(this, 15)"/></td>
					 <td width="5%"><g:select from="${taxList}" value="${invoiceDetailsInstance?.tax1?.id}" noSelection="['':'--Select--']" style="width:150px;" name="tax1" optionKey="id" optionValue="name" id="tax1_1${i+1}" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>
               		 <td width="5%"><g:select from="${taxList}" value="${invoiceDetailsInstance?.tax2?.id}" noSelection="['':'--Select--']" style="width:150px;" name="tax2" optionKey="id" optionValue="name" id="tax2_1${i+1}" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>
                	<td width="5%"><g:select from="${taxList}" value="${invoiceDetailsInstance?.tax3?.id}" noSelection="['':'--Select--']" style="width:150px;" name="tax3" optionKey="id" optionValue="name" id="tax3_1${i+1}" 
					   onChange="${remoteFunction(action: 'getTaxDetails',
                       onSuccess : 'onChangeTax(data)',
                       params: '\'id=\' + this.value+\'&elementId=\'+this.id' )}"></g:select></td>  
					<td width="5%"><input size="25" type="text" id="total_1${i+1}" value="${invoiceDetailsInstance?.total}" name="total" value="0.00" readonly="readonly" style="text-align: right;width: 100px"/></td>
		            <td width="2%"><input type="button" class="btn btn-mini btn-danger" id="delPOIbutton_1${i+1}" value="Delete" onclick="deleteRow(this)"/></td>
		            <td>&nbsp;
		            <g:hiddenField name="hiddenTaxValue" id="hiddenTaxValue_1${i+1}" value=""/>
	            	<g:hiddenField name="hiddenAppliedTaxList" id="hiddenAppliedTaxList_1${i+1}" value=""/>
					<g:hiddenField name="hiddenTaxList" id="hiddenTaxList_1${i+1}" value="${taxList as JSON}"/>
					<g:each in="${taxList}" var="taxInstance">
		            	<g:hiddenField name="hiddenTaxDetails" id="hiddenTaxDetails_${taxInstance?.id}_1${i+1}" value=""/>
		            </g:each>	
		            </td>
	            </tr>
	          </g:else> 
            </g:each>
        </g:else>
		
		
	</table>
</div>
	<br>
    <input type="button" class="btn btn-primary" id="addmorePOIbutton" value="Add Row" onclick="insRow()"/>
    
    
    <div class="row-fluid">
    <div class="span3" style="float: right;margin-right:9%;">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-eye-open"></i>
								</span>
								<h5>Credit Note statistics</h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Tax</th>
											<th>Amount(INR)</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><b>Sub Total</b></td>
											<td><g:hiddenField name="hiddenSubTotal" id="hiddenSubTotal" value="${creditNoteInstance?.id == null ? '0.00' : creditNoteInstance?.subTotal}"/><div id="subTotal" style="float:right">${creditNoteInstance?.id == null ? '0.00' : creditNoteInstance?.subTotal}</div></td>
										</tr>
										 <g:each in="${taxList}" var="taxInstance" status="i">
								               <tr>
								                 <td><span>${taxInstance?.name }:</span></td>
								                 <td>
								                 <div id="divTaxTotal_${taxInstance.id}" style="float:right">
								                 0.00
								                 </div>
								                 <g:hiddenField name="taxTotal" id="taxTotal_${taxInstance.id}" value="0"/>
								                 <g:hiddenField name="taxId" id="taxId_${taxInstance.id}" value="${taxInstance.id}"/>
								                 </td>
								                 
								              </tr> 
							               </g:each>
							               <tr> 
							                  <td class="highlight">
							                      <strong><span>Total:</span></strong>
							                  </td>
							                  <td class="highlight">
							                  <g:hiddenField name="hiddenNetTotal" id="hiddenNetTotal" value="${creditNoteInstance?.id == null ? '0.00' : creditNoteInstance?.balanceDue}"/>
							                  <div id="netTotal" style="float:right">${creditNoteInstance?.id == null ? '0.00' : creditNoteInstance?.balanceDue}</div>
							                  </td>
							                 </tr>  
									</tbody>
								</table>
							</div>
						</div>
					</div>
	      </div>
	
<g:each in="${invoiceInstance?.invoiceDetails}" status="i" var="instanceCount">
 <script>
	var rowIndex = "1"+ '${i+1}'
	<g:if test="${i==0}">
		rowIndex = '${i+1}'
	</g:if>	
		calulateTaxWithoutRemoteCall(rowIndex)
 </script>  		
</g:each>	
<script>
  function deleteRow(row){
		var obj = document.getElementById('quantity_1')
		var i=row.parentNode.parentNode.rowIndex;
		if(i != 1){
			document.getElementById('itemTable').deleteRow(i);
			doCalculate(obj);
		}
	}

  function insRow()
	{
			var x=document.getElementById('itemTable');
			   // deep clone the targeted row
			var new_row = x.rows[1].cloneNode(true);
			   // get the total number of rows
			var len = x.rows.length;
			   // set the innerHTML of the first row 
			//new_row.cells[0].innerHTML = len;
			
			var inp0 = new_row.cells[0].getElementsByTagName('select')[0];
			inp0.id += len;
			inp0.value = '';

			var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
			inp1.id += len;
			inp1.value = '';
			   // grab the input from the first cell and update its ID and value
			var inp2 = new_row.cells[2].getElementsByTagName('input')[0];
			inp2.id += len;
			inp2.value = '';
			
			var inp3 = new_row.cells[3].getElementsByTagName('input')[0];
			inp3.id += len;
			inp3.value = '';

			var inp4 = new_row.cells[4].getElementsByTagName('select')[0];
			inp4.id += len;
			inp4.value = '';

			var inp5 = new_row.cells[5].getElementsByTagName('select')[0];
			inp5.id += len;
			inp5.value = '';


			var inp6 = new_row.cells[6].getElementsByTagName('select')[0];
			inp6.id += len;
			inp6.value = '';

			var inp7 = new_row.cells[7].getElementsByTagName('input')[0];
			inp7.id += len;
			inp7.value = '0.00';

			var inpbtn = new_row.cells[8].getElementsByTagName('input')[0];
			inpbtn.id += len;
			//inpbtn.value = '0';

			inp6 = new_row.cells[9].getElementsByTagName('input')[0];
			inp6.id += len;
			inp6.value = '0.00';

			inp6 = new_row.cells[9].getElementsByTagName('input')[1];
			inp6.id += len;
			inp6.value = '0.00';

			inp6 = new_row.cells[9].getElementsByTagName('input')[2];
			inp6.id += len;
			inp6.value = '0.00';

			<g:each in='${taxList}' var='taxInstance' status='i'>
				inp6 = new_row.cells[9].getElementsByTagName('input')[${i+3}];
				inp6.id += len;
				inp6.value = '0.00';
   		</g:each>

			   // append the new row to the table
			x.appendChild( new_row );
		}
	
 </script>	
	<script>
         $("#creditNoteForm").validate({
 			rules:{
     			creditNoteDate:{
     				required:true
         		},
         		items:{
     				required:true
         		},
         		quantity:{
     				required:true
         		},
         		rate:{
     				required:true
         		}
     			
 			},
 			messages:{
 				creditNoteDate:"",
 				items : "",
 				quantity:"",
 				rate:""
 	 				
 			},
 			errorClass: "help-inline",
 			errorElement: "span",
 			highlight:function(element, errorClass, validClass) {
 				$(element).parents('.control-group').addClass('error');
 			},
 			unhighlight: function(element, errorClass, validClass) {
 				$(element).parents('.control-group').removeClass('error');
 			}
 		});
   </script>
	
