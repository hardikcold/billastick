<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Credit Note Details</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
			              <g:if test="${flash.errorMessage}">
							<div class="alert alert-error">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Error!</strong> ${flash.errorMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<span class="icon" style="float:right"><g:link action="search" controller="creditNote" id="${creditNoteInstance?.id}"><i class="icon-arrow-left" title="Cancel"></i>&nbsp;</g:link></span>
								<span class="icon" style="float:right"><g:link action="printCreditNote" controller="creditNote" id="${creditNoteInstance?.id}"><i class="icon-print" title="Print"></i>&nbsp;</g:link></span>
								<span class="icon" style="float:right"><g:link action="edit" controller="creditNote" id="${creditNoteInstance?.id}"><i class="icon-pencil" title="Edit"></i>&nbsp;</g:link></span>
								<span class="icon" style="float:right">&nbsp;</span>
								<h5>Credit Note Details</h5>
							</div>
							<g:form class="form-horizontal">
								<g:render template="creditNote"></g:render>
							</g:form>
						</div><!-- End of widget-box -->	
						
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
