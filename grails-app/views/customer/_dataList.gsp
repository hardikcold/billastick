<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn style="text-align: left" action="search" onLoading="showSpinner(\'customerListDiv\')" property="customerName" update="customerListDiv" title="Name" defaultOrder="desc" params="[max:params?.max ?: 10, searchText : params?.searchText?:'']"/>
			<th style="text-align: right">Opening Balance</th>
			<th style="text-align: right">Dues</th>
			<th style="text-align: right">City</th>
			<th style="text-align: right">Mobile</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${customerInstanceList?.size() > 0 }">
				<g:each in="${customerInstanceList}" var="customerInstance" status="i">
					<tr class="gradeX">
						<td>${customerInstance?.customerName }</td>
						<td style="text-align: right" width="10%">${customerInstance?.openingBalance}</td>
						<td style="text-align: right" width="10%">${customerInstance?.totalDues}</td>
						<td style="text-align: right" width="10%">${customerInstance?.city }</td>
						<td style="text-align: right" width="10%">${customerInstance?.mobileNumber }</td>
						<td width="7%" style="text-align: right">
								<g:link action="edit" controller="customer" id="${customerInstance?.id}"><i class="icon-pencil" title="Edit"></i>&nbsp;</g:link>
								<a href="#myAlert${i}" data-toggle="modal"><i class="icon-trash" title="Delete"></i>&nbsp;</a>
								<g:link id="${customerInstance?.id}" controller="customer" action="customerDetails"><i class="icon-th-list"  title="Details"></i>&nbsp;</g:link>
						</td>
						 <div id="myAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${customerInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="3" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="list" total="${customerInstanceListTotal}" 
			update="customerListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[searchText : params?.searchText?:'']" onLoading="showSpinner(\'customerListDiv\')"/>
</div>

