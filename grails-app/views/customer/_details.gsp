<div class="content">

	<div class="page-header">
		<div class="icon">
			<span class="ico-dollar"></span>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">

			<div class="block">
				<div class="data invoice">

					<div class="row-fluid">
						<div class="span3">
							<address>
								<strong>${customerInstance?.customerName}</strong>
								<g:if test="${customerInstance?.address1}">
								<br> ${customerInstance?.address1}
								</g:if>
								<g:if test="${customerInstance?.address2}">
								<br> ${customerInstance?.address2}
								</g:if>
								<g:if test="${customerInstance?.city}">
								<br>${customerInstance?.city}
								</g:if>
								<g:if test="${customerInstance?.mobileNumber}">
								<br>Mobile:	${customerInstance?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3"></div>
						<div class="span3">
							<div class="highlight">
								<strong>Opening Balance:</strong> ${customerInstance?.openingBalance?:0.00}  <em>INR</em>
							</div>
							<div class="highlight">
								<strong>Payment Due:</strong> ${customerInstance?.totalDues?:0.00}  <em>INR</em>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>

</div>