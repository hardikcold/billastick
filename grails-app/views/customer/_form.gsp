<div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'customerName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer Name" /></label>
     <div class="controls">
         <input type="text" name="customerName" maxlength="100" value="${customerInstance?.customerName}" />
         <span for="customerName" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="customerName"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'customerEmail', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer Email" /></label>
     <div class="controls">
         <input type="text" name="customerEmail" maxlength="100" value="${customerInstance?.customerEmail}" />
         <span for="customerEmail" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="customerEmail"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'address1', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Address 1" /></label>
     <div class="controls">
         <input type="text" name="address1" maxlength="100" value="${customerInstance?.address1}" />
         <span for="address1" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="address1"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'address2', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Address 2" /></label>
     <div class="controls">
         <input type="text" name="address2" maxlength="100" value="${customerInstance?.address2}" />
         <span for="address2" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="address2"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'city', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="City" /></label>
     <div class="controls">
         <input type="text" name="city" maxlength="30" value="${customerInstance?.city}" />
         <span for="city" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="city"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'state', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="State" /></label>
     <div class="controls">
         <input type="text" name="state" maxlength="30" value="${customerInstance?.state}" />
         <span for="state" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="state"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'officeNumber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Office#" /></label>
     <div class="controls">
         <input type="text" name="officeNumber" id="officeNumber" maxlength="30"  value="${customerInstance?.officeNumber}" />
         <span for="officeNumber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="officeNumber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'mobileNumber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Mobile#" /></label>
     <div class="controls">
         <input type="text" name="mobileNumber" id="mobileNumber" maxlength="30" value="${customerInstance?.mobileNumber}" />
         <span for="mobileNumber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="mobileNumber"></g:fieldError></span>
     </div>
 </div>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'openingBalance', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Opening Balance" /></label>
     <div class="controls">
         <input type="text" name="openingBalance" maxlength="50" value="${customerInstance?.openingBalance}" />
         <span for="tinNummber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="openingBalance"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'tinNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="TIN#" /></label>
     <div class="controls">
         <input type="text" name="tinNummber" maxlength="20" value="${customerInstance?.tinNummber}" />
         <span for="tinNummber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="tinNummber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'panNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="PAN#" /></label>
     <div class="controls">
         <input type="text" name="panNummber" maxlength="20" value="${customerInstance?.panNummber}" />
         <span for="panNummber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="panNummber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'stNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="ST#" /></label>
     <div class="controls">
         <input type="text" name="stNummber" maxlength="20" value="${customerInstance?.stNummber}" />
         <span for="stNummber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="stNummber"></g:fieldError></span>
     </div>
 </div>
 
 <script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#customerForm").validate({
			rules:{
				customerName:{
					required:true
				},
				officeNumber:{
					digits:true
				},
				mobileNumber:{
					required:true,
					digits:true
				}
			},
			messages:{
				customerName:"Please enter Customer Name.",
				mobileNumber:"Please enter Mobile Number."		
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>