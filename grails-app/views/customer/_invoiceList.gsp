<%@ page import="com.billing.utility.ReceivedPaymentStatus" %>
<table class="table table-bordered data-table" style="width: 50%">
	<thead>
		<tr>
			<util:remoteSortableColumn action="customerDetails" onLoading="showSpinner(\'invoiceListTab\')" property="dateCreated" update="invoiceListTab" title="Invoice Date" defaultOrder="asc" params="[max:params?.max ?: 10, id : params?.id?:'']"/>
			<th style="text-align: right">Invoice #</th>
			<th style="text-align: right">Amount</th>
			<th style="text-align: center">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${invoiceList?.size() > 0 }">
				<g:each in="${invoiceList}" var="invoiceInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center"><g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.dateCreated }'/></td>
						<td style="text-align: right" width="10%">${invoiceInstance?.invoiceNumber }</td>
						<td  width="10%" style="text-align: right">${invoiceInstance?.netTotal}</td>
						<td width="1%" style="text-align: center">
							<g:link action="edit" controller="invoice" id="${invoiceInstance?.id}"><i class="icon-pencil" title="Edit"></i>&nbsp;</g:link>
							<a href="#myAlertCredit${i}" data-toggle="modal"><i class="icon-trash" title="Delete"></i>&nbsp;</a>
						</td>
						 <div id="myAlertCredit${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" controller="invoice" id="${invoiceInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="4" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right;margin-right: 50%" class="pagination">
<util:remotePaginate action="customerDetails" total="${invoiceListTotal}" 
			update="invoiceListTab" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[id:params?.id]" onLoading="showSpinner(\'invoiceListTab\')"/>
</div>

