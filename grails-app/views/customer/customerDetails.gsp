<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Customer Details</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
			              <g:if test="${flash.errorMessage}">
							<div class="alert alert-error">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Error!</strong> ${flash.errorMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<span class="icon" style="float:right"><g:link action="search" controller="customer" id="${customerInstance?.id}"><i class="icon-arrow-left" title="Cancel"></i>&nbsp;</g:link></span>
								<span class="icon" style="float:right"><g:link action="edit" controller="customer" id="${customerInstance?.id}"><i class="icon-pencil" title="Edit"></i>&nbsp;</g:link></span>
								<span class="icon" style="float:right">&nbsp;</span>								
								<h5>Customer Details</h5>
							</div>
							<g:form class="form-horizontal">
								<g:render template="details"></g:render>
							</g:form>
							 <div class="widget-box">
								<div class="widget-title">
									<ul class="nav nav-tabs">
										<li class="active"><g:remoteLink data-toggle="tab" fragment="invoiceListTab"  id="${params?.id}" action="customerDetails" update="invoiceListTab">Invoice</g:remoteLink></li>
										<li><g:remoteLink data-toggle="tab" fragment="creditNoteListTab"  id="${params?.id}" action="creditNoteDetails" update="creditNoteListTab">Credit Note</g:remoteLink></li>
										<li><g:remoteLink data-toggle="tab" fragment="receivedPaymentListTab"  id="${params?.id}" action="paymentDetails" update="receivedPaymentListTab">Payments</g:remoteLink></li>
									</ul>
								</div>
								<div class="widget-content tab-content">
								<div id="invoiceListTab" class="tab-pane active">
									<g:render template="invoiceList"></g:render>
								</div>
								
								<div id="creditNoteListTab" class="tab-pane">
									<g:render template="creditNoteList"></g:render>
								</div>
								
								<div id="receivedPaymentListTab" class="tab-pane">
									<g:render template="receivedPaymentList"></g:render>
								</div>
								</div>
							</div>
						</div><!-- End of widget-box -->
						<input type="button" onclick="window.location.href = '${createLink(action: 'search', controller: 'invoice')}'" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Cancel')}" />
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
