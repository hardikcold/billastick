<div class="row-fluid">
					<div class="span4">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th-list"></i>
								</span>
								<h5>Receipts </h5>
							</div>
							<div class="widget-content">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th style="text-align: left">Frequency</th>
											<th style="text-align: right">Amount (in Rs.)</th>
										</tr>
									</thead>
									<tbody>
										<g:each in="${totalReceipts}" var="key,value" status="i">
											<tr class="gradeX">
												<td>${key} </td>
												<td style="text-align: right" width="30%">${value?.get(2) }</td>
											</tr>
										</g:each>	
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="span4">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th-list"></i>
								</span>
								<h5>Sales</h5>
							</div>
							<div class="widget-content">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th style="text-align: left">Frequency</th>
											<th style="text-align: right">Amount (in Rs.)</th>
										</tr>
									</thead>
									<tbody>
										<g:each in="${totalSales}" var="key,value" status="i">
											<tr class="gradeX">
												<td>${key} </td>
												<td style="text-align: right" width="30%">${value?.get(2) }</td>
											</tr>
										</g:each>	
									</tbody>
								</table>
							</div>
						</div>
						</div>
					<div class="span4">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th-list"></i>
								</span>
								<h5>Dues</h5>
							</div>
							<div class="widget-content">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th style="text-align: left">Frequency</th>
											<th style="text-align: right">Amount (in Rs.)</th>
										</tr>
									</thead>
									<tbody>
										<g:each in="${totalDues}" var="key,value" status="i">
											<tr class="gradeX">
												<td>${key} </td>
												<td style="text-align: right" width="30%">${value?.get(2) }</td>
											</tr>
										</g:each>	
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>