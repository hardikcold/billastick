<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Invoice</title>
	</head>
	<body>
				<!-- End of content-header -->
			
               <div class="container-fluid">
					<div class="row-fluid">
					
						<g:render template="collectionDetails"></g:render>
					</div><!-- End of row-fluid -->
				</div><!-- End of container-fluid -->
	</body>
</html>
