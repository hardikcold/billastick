<%@page import="com.billing.utility.PaymentType"%>
<%@ page import="com.billing.utility.InvoiceStatus" %>
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn action="search" onLoading="showSpinner(\'estimateListDiv\')" property="estNumber" update="estimateListDiv" title="Estimate#" defaultOrder="asc" params="[max:params?.max ?: 10, searchText : params?.searchText?:'']"/>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right">Sub Total</th>
			<th style="text-align: right">Net Total</th>
			<th style="text-align: right">Estimate Date</th>
			<th style="text-align: right">Status</th>
			<th style="text-align: right">Payment Type</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${estimateInstanceList?.size() > 0 }">
				<g:each in="${estimateInstanceList}" var="estimateInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center">${estimateInstance?.estNumber }</td>
						<td>${estimateInstance?.customer.customerName }</td>
						<td style="text-align: right" width="10%">${estimateInstance?.subTotal}</td>
						<td style="text-align: right" width="10%">${estimateInstance?.netTotal}</td>
						<td style="text-align: right" width="10%"><g:formatDate format='dd/MM/yyyy' date='${estimateInstance?.estimateDate}'/></td>
						<td width="10%" class="taskStatus">
							<g:if test="${estimateInstance?.estimateStatus == InvoiceStatus.Closed}">
								<span class="invoiced">${InvoiceStatus.Closed}</span>
							</g:if>
							<g:if test="${estimateInstance?.estimateStatus == InvoiceStatus.Open}">
								<span class="draft">${InvoiceStatus.Open}</span>
							</g:if>
							<g:if test="${estimateInstance?.estimateStatus == InvoiceStatus.Cancelled}">
								<span class="cancelled">${InvoiceStatus.Cancelled}</span>
							</g:if>
							<g:if test="${estimateInstance?.estimateStatus == InvoiceStatus.Overdue}">
								<span class="overdue">${InvoiceStatus.Overdue}</span>
							</g:if>
							<g:if test="${estimateInstance?.estimateStatus == InvoiceStatus.Refundable}">
								<span class="refundable">${InvoiceStatus.Refundable}</span>
							</g:if>
						</td>
						<td style="text-align: right" width="10%" >${estimateInstance?.paymentType}</td>
						<td width="5%">
							 <div class="btn-group">
									  <button class="btn btn-primary">Action</button>
									  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></button>
									  <ul class="dropdown-menu">
									  	<li><g:link action="edit" controller="estimate" id="${estimateInstance?.id}"><i class="icon-pencil"></i>&nbsp;Edit</g:link></li>
										<li><a href="#estimateDeleteAlert${i}" data-toggle="modal"><i class="icon-trash"></i>&nbsp;Delete</a></li>
										<li><g:link id="${estimateInstance?.id}" controller="invoice" action="convertToInvoice"><i class="icon-retweet"></i>&nbsp;Convert to Invoice</g:link></li>
										<li><g:link id="${estimateInstance?.id}" controller="estimate" action="estimateDetails"><i class="icon-th-list"></i>&nbsp;Details</g:link></li>
										<li><g:link id="${estimateInstance?.id}" controller="estimate" action="printEstimate"><i class="icon-print"></i>&nbsp;Print</g:link></li>
									  </ul>
								</div>
                                <div id="estimateDeleteAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Once you choose to delete, the details of the estimate will be removed from the system forever and you will not be able to retrieve it later. Are you sure about deleting this estimate?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${estimateInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
						</td>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="9" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="search" total="${estimateInstanceListTotal}" 
			update="estimateListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[searchText : params?.searchText?:'']" onLoading="showSpinner(\'estimateListDiv\')"/>
</div>

