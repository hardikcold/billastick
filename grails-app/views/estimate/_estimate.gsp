<div class="content">

	<div class="page-header">
		<div class="icon">
			<span class="ico-dollar"></span>
		</div>
		<h3>
			Estimate #${estimateInstance?.estNumber} 
		</h3>
	</div>

	<div class="row-fluid">
		<div class="span12">

			<div class="block">
				<div class="data invoice">

					<div class="row-fluid">
						<div class="span3">
							<address>
								<strong>${estimateInstance?.user?.displayName}</strong>
								<g:if test="${estimateInstance?.user?.address1}">
								<br> ${estimateInstance?.user?.address1}
								</g:if>
								<g:if test="${estimateInstance?.user?.address2}">
								<br> ${estimateInstance?.user?.address2}
								</g:if>
								<g:if test="${estimateInstance?.user?.city}">
								<br>${estimateInstance?.user?.city}
								</g:if>
								<g:if test="${estimateInstance?.user?.mobileNumber}">
								<br>Mobile:	${estimateInstance?.user?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3">
							<address>
								<strong>${estimateInstance?.customer?.customerName}</strong>
								<g:if test="${estimateInstance?.customer?.address1}">
								<br> ${estimateInstance?.customer?.address1}
								</g:if>
								<g:if test="${estimateInstance?.customer?.address2}">
								<br> ${estimateInstance?.customer?.address2} 
								</g:if>
								<g:if test="${estimateInstance?.customer?.city}">
								<br>${estimateInstance?.customer?.city}
								</g:if>
								<g:if test="${estimateInstance?.customer?.mobileNumber}">
								<br>Phone :  ${estimateInstance?.customer?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3"></div>
						<div class="span3">
								<strong>Estimate Date:</strong> <g:formatDate format='dd/MM/yyyy' date='${estimateInstance?.estimateDate}'/><br>
								<strong>Expire Date:</strong> <g:formatDate format='dd/MM/yyyy' date='${estimateInstance?.expireDate}'/> 
							<div class="highlight">
								<strong>Estimate Amount:</strong> ${estimateInstance?.netTotal}  <em>INR</em>
							</div>
						</div>
					</div>

					<h4>Description</h4>
					<table class="table" width="100%">
						<thead>
							<tr>
								<th width="60%" style="text-align: left">Description</th>
								<th width="10%" style="text-align: right">Price</th>
								<th width="10%" style="text-align: right">Quantity</th>
								<th width="10%" style="text-align: right">Discount</th>
								<th width="10%" style="text-align: right">Tax1</th>
								<th width="10%" style="text-align: right">Tax2</th>
								<th width="10%" style="text-align: right">Tax3</th>
								<th width="10%" style="text-align: right">Total</th>
							</tr>
						</thead>
						<tbody>
						<g:each in="${estimateInstance?.estimateDetails}" var="detailsInstance">
							<tr>
								<td>${detailsInstance?.item?.itemName }</td>
								<td style="text-align: right">${detailsInstance?.rate}</td>
								<td style="text-align: right">${detailsInstance?.quantity}</td>
								<td style="text-align: right">${detailsInstance?.discountRate}%</td>
								<td style="text-align: right">${detailsInstance?.tax1 == null ? 'N/A' : detailsInstance?.tax1?.taxName + '@'+ detailsInstance?.tax1?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.tax2 == null ? 'N/A' : detailsInstance?.tax2?.taxName + '@'+ detailsInstance?.tax2?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.tax3 == null ? 'N/A' : detailsInstance?.tax3?.taxName + '@'+ detailsInstance?.tax3?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.total }</td>
							</tr>
						</g:each>	
						</tbody>
					</table>

					<div class="row-fluid">
						<div class="span9"></div>
						<div class="span3">
							<table class="table" width="100%">
								<tr ><td><span><b>Sub Total:</b></span></td><td style="text-align: right">${estimateInstance?.subTotal}</td></tr>
								<g:each in="${estimateInstance?.estimateTaxTransactionDetails}" var="taxTransactionInstance">
									<tr ><td><span>${taxTransactionInstance?.tax?.taxName + '@'+ taxTransactionInstance?.tax?.taxRate + '%'}:</span></td><td style="text-align: right">${taxTransactionInstance?.taxAmount}</td></tr>	
								</g:each>
								<tr class="highlight"><td><span><b>Net Total:</b></span></td><td style="text-align: right"> ${estimateInstance?.netTotal}</td></tr>
							</table>	
						</div>
					</div>

				</div>

			</div>

		</div>
	</div>

</div>