
<label class="control-label"><g:message code="tax.taxName.label"
		default="Invoice#" /></label>
<div class="controls">
	<input type="text" name="searchText"
		onkeyup="${remoteFunction(action: 'search',
						                       update: 'invoiceListDiv',
						                       params: '\'searchText=\' + this.value')}" />
</div>
<label class="control-label"><g:message code="tax.taxName.label"
		default="From" /></label>
<div class="controls">
	<input type="text" class="uptoTodayDatepicker" id="invoiceDate"
		name="invoiceDate" size="10"
		value="<g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/>" />
</div>
<label class="control-label"><g:message code="tax.taxName.label"
		default="To" /></label>
<div class="controls">
	<input type="text" class="uptoTodayDatepicker" id="invoiceDate"
		name="invoiceDate" size="10"
		value="<g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/>" />
</div>