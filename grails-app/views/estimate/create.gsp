<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Add Estimate</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
			              <g:if test="${flash.errorMessage}">
							<div class="alert alert-error">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Error!</strong> ${flash.errorMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create Estimate</h5>
							</div>
							<div class="widget-content nopadding">
								<g:form action="save" class="form-horizontal" name="estimateForm">
                                   <g:render template="form"></g:render>
                                   <div class="form-actions">
									     <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Save')}" />
										<input type="button" onclick="window.location.href = '${createLink(action: 'search', controller: 'estimate')}'" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Cancel')}" />
									 </div>  
                                </g:form>
                                <g:form name="customerPopupForm" action="saveCustomerPopupForEstimate" controller="customer" class="form-horizontal">
                                	<div class="modal hide" id="modal-add-event">
										 <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">×</button>
											<h3>Add a new customer</h3>
										</div>
										<div class="modal-body">
											<div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'customerName', 'error')}">
											     <label class="control-label"><g:message code="tax.taxName.label" default="Customer Name" /></label>
											     <div class="controls">
											         <input type="text" name="customerName" maxlength="100" value="${customerInstance?.customerName}" />
											         <span for="customerName" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="customerName"></g:fieldError></span>
											     </div>
											 </div>
											 <div class="control-group  fieldcontain ${hasErrors(bean: customerInstance, field: 'mobileNumber', 'error')}">
											     <label class="control-label"><g:message code="tax.taxName.label" default="Mobile#" /></label>
											     <div class="controls">
											         <input type="text" name="mobileNumber" id="mobileNumber" maxlength="30" value="${customerInstance?.mobileNumber}" />
											         <span for="mobileNumber" generated="true" class="help-inline"><g:fieldError bean="${customerInstance}" field="mobileNumber"></g:fieldError></span>
											     </div>
											 </div>
 
										</div>
										<div class="form-actions" style="text-align: right">
										    <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Save')}" />
											<input type="button" data-dismiss="modal" onclick="window.location.href = '${createLink(action: 'create', controller: 'estimate')}'" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Cancel')}" />
									 </div>  
									</div>
                                </g:form>
							</div>
						</div><!-- End of widget-box -->	
						
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	 <script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#customerPopupForm").validate({
			rules:{
				customerName:{
					required:true
				},
				officeNumber:{
					digits:true
				},
				mobileNumber:{
					required:true,
					digits:true
				}
			},
			messages:{
				customerName:"Please enter Customer Name.",
				mobileNumber:"Please enter Mobile Number."		
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>
	</body>
</html>
