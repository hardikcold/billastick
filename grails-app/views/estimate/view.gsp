<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Estimate</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
						
						<div class="widget-box">
				          <div class="widget-title">
				             <span class="icon"><i class="icon-th"></i></span> 
				            <h5>Estimate</h5>
				          </div>
				          <div class="widget-content nopadding" id="estimateListDiv">
				           	<g:render template="estimate"></g:render>
				          </div>
				        </div><!-- End of widget-box -->
				        
				        
				</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
