<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>

	<meta name="billastick" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="online invoice system" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>Multiplus | Responsive Landing page Template</title>
	
   
    <!-- CSS-->
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'css/landing', file: 'reset.css')}" />
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'css/landing', file: 'foundation.css')}" />
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'css/landing', file: 'style.css')}" />
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'css/landing', file: 'elastislide.css')}" />
	<link rel="stylesheet" type="text/css" href="${resource(dir: 'css/landing', file: 'prettyphoto.css')}" />
	<link rel="stylesheet" href="${resource(dir: 'css/landing', file: 'foundation-icons.css')}" media="screen" />
	<link rel="stylesheet" href="${resource(dir: 'css/landing', file: 'foundation-icons2.css')}" media="screen" />
	<link rel="stylesheet" href="${resource(dir: 'css/landing', file: 'flexslider.css')}" media="screen" />
	<link rel="stylesheet" href="${resource(dir: 'css/landing', file: 'pricing_table.css')}" media="screen" /><%--

   
    <!-- FAVICON-->
	<link rel="shortcut icon" href="images/landing/favicon.ico">

    --%>
    <!-- //SCRIPTS -->
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'jquery-1.7.2.min.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'jquery.easing.1.3.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'jquery.elastislide.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'jquery.tweet.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'jquery.prettyphoto.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'jquery.foundation.reveal.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'contact.js')}"></script>
	<script type="text/javascript" src="${resource(dir: 'js/landing', file: 'main.js')}"></script>

    
	
</head>
<body>
<!--Topbar contact section-->
<div id="droptop">
	<div class="topbar">
		<!-- Map Placeholder -->
		<iframe class="map-placeholder" width="100%" height="550" src=""  frameborder="0"></iframe>
		<!-- Map Placeholder -->
		<div class="row">
			<div class="twelve columns">
				<!--Contact form section-->
				<div id="contact-wrap">
					<form id="contactForm" action="http://counterpart.x10.mx/html/multiplus/php/contact.php" method="post">
						<ul>
							<li>
								<label for="senderName">Your Name</label>
								<input type="text" name="senderName" id="senderName" placeholder="Please type your name" required="required" maxlength="40" />
							</li>

							<li>
								<label for="senderEmail">Your Email Address</label>
								<input type="email" name="senderEmail" id="senderEmail" placeholder="Please type your email address" required="required" maxlength="50" />
							</li>

							<li>
								<label for="message">Your Message</label>
								<textarea name="message" id="message" placeholder="Please type your message" required="required" cols="80" rows="10" maxlength="10000"></textarea>
							</li>
						</ul>
							
						<div id="formButtons">
							<input type="submit" id="sendMessage" name="sendMessage" value="Send Email" />
						</div>
					</form>
									
				</div>
				<!--Contact form section-->
			</div>
		</div>
	</div>

	<div id="topline"></div>

	<div class="row">

		<a href="#" class="topbar-btn" onclick="return false;"></a>

		<div class="topwrap">
			<div class="login"><a href="#" data-reveal-id="loginform"><i class="foundicon1-unlock login-ico"></i> Login</a></div>
			<div class="register"><a href="#" data-reveal-id="signupform"><i class="foundicon1-people register-ico"></i> Sign Up</a></div>
		</div>
	</div>
</div>
<!--Topbar contact section-->

<div class="row">

<!-- Modal Section -Login -->
<div id="loginform" class="reveal-modal">
	<h3>Members Login</h3>
	<p>Faworki cotton candy sweet cupcake applicake topping liquorice. Gummies cheesecake toffee cake.</p>
	<div class="space20"></div>
	<g:form name="loginform" action="signIn" controller="auth">
		<fieldset>
			<label><strong>Email</strong></label>
			<input type="text" required="required" placeholder="E-mail address" name="username" id="username" />
			<label><strong>Password</strong></label>
			<input type="password" required="required" placeholder="Password" name="password" id="password"/>
			<div class="space10"></div>
			<input type="submit" class="radius button" value="Login" />
			<div class="space10"></div>
			<p><a href="#">Forgot password?</a> Click here.</p>
		</fieldset>
	</g:form>
	<a class="close-reveal-modal"><i class="foundicon1-remove"></i></a>
</div>
<!-- Modal Section -Login -->

<!-- Modal Section -Register -->
<div id="signupform" class="reveal-modal">
	<h3>Sign up. It's free</h3>
	<p>Register and get premium services for free.</p>
	<div class="space20"></div>
	<g:form name="registerForm" action="registerUser" controller="auth">
		<fieldset>
			<label><strong>Email</strong></label>
			<input type="text" required="required" placeholder="E-mail address" name="username" id="username"/>
			<label><strong>Password</strong></label>
			<input type="password" required="required" placeholder="Password" name="passwordHash" id="passwordHash"/>
			<label><strong>Organization Name</strong></label>
			<input type="text" required="required" placeholder="Organization Name" name="organizationName" id="organizationName" />
			<label><strong>First Name</strong></label>
			<input type="text" required="required" placeholder="First Name" name="firstName" id="firstName" />
			<label><strong>Last Name</strong></label>
			<input type="text" required="required" placeholder="Last Name" name="lastName" id="lastName" />
			<label><strong>Mobile #</strong></label>
			<input type="text" required="required" placeholder="Mobile Number" name="mobileNumber" id="mobileNumber" />
			<div class="space10"></div>
			<p>Check our <a href="#">terms and conditions</a> before sign up. If you familiar with our terms, go ahead click register me button.</p>
			<div class="space10"></div>
			<input type="submit" class="radius button" value="Register me" />
		</fieldset>
	</g:form>
	<a class="close-reveal-modal"><i class="foundicon1-remove"></i></a>
</div>
<!-- Modal Section -Register -->

<!-- Header Section -->
<div id="header">
	<div class="header-wrap row">
		<div class="four columns">
			<!-- Logo Section -->
			<div class="twelve columns logo">
				<h1><a href="index-2.html"><img src="${resource(dir: 'images/landing', file: 'logo.png')}" alt=""></a></h1>
			</div>
			<!-- Logo Section -->
		</div>

		
	</div>
</div>
<!-- Header Section -->

<div class="twelve columns">
<div class="wrap">
	<h4>Product Features</h4>
	<div class="bubble-white"></div>
	
	<div class="space40"></div>
			
		<!-- Features wrap Section -->
		<div class="twelve columns">
			<div id="features-wrap">
				<div class="four columns features-content">
					<div class="features-icon"><i class="foundicon1-flag"></i></div>
					<div class="features-description">
						<h6>Instant Invoicing</h6>
						<p>Manage all your invoices in a snapshot and keep a track of payments from your clients, all at one place.</p>
					</div>
				</div>

				<div class="four columns features-content">
					<div class="features-icon"><i class="foundicon1-idea"></i></div>
					<div class="features-description">
						<h6>Your own Branding and Logo </h6>
						<p>Customize header color & logo of your invoice and leave a professional impression on your clients.</p>
					</div>
				</div>

				<div class="four columns features-content">
					<div class="features-icon"><i class="foundicon1-globe"></i></div>
					<div class="features-description">
						<h6>Import-Export Your Data</h6>
						<p>Import export your existing customer and product data from Excel table. Use the built-in import wizard.</p>
					</div>
				</div>
			</div>
		</div>
		<!-- Features wrap Section -->

	</div>
</div>

<div class="clear"></div>
<div class="space80"></div>
<div class="clear"></div>

<div class="twelve columns">
	<div class="wrap">
		<h4>Take a look at our screenshots</h4>
		<div class="bubble-white"></div>
		<div class="space40"></div>

		<!-- Screenshot Carousel section -->
		<div id="carousel" class="es-carousel-wrapper">
			<div class="es-carousel">
				<ul>
					<li><a href="demo/mason/1.png" rel="prettyPhoto" title=""><img src="demo/mason/1.png" alt="" /></a></li>
					<li><a href="demo/mason/2.png" rel="prettyPhoto" title=""><img src="demo/mason/2.png" alt="" /></a></li>
					<li><a href="demo/mason/3.png" rel="prettyPhoto" title=""><img src="demo/mason/3.png" alt="" /></a></li>
					<li><a href="demo/mason/4.png" rel="prettyPhoto" title=""><img src="demo/mason/4.png" alt="" /></a></li>
					<li><a href="demo/mason/5.png" rel="prettyPhoto" title=""><img src="demo/mason/5.png" alt="" /></a></li>
					<li><a href="demo/mason/6.png" rel="prettyPhoto" title=""><img src="demo/mason/6.png" alt="" /></a></li>
					<li><a href="demo/mason/7.png" rel="prettyPhoto" title=""><img src="demo/mason/7.png" alt="" /></a></li>
					<li><a href="demo/mason/8.png" rel="prettyPhoto" title=""><img src="demo/mason/8.png" alt="" /></a></li>
					<li><a href="demo/mason/9.png" rel="prettyPhoto" title=""><img src="demo/mason/9.png" alt="" /></a></li>
					
				</ul>
			</div>
		</div>
		<!-- Screenshot Carousel section -->

	</div>
</div>

<div class="clear"></div>
<div class="space90"></div>
<div class="clear"></div>

<div class="twelve columns">
	<div class="wrap">
		<h4>Pricing</h4>
		<div class="bubble-white"></div>
		<div class="space60"></div>

		<!-- Pricing Table section -->
		<div id='pricing_plan1' class="three columns">
			<dl class='plans' >
				<dd class="plan_title">Free</dd>
				<dd class="plan_price">-</dd>
			</dl>

			<dl class='plan' id="one">
				<dt class='plan_more'>View<a href="#one" class="more_icon"></a><a href="#" class="less_icon"></a></dt>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Customers</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Items</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">25 Invoices</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Reports</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">24 X 7 Support</div>
				</dd>
				<dd class="plan_buy">
					<a href='#' class='buy' >Buy Now</a>
				</dd>
			</dl>
		</div>

		<div id='pricing_plan2' class="three columns">
			<dl class='plans'>
				<dt class="plan_title">Standard</dt>
				<dd class="plan_price">4000</dd>
			</dl>

			<dl class='plan' id="two">
				<dt class='plan_more'>View<a href="#two" class="more_icon"></a><a href="#" class="less_icon"></a></dt>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Customers</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Items</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">500 Invoices Per Month</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Reports</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">24 X 7 Support</div>
				</dd>
				<dd class="plan_buy">
					<a href='#' class='buy' >Buy Now</a>
				</dd>
			</dl>
		</div>

		<div id='pricing_plan3' class="three columns">
			<dl class='plans'>
				<dt class="plan_title">Pro</dt>
				<dd class="plan_price">6000</dd>
			</dl>
		
			<dl class='plan' id="three">
				<dt class='plan_more'>View<a href="#three" class="more_icon"></a><a href="#" class="less_icon"></a></dt>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Customers</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Items</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">1000 Invoices Per Month</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Reports</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">24 X 7 Support</div>
				</dd>
				<dd class="plan_buy">
					<a href='#' class='buy' >Buy Now</a>
				</dd>
			</dl>
		</div>

		<div id='pricing_plan4' class="three columns">
			<dl class='plans'>
				<dt class="plan_title">Gold</dt>
				<dd class="plan_price">10,000</dd>
			</dl>

			<dl class='plan' id="four">
				<dt class='plan_more'>View<a href="#four" class="more_icon"></a><a href="#" class="less_icon"></a></dt>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Customers</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Items</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Unlimited Invoices</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">Reports</div>
				</dd>
				<dd class="plan_features">
					<div class="feature_desc">24 X 7 Support</div>
				</dd>
				<dd class="plan_buy">
					<a href='#' class='buy' >Buy Now</a>
				</dd>
			</dl>
		</div>
		<!-- Pricing Table section -->
	</div>
</div>

<div class="clear"></div>
<div class="space90"></div>
<div class="clear"></div>

<div class="row">
	<div class="twelve columns">
		<div class="wrap">
			<h4>FAQ Section</h4>
			<div class="bubble-white"></div>
			<div class="space40"></div>

			<!-- FAQ section -->
			<div class="six columns toggle-wrap">
				<ul id="toggle-view">
					<li>
						<h3>Is it possible to convert Estimate to Invoice?</h3>
						<span>+</span>
						<div class="panel">
							<p>Customer can convert Estimate to Invoice. For that customer need to click on Conver to Invoice link from action menu of estimation list.</p>
						</div>
					</li>
				
					<li>
						<h3>Why do we use it?</h3>
						<span>+</span>
						<div class="panel">
							<p>It will give you the details of sales transactions, received payments, refunds, customer dues and more.</p>
						</div>
					</li>

					<li>
						<h3>Who can use this system ?</h3>
						<span>+</span>
						<div class="panel">
							<p>Anyone who wants to keep track of all their business's sales transaction. By using this system organization will get total dues amount which they collect from customers, can create invoice, can create different tax and assigned it to newly created items.</p>
						</div>
					</li>

				</ul>
			</div>
			<!-- FAQ section -->

			

		</div>
	</div>
</div>


<div class="clear"></div>
<div class="space50"></div>
<div class="clear"></div>


</div>

<!-- Footer copyright Section -->
<div id="footer-bottom">
	<div class="row footer-bottom">
		<div id="footer-bottom-wrap">
			<div class="six columns copyright">
				<p>Copyright 2013. <a href="#">Billastick</a></p>
			</div>

		</div>
	</div>
</div>
<!-- Footer copyright Section -->

<!-- Back to top button Section -->
<p id="back-top">
	<a href="#top"><span></span></a>
</p>
<!-- Back to top button Section -->

</body>
</html>