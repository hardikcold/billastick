<%@page import="com.billing.utility.PaymentType"%>
<%@ page import="com.billing.utility.InvoiceStatus" %>
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn action="search" onLoading="showSpinner(\'invoiceListDiv\')" property="invoiceNumber" update="invoiceListDiv" title="Invoice#" defaultOrder="asc" params="[max:params?.max ?: 10, searchText : params?.searchText?:'']"/>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right">Sub Total</th>
			<th style="text-align: right">Net Total</th>
			<th style="text-align: right">Balance Due</th>
			<th style="text-align: right">Invoice Date</th>
			<th style="text-align: right">Status</th>
			<th style="text-align: right">Payment Type</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${invoiceInstanceList?.size() > 0 }">
				<g:each in="${invoiceInstanceList}" var="invoiceInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center">${invoiceInstance?.invoiceNumber }</td>
						<td>${invoiceInstance?.customer.customerName }</td>
						<td style="text-align: right" width="10%">${invoiceInstance?.subTotal}</td>
						<td style="text-align: right" width="10%">${invoiceInstance?.netTotal}</td>
						<td  width="10%" style="text-align: right">${invoiceInstance?.totalBalanceDue}</td>
						<td style="text-align: right" width="10%"><g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/></td>
						<td width="10%" class="taskStatus">
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Closed}">
								<span class="invoiced">${InvoiceStatus.Closed}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Open}">
								<span class="draft">${InvoiceStatus.Open}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Cancelled}">
								<span class="cancelled">${InvoiceStatus.Cancelled}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Overdue}">
								<span class="overdue">${InvoiceStatus.Overdue}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Refundable}">
								<span class="refundable">${InvoiceStatus.Refundable}</span>
							</g:if>
						</td>
						<td style="text-align: right" width="10%" >${invoiceInstance?.paymentType}</td>
						<td width="5%">
							 <div class="btn-group">
									  <button class="btn btn-primary">Action</button>
									  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></button>
									  <ul class="dropdown-menu">
									  	<g:if test="${invoiceInstance?.paymentDone == false }">
									  		<li><g:link action="edit" controller="invoice" id="${invoiceInstance?.id}"><i class="icon-pencil"></i>&nbsp;Edit</g:link></li>
									  	</g:if>
									 	<g:if test="${invoiceInstance?.totalBalanceDue > 0}">
											<li><g:link id="${invoiceInstance?.id}" controller="receivedPayment" action="received"><i class="icon-briefcase"></i>&nbsp;Received Payment</g:link></li>
										</g:if>
										<li><g:link id="${invoiceInstance?.id}" controller="creditNote" action="create"><i class="icon-check"></i>&nbsp;Create Note</g:link></li>
										<g:if test="${invoiceInstance?.totalBalanceDue < 0}">
											<li><g:link id="${invoiceInstance?.id}" controller="refund" action="create"><i class="icon-retweet"></i>&nbsp;Refund</g:link></li>
										</g:if>
										<li><a href="#myAlert${i}" data-toggle="modal"><i class="icon-trash"></i>&nbsp;Delete</a></li>
										<li><g:link id="${invoiceInstance?.id}" controller="invoice" action="invoiceDetails"><i class="icon-th-list"></i>&nbsp;Details</g:link></li>
										<li><g:link id="${invoiceInstance?.id}" controller="invoice" action="printInvoice"><i class="icon-print"></i>&nbsp;Print</g:link></li>
									  </ul>
								</div>
                                <div id="myAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Once you choose to delete, the details of the invoice will be removed from the system forever and you will not be able to retrieve it later. Are you sure about deleting this invoice?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${invoiceInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
						</td>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="9" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="search" total="${invoiceInstanceListTotal}" 
			update="invoiceListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[searchText : params?.searchText?:'']" onLoading="showSpinner(\'invoiceListDiv\')"/>
</div>

