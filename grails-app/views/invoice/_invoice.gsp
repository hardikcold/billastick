<div class="content">

	<div class="page-header">
		<div class="icon">
			<span class="ico-dollar"></span>
		</div>
		<h3>
			Invoice #${invoiceInstance?.invoiceNumber} 
		</h3>
	</div>

	<div class="row-fluid">
		<div class="span12">

			<div class="block">
				<div class="data invoice">

					<div class="row-fluid">
						<div class="span3">
							<address>
								<strong>${invoiceInstance?.user?.displayName}</strong>
								<g:if test="${invoiceInstance?.user?.address1}">
								<br> ${invoiceInstance?.user?.address1}
								</g:if>
								<g:if test="${invoiceInstance?.user?.address2}">
								<br> ${invoiceInstance?.user?.address2}
								</g:if>
								<g:if test="${invoiceInstance?.user?.city}">
								<br>${invoiceInstance?.user?.city}
								</g:if>
								<g:if test="${invoiceInstance?.user?.mobileNumber}">
								<br>Mobile:	${invoiceInstance?.user?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3">
							<address>
								<strong>${invoiceInstance?.customer?.customerName}</strong>
								<g:if test="${invoiceInstance?.customer?.address1}">
								<br> ${invoiceInstance?.customer?.address1}
								</g:if>
								<g:if test="${invoiceInstance?.customer?.address2}">
								<br> ${invoiceInstance?.customer?.address2} 
								</g:if>
								<g:if test="${invoiceInstance?.customer?.city}">
								<br>${invoiceInstance?.customer?.city}
								</g:if>
								<g:if test="${invoiceInstance?.customer?.mobileNumber}">
								<br>Phone :  ${invoiceInstance?.customer?.mobileNumber}
								</g:if>
							</address>
						</div>
						<div class="span3"></div>
						<div class="span3">
								<strong>Invoice Date:</strong> <g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/><br>
								<strong>Due Date:</strong> <g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.dueDate}'/> 
							<div class="highlight">
								<strong>Invoice Amount:</strong> ${invoiceInstance?.netTotal}  <em>INR</em>
							</div>
							<div class="highlight">
								<strong>Amount Due:</strong> ${invoiceInstance?.totalBalanceDue}  <em>INR</em>
							</div>
						</div>
					</div>

					<h4>Description</h4>
					<table class="table" width="100%">
						<thead>
							<tr>
								<th width="60%" style="text-align: left">Description</th>
								<th width="10%" style="text-align: right">Price</th>
								<th width="10%" style="text-align: right">Quantity</th>
								<th width="10%" style="text-align: right">Discount</th>
								<th width="10%" style="text-align: right">Tax1</th>
								<th width="10%" style="text-align: right">Tax2</th>
								<th width="10%" style="text-align: right">Tax3</th>
								<th width="10%" style="text-align: right">Total</th>
							</tr>
						</thead>
						<tbody>
						<g:each in="${invoiceInstance?.invoiceDetails}" var="detailsInstance">
							<tr>
								<td>${detailsInstance?.item?.itemName }</td>
								<td style="text-align: right">${detailsInstance?.rate}</td>
								<td style="text-align: right">${detailsInstance?.quantity}</td>
								<td style="text-align: right">${detailsInstance?.discountRate}%</td>
								<td style="text-align: right">${detailsInstance?.tax1 == null ? 'N/A' : detailsInstance?.tax1?.taxName + '@'+ detailsInstance?.tax1?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.tax2 == null ? 'N/A' : detailsInstance?.tax2?.taxName + '@'+ detailsInstance?.tax2?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.tax3 == null ? 'N/A' : detailsInstance?.tax3?.taxName + '@'+ detailsInstance?.tax3?.taxRate + '%'}</td>
								<td style="text-align: right">${detailsInstance?.total }</td>
							</tr>
						</g:each>	
						</tbody>
					</table>

					<div class="row-fluid">
						<div class="span9"></div>
						<div class="span3">
							<table class="table" width="100%">
								<tr ><td><span><b>Sub Total:</b></span></td><td style="text-align: right">${invoiceInstance?.subTotal}</td></tr>
								<g:each in="${invoiceInstance?.taxTransactionDetails}" var="taxTransactionInstance">
									<tr ><td><span>${taxTransactionInstance?.tax?.taxName + '@'+ taxTransactionInstance?.tax?.taxRate + '%'}:</span></td><td style="text-align: right">${taxTransactionInstance?.taxAmount}</td></tr>	
								</g:each>
								<tr class="highlight"><td><span><b>Net Total:</b></span></td><td style="text-align: right"> ${invoiceInstance?.netTotal}</td></tr>
								 <tr>
					                 <td><span style="color:red">Credit Applied:</span></td>
					                 <td>
					                 <div id="creditUsedDiv" style="float:right">
					                 	(${totalCreditAppliedAmount})
					                 </div>
					                 <g:hiddenField name="hiddenTotalCreditApplied" id="hiddenTotalCreditApplied" value="${totalCreditAppliedAmount}"/>
					                 </td>
					                 
					              </tr> 
					              <tr>
					                 <td><span style="color:red">Payment Received:</span></td>
					                 <td>
					                 <div id="receiptsDiv" style="float:right">
					                 	(${totalReceiptsAmount })
					                 </div>
					                 <g:hiddenField name="hiddenTotalReceipts" id="hiddenTotalReceipts" value="${totalReceiptsAmount}"/>
					                 </td>
					                 
					              </tr> 
					               <tr>
					                 <td><span style="color:green">Refunded:</span></td>
					                 <td>
					                 <div id="refundDiv" style="float:right">
					                 	${totalRefundedAmount }
					                 </div>
					                 <g:hiddenField name="hiddenTotalRefunds" id="hiddenTotalRefunds" value="${totalRefundedAmount}"/>
					                 </td>
					                 
					              </tr> 
								<tr class="highlight"><td><span><b>Balance Due:</b></span></td><td style="text-align: right"> ${invoiceInstance?.totalBalanceDue}</td></tr>
							</table>	
								<%--<p>
									<strong><span>Subtotal:</span>  ${invoiceInstance?.subTotal} <em>INR</em></strong>
								</p>
								<div class="highlight">
									<strong><span>Total:</span>  ${invoiceInstance?.netTotal} <em>INR</em></strong>
								</div>
							--%>
						</div>
					</div>

				</div>

			</div>

		</div>
	</div>

</div>