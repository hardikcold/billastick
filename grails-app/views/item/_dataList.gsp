<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn style="text-align: left" action="list" onLoading="showSpinner(\'itemListDiv\')" property="itemName" update="itemListDiv" title="Name" defaultOrder="desc" params="[max:params?.max ?: 10]"/>
			<th style="text-align: right">Item Name</th>
			<th style="text-align: right">Tax Name</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${itemInstanceList?.size() > 0 }">
				<g:each in="${itemInstanceList}" var="itemInstance" status="i">
					<tr class="gradeX">
						<td>${itemInstance?.itemName } - ${itemInstance?.measurement?.measurementCode }</td>
						<td style="text-align: right" width="15%">${itemInstance?.rate?:0.00}</td>
						<td style="text-align: right" width="15%">${itemInstance?.tax?.name?:"N/A"}</td>
						<td width="5%" style="text-align: right">
							<g:link action="edit" controller="item" id="${itemInstance?.id}"><i class="icon-pencil" title="Edit"></i>&nbsp;</g:link>
								<a href="#myAlert${i}" data-toggle="modal"><i class="icon-trash" title="Delete"></i>&nbsp;</a>
						</td>
						 <div id="myAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${itemInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="3" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="list" total="${itemInstanceListTotal}" 
			update="itemListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" onLoading="showSpinner(\'itemListDiv\')"/>
</div>

