<%@page import="com.billing.domain.Tax"%>
<%@ page import="com.billing.domain.Item" %>


 
 <div class="control-group  fieldcontain ${hasErrors(bean: itemInstance, field: 'itemName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Item Name" /></label>
     <div class="controls">
         <input type="text" name="itemName" maxlength="30" value="${itemInstance?.itemName}" />
         <span for="itemName" generated="true" class="help-inline"><g:fieldError bean="${itemInstance}" field="itemName"></g:fieldError></span>
     </div>
 </div>
 <div class="control-group fieldcontain ${hasErrors(bean: itemInstance, field: 'rate', 'error')}">
     <label class="control-label"><g:message code="tax.taxRate.label" default="Item Rate" /></label>
     <div class="controls">
         <input type="text"  name="rate" id="rate" value="${itemInstance?.rate}" onkeyup="onlyDecimal(this, 15)"/>
         <span class="help-inline" for="rate" generated="true"><g:fieldError bean="${itemInstance}" field="rate"></g:fieldError></span>
     </div>
 </div>
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxRate.label" default="Measurement" /></label>
     <div class="controls">
        <g:select id="measurement" name="measurement.id" from="${measurementList}" value="${itemInstance?.measurement?.id}" optionKey="id" optionValue="displayName"/>
     </div>
 </div>

 <div class="control-group  fieldcontain ${hasErrors(bean: itemInstance, field: 'description', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Item Description" /></label>
     <div class="controls">
         <textarea name="description" maxlength="100">${itemInstance?.description}</textarea>
     </div>
 </div>
  <div class="control-group">
     <label class="control-label">Select Tax </label>
     <div class="controls">
     	<g:each in="${taxList}" status="i" var="taxInstance">
        	  <g:checkBox name="taxName" value="${taxInstance?.id}" checked="${itemInstance?.tax?.id?.contains(taxInstance?.id) == true ? 'true' : 'false' }"/><span style="margin-top: 10px;">&nbsp;${taxInstance?.name}</span>
                <g:if test="${ (i+1) % 3 == 0}">
                <br>
                </g:if>
        </g:each>
        <br>	 
     </div>
 </div>
 
<script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#itemForm").validate({
			rules:{
				itemName:{
					required:true
				},
				rate:{
					required:true
				}
			},
			messages:{
				itemName:"Please enter Item Name.",
				rate:"Please enter Item Rate.",
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>

