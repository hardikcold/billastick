<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-th-list"></i> Validation</a>
			<ul>
    <li ><g:link controller="dashboard"><i class="icon icon-home"></i> <span>Dashboard</span></g:link></li>
     <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Item</span></a>
      <ul>
        <li><g:link controller="tax" action="create">Tax</g:link></li>
        <%--<li><g:link controller="taxType" action="create">Tax Type</g:link></li>
      --%>
      <li><g:link controller="measurement" action="create">Measurement</g:link></li>
        <li><g:link controller="item" action="create">Item</g:link></li>
      </ul>
    </li>
    <li> <g:link controller="customer" action="search"><img src="${resource(dir: 'img', file: 'customer.png')}"/>  <span>Customer</span></g:link></li>
    <li class="submenu"> <a href="#"><i class="icon icon-signal"></i> <span>Sales</span></a>
    	<ul>
    		 <li> <g:link controller="estimate" action="search"> Estimate</g:link> </li>
		    <li> <g:link controller="invoice" action="search"> Invoice</g:link> </li>
		    <li> <g:link controller="receivedPayment" action="search">Received Payment</g:link> </li>
		    <li> <g:link controller="creditNote" action="search">Credit Note</g:link> </li>
		  </ul>
	</li>	    
		    <li> <g:link controller="report" action="index" params="[type:'invoice']"><i class="icon icon-book"></i> <span>Reports</span></g:link> </li>
    <li class="submenu"> <a href="#"><i class="icon  icon-search"></i> <span>Search</span></a>
      <ul>
      	<li><g:link controller="search" action="searchInvoice">Invoice</g:link></li>
        <li><g:link controller="search" action="searchCreditNote" >Credit Note</g:link></li>
      </ul>
    </li>
    <shiro:hasPermission permission="user:*">
    	<li> <g:link controller="user" action="search"><i class="icon icon-user"></i> <span>User</span></g:link> </li>
    </shiro:hasPermission>
    <li style="float: right"><span>Welcome <shiro:principal/> </span></li>
</div>
