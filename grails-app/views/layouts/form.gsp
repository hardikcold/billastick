<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="${resource(dir: 'css', file: 'bootstrap.min.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir: 'css', file: 'bootstrap-responsive.min.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir: 'css', file: 'select2.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir: 'css', file: 'uniform.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir: 'css', file: 'billastick-style.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir: 'css', file: 'billastick-media.css')}" rel="stylesheet" type="text/css" />
		<link href="${resource(dir: 'css', file: 'jquery-ui.css')}" rel="stylesheet" type="text/css" />
		
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'jquery-1.9.1.js')}'></script>
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'jquery.min.js')}'></script>
  		 <script type='text/javascript' src='${resource(dir: 'js', file: 'jquery-ui.js')}'></script>
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'bootstrap.min.js')}'></script>
		<script type='text/javascript' src='${resource(dir: 'js', file: 'jquery.uniform.js')}'></script> 
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'select2.min.js')}'></script>
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'jquery.validate.js')}'></script>
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'billastick.form_common.js')}'></script>
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'common.js')}'></script>
		 <script type='text/javascript' src='${resource(dir: 'js', file: 'jquery.blockUI.js')}'></script>
				 
		 	
		  <script type="text/javascript">
			   function showSpinner(obj){
			        $('#' + obj).html("<img src='${resource(dir: 'img', file: 'spinner.gif')}' style=\'display: block; margin: 0 auto\'>")
			   }

			   $(document).ready(function(){
				   
				   //$('select').select2();
					//$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
					
					/*$('select').select2();*/
				    //$('.datepicker').datepicker();
					$( ".uptoTodayDatepicker" ).datepicker({
						maxDate: '0',
						dateFormat : 'dd/mm/yy',
						 showOn: "button",
						 buttonImage: "${resource(dir: 'images', file: 'calendar.png')}",
						 buttonImageOnly: true,
						 changeMonth: true,
						 changeYear: true,
					});
					
					$( ".datepicker" ).datepicker({
						minDate : '0',
						dateFormat : 'dd/mm/yy',
						showOn: "button",
						 buttonImage: "${resource(dir: 'images', file: 'calendar.png')}",
						 buttonImageOnly: true,
						 changeMonth: true,
						 changeYear: true,
					});

					$( ".allDayDatepicker" ).datepicker({
						dateFormat : 'dd/mm/yy',
						showOn: "button",
						 buttonImage: "${resource(dir: 'images', file: 'calendar.png')}",
						 buttonImageOnly: true,
						 changeMonth: true,
						 changeYear: true,
					});
					
				});
			</script>

		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		<!--Header-part-->
		<div id="header">
		  <h1></h1>
		</div>
		<!--close-Header-part-->
		
		<!--top-Header-messaages-->
		<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
		<!--close-top-Header-messaages-->  
		
		
		
		<!--top-Header-menu-->
		<div id="user-nav" class="navbar navbar-inverse"><ul class="nav">
		    <li class="" ><g:link title="Profile" action="edit" controller="profile" id="${session?.currentUser?.id }"><i class="icon icon-user"></i> <span class="text">Profile</span></g:link></li>
		    <%--<li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
		    --%><li class=""><g:link title="Logout" action="signOut" controller="auth"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></g:link></li>
		  </ul>
		</div>
		<!--close-top-Header-menu-->
		
		<!-- top-navigation-menu -->
			<g:render template="/layouts/topNavigation"></g:render>
		<!-- close top-navigation-menu -->
		
		<div id="content">
			<g:render template="/layouts/middleNavigation"></g:render>
			<g:layoutBody/>
		</div>
		
		<div class="row-fluid">
	      <div id="footer" class="span12"> Copyright &copy; 2013 Billastick. All rights reserved</div>
	    </div>
		
		
		
	</body>
</html>
