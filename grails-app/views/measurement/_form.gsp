 
  <div class="control-group  fieldcontain ${hasErrors(bean: measurementInstance, field: 'measurementName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Name" /></label>
     <div class="controls">
         <input type="text" name="measurementName" maxlength="25" value="${measurementInstance?.measurementName}" />
         <span for="measurementName" generated="true" class="help-inline"><g:fieldError bean="${measurementInstance}" field="measurementName"></g:fieldError></span>
     </div>
 </div>
 <div class="control-group fieldcontain ${hasErrors(bean: measurementInstance, field: 'measurementCode', 'error')}">
     <label class="control-label"><g:message code="tax.taxRate.label" default="Code" />(Ex: Kilograms : KG)</label>
     <div class="controls">
         <input type="text"  name="measurementCode" maxlength="10" value="${measurementInstance?.measurementCode}"/>
         <span for="measurementCode" generated="true" class="help-inline"><g:fieldError bean="${measurementInstance}" field="measurementCode"></g:fieldError></span>
     </div>
 </div>


<script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#measurementForm").validate({
			rules:{
				measurementName:{
					required:true
				},
				measurementCode:{
					required:true
				}
			},
			messages:{
				measurementName:"Please enter Name.",
				measurementCode:"Please enter Code."	
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>