<%@ page import="com.billing.utility.ReceivedPaymentStatus" %>
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn action="search" onLoading="showSpinner(\'receivedPaymentListDiv\')" property="dateCreated" update="receivedPaymentListDiv" title="Payment Date" defaultOrder="asc" params="[max:params?.max ?: 10, searchText : params?.searchText?:'']"/>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right">Invoice#</th>
			<th style="text-align: right">Payment Method</th>
			<th style="text-align: right">Amount</th>
			<th style="text-align: center">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${receivedInstanceList?.size() > 0 }">
				<g:each in="${receivedInstanceList}" var="receivedInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center"><g:formatDate format='dd/MM/yyyy' date='${receivedInstance?.paymentDate }'/></td>
						<td>${receivedInstance?.invoice?.customer?.customerName }</td>
						<td style="text-align: right" width="10%">${receivedInstance?.invoice?.invoiceNumber }</td>
						<td style="text-align: right" width="10%">${receivedInstance?.paymentMethod }</td>
						<td  width="10%" style="text-align: right">${receivedInstance?.receivedAmount}</td>
						<td width="1%" style="text-align: center">
							<a href="#myAlert${i}" data-toggle="modal"><i class="icon-trash" title="Delete"></i>&nbsp;</a>
						</td>
						 <div id="myAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${receivedInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="7" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="search" total="${receivedInstanceListTotal}" 
			update="receivedPaymentListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[searchText : params?.searchText?:'']" onLoading="showSpinner(\'receivedPaymentListDiv\')"/>
</div>

