 <%@ page import="com.billing.domain.Customer" %>
<%@ page import="com.billing.utility.PaymentMethod" %>
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice#" /></label>
     <div class="controls">
         <input type="hidden" name="invoiceNumber" id="invoiceNumber" value="${invoiceInstance?.invoiceNumber}" />
			<input type="hidden" name="invoiceId" id="invoiceId" value="${invoiceInstance?.id}" />
			${invoiceInstance?.invoiceNumber}
     </div>
 </div>
 
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer" /></label>
     <div class="controls">
        <input type="hidden" name="customerId" id="customerId" value="${invoiceInstance?.customer?.id}" />
			<g:link id="${invoiceInstance?.customer?.id}" controller="customer" action="edit"> ${invoiceInstance?.customer?.customerName} </g:link>
     </div>
 </div>
 
  <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Due Amount" /></label>
     <div class="controls">
        ${invoiceInstance?.totalBalanceDue}
     </div>
 </div>
 
  <div class="control-group fieldcontain ${hasErrors(bean: receivedPaymentInstance, field: 'receivedAmount', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Received Amounts" /></label>
     <div class="controls">
        <input type="text" name="receivedAmount" id="receivedAmount" onkeyup="onlyDecimal(this, 15)" value="${receivedPaymentInstance?.receivedAmount }" />
     </div>
 </div>

<div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Payment Date" /></label>
     <div class="controls">
          <input type="text" class="uptoTodayDatepicker" id="paymentDate" name="paymentDate" data-date-format="dd/mm/yyyy" size="10" value="<g:formatDate format='dd/MM/yyyy' date='${receivedPaymentInstance?.paymentDate}'/>"/>
     </div>
 </div>
 	
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxRate.label" default="Payment Type" /></label>
     <div class="controls">
        <g:select name="paymentMethod" id="paymentMethod" from="${PaymentMethod.list()}" />
     </div>
 </div>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: receivedPaymentInstance, field: 'notes', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Notes" /></label>
     <div class="controls">
         <textarea name="notes">${receivedPaymentInstance?.notes}</textarea>
     </div>
 </div>
 <script type="text/javascript">
	$(document).ready(function(){
		// Form Validation
	    $("#receivedPaymentForm").validate({
			rules:{
				receivedAmount:{
					required:true,
					max: ${invoiceInstance?.totalBalanceDue}
				},
				paymentDate:{
					required:true
				}
			},
			messages:{
				paymentDate:""
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script> 	
