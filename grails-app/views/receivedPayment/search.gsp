<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Received Payment</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Search Received Payment</h5>
							</div>
							<div class="widget-content nopadding form-horizontal">
							     <label class="control-label"><g:message code="tax.taxName.label" default="Customer Name" /></label>
							     <div class="controls">
							         <input type="text" name="searchText" onkeyup="${remoteFunction(action: 'search',
						                       update: 'receivedPaymentListDiv',
						                       params: '\'searchText=\' + this.value')}"/>
							     </div>
					
							</div>
						</div><!-- End of widget-box -->	
						
						
						<div class="widget-box">
				          <div class="widget-title">
				             <span class="icon"><i class="icon-th"></i></span> 
				            <h5>Received Payment List</h5>
				          </div>
				          <div id="DataTables_Table_2_length" class="dataTables_length" style="float:right">
							<label>
							Show
							<g:select name="max" from="['10','20','50','100']" class="paginationDropdown"  onChange="${remoteFunction(action: 'search',
		                       update: 'receivedPaymentListDiv',
							   onLoading:'showSpinner(\'receivedPaymentListDiv\')',	
		                       params: '\'max=\' + this.value + \'&searchText=\'')}"/>
							entries
							</label>
						</div>
				          <div class="widget-content nopadding" id="receivedPaymentListDiv">
				           	<g:render template="dataList"></g:render>
				          </div>
				        </div><!-- End of widget-box -->
				        
				        
				</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
