<%@ page import="com.billing.utility.ReceivedPaymentStatus" %>
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn action="search" onLoading="showSpinner(\'refundListDiv\')" property="dateCreated" update="refundListDiv" title="Refund Date" defaultOrder="asc" params="[max:params?.max ?: 10, searchText : params?.searchText?:'']"/>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right">Ref#</th>
			<th style="text-align: right">Payment Method</th>
			<th style="text-align: right">Amount</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${refundInstanceList?.size() > 0 }">
				<g:each in="${refundInstanceList}" var="refundInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center"><g:formatDate format='dd/MM/yyyy' date='${refundInstance?.refundDate }'/></td>
						<td>${refundInstance?.creditNote?.customer?.customerName }</td>
						<td style="text-align: right" width="10%">${refundInstance?.creditNote?.creditNoteNumber }</td>
						<td style="text-align: right" width="10%">${refundInstance?.paymentMethod }</td>
						<td  width="10%" style="text-align: right">${refundInstance?.refundAmount}</td>
						<td width="5%">
							 <div class="btn-group">
									  <button class="btn btn-primary">Action</button>
									  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></button>
									  <ul class="dropdown-menu">
										<li><a href="#refundAlert${i}" data-toggle="modal"><i class="icon-trash"></i>&nbsp;Delete</a></li>
									  </ul>
								</div>
                                <div id="refundAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${refundInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
						</td>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="7" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="search" total="${refundInstanceListTotal}" 
			update="refundListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[searchText : params?.searchText?:'']" onLoading="showSpinner(\'refundListDiv\')"/>
</div>

