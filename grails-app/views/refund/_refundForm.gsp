 <%@page import="java.lang.Math"%>
<%@ page import="com.billing.domain.Customer" %>
<%@ page import="com.billing.utility.PaymentMethod" %>
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice#" /></label>
     <div class="controls">
         <input type="hidden" name="creditNoteNumber" id="creditNoteNumber" value="${invoiceInstance?.invoiceNumber}" />
			<input type="hidden" name="invoiceId" id="invoiceId" value="${invoiceInstance?.id}" />
			${invoiceInstance?.invoiceNumber}
     </div>
 </div>
 
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Customer" /></label>
     <div class="controls">
        <input type="hidden" name="customerId" id="customerId" value="${invoiceInstance?.customer?.id}" />
			<g:link id="${invoiceInstance?.customer?.id}" controller="customer" action="edit"> ${invoiceInstance?.customer?.customerName} </g:link>
     </div>
 </div>
 
  <div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Refundable Amount" /></label>
     <div class="controls">
        ${Math.abs(invoiceInstance?.totalBalanceDue?:0)}
     </div>
 </div>
 
  <div class="control-group fieldcontain ${hasErrors(bean: refundInstance, field: 'refundAmount', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Amount" /></label>
     <div class="controls">
        <input type="text" name="refundAmount" id="refundAmount" value="${refundInstance?.refundAmount }" onkeyup="onlyDecimal(this, 15)"/>
     </div>
 </div>
 

<div class="control-group">
     <label class="control-label"><g:message code="tax.taxName.label" default="Refund Date" /></label>
     <div class="controls">
          <input type="text" class="uptoTodayDatepicker" id="refundDate" name="refundDate" data-date-format="dd/mm/yyyy" size="10" value="<g:formatDate format='dd/MM/yyyy' date='${refundInstance?.refundDate}'/>"/>
     </div>
 </div>
 	
 <div class="control-group">
     <label class="control-label"><g:message code="tax.taxRate.label" default="Payment Type" /></label>
     <div class="controls">
        <g:select name="paymentMethod" id="paymentMethod" from="${PaymentMethod.list()}" />
     </div>
 </div>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: refundInstance, field: 'notes', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Notes" /></label>
     <div class="controls">
         <textarea name="notes">${refundInstance?.notes}</textarea>
     </div>
 </div>
 <script type="text/javascript">
	$(document).ready(function(){
		// Form Validation
	    $("#refundForm").validate({
			rules:{
				refundAmount:{
					required:true,
					max: ${Math.abs(invoiceInstance?.totalBalanceDue?:0)}
				},
				refundDate:{
					required:true
				}
			},
			messages:{
				refundDate:""
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>  	
