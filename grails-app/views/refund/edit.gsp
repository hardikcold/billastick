<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Edit Refund</title>
	</head>
	<body>
	<div id="content-header">
			<div id="breadcrumb">
				<a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#">Credit Note</a>
				<a href="#" class="current">Edit Refund</a>
			</div>
	</div><!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Edit Refund</h5>
							</div>
							<div class="widget-content nopadding">
								<g:form action="update" class="form-horizontal">
								 	<g:hiddenField name="id" value="${refundInstance?.id}" />
									<g:hiddenField name="version" value="${refundInstance?.version}" />      
                                   <g:render template="form"></g:render>
                                   <div class="form-actions">
									     <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Save')}" />
										<input type="button" onclick="window.location.href = '${createLink(action: 'search', controller: 'invoice')}'" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Cancel')}" />
									 </div>  
                                </g:form>
							</div>
						</div><!-- End of widget-box -->	
						
						
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
