<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Search Refund</title>
	</head>
	<body>
	<div id="content-header">
			<div id="breadcrumb">
				<a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
				<a href="#" class="current">Refund</a>
			</div>
	</div><!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Search Refund</h5>
							</div>
							<div class="widget-content nopadding form-horizontal">
							     <label class="control-label"><g:message code="tax.taxName.label" default="Refund#" /></label>
							     <div class="controls">
							         <input type="text" name="searchText" onkeyup="${remoteFunction(action: 'search',
						                       update: 'refundListDiv',
						                       params: '\'searchText=\' + this.value')}"/>
							     </div>
					
							</div>
						</div><!-- End of widget-box -->	
						
						
						<div class="widget-box">
				          <div class="widget-title">
				             <span class="icon"><i class="icon-th"></i></span> 
				            <h5>Refund List</h5>
				          </div>
				          <div id="DataTables_Table_2_length" class="dataTables_length" style="float:right">
							<label>
							Show
							<g:select name="max" from="['10','20','50','100']" onChange="${remoteFunction(action: 'search',
		                       update: 'refundListDiv',
							   onLoading:'showSpinner(\'refundListDiv\')',	
		                       params: '\'max=\' + this.value + \'&searchText=\'')}"/>
							entries
							</label>
						</div>
				          <div class="widget-content nopadding" id="refundListDiv">
				           	<g:render template="dataList"></g:render>
				          </div>
				        </div><!-- End of widget-box -->
				        
				        
				</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
