<%@page import="com.billing.utility.ReportFormat"%>
<%@page import="com.billing.utility.InvoiceType"%>
<g:form class="form-horizontal" name="reportOptionForm" action="invoiceReportByType" controller="report">
<div class="chat-message well" style="text-align: center;width: 90%;margin-left: 10%">
     <h6>By Invoice Type</h6>
</div>
<div class="chat-content panel-left2">
	<label class="control-label"><g:message
			code="tax.taxName.label" default="From" /></label>
	<div class="controls">
		<input type="text" class="allDayDatepicker" id="fromDate"
			name="fromDate" size="10" value="${params?.fromDate}" />
	</div>
	<label class="control-label"><g:message
			code="tax.taxName.label" default="To" /></label>
	<div class="controls">
		<input type="text" class="allDayDatepicker" id="toDate" name="toDate"
			size="10" value="${params?.toDate}" />
	</div>

	<label class="control-label"><g:message
			code="tax.taxName.label" default="Invoice Type" /></label>
	<div class="controls">
		<g:select name="invoiceType" name="invoiceType"
			from="${InvoiceType.list()}" noSelection="['':'All']" value="" />
	</div>
	
	 <div class="control-group">
		<label class="control-label">Report Format</label>
		<div class="controls">
			<label><g:radio name="reportFormat" value="${ReportFormat.Pdf}" checked="${invoiceInstance?.invoiceType == InvoiceType.Retail ? 'true' : 'false' }" /> ${ReportFormat.Pdf}</label>
			<label><g:radio name="reportFormat" value="${ReportFormat.Excel}" checked="${invoiceInstance?.invoiceType == InvoiceType.Tax}"  /> ${ReportFormat.Excel}</label>
		</div>
	</div>
	
	<div style="margin-left: 20%">
     	<g:submitButton name="invoiceReportByType" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Generate')}" />
    </div>  
</div>
</g:form>