<%@page import="com.billing.utility.InvoiceType"%>
<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Credit Note</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
			            <div class="widget-box widget-chat">
			              <div class="widget-title"> <span class="icon"> <i class="icon-file"></i> </span>
			                <h5>Report Option</h5>
			              </div>
			              <div class="widget-content nopadding">
			                <div class="chat-users panel-right2">
			                  <div class="panel-title">
			                    <h5>Report</h5>
			                  </div>
			                  <div class="panel-content nopadding">
			                    <ul class="contact-list">
			                      <li id="user-Sunil" class="online ${params?.type.equals('invoice') ? 'new' : ''}"><g:link action="index" params="[type:'invoice']"><span>Tax/Retails Invoices</span></g:link></li>
			                      <li id="user-Laukik" class="online ${params?.type.equals('payment') ? 'new' : ''}"><g:link action="index" params="[type:'payment']" ><span>Cash/Credit Invoices</span></g:link></li>
			                      <%--<li id="user-vijay" class="online ${params?.type.equals('customer') ? 'new' : ''}"><g:link action="index" params="[type:'customer']"> <span>By Customer</span></g:link></li>
			                    --%></ul>
			                  </div>
			                </div>
				            <div id="reportForm">
				                	<g:render template="${templateName ?: 'reportInvoiceDetailForm'}"></g:render>
				            </div>
			              </div>
			            </div>
			          </div>
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
