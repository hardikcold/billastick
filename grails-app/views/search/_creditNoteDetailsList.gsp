<%@ page import="com.billing.utility.InvoiceStatus" %>
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn width="10%" action="searchCreditNote" onLoading="showSpinner(\'creditNoteDetailsListDiv\')" property="creditNoteNumber" update="creditNoteDetailsListDiv" title="Credit Note#" defaultOrder="asc" params="[max:params?.max ?: 10, toDate : params?.toDate, fromDate : params?.fromDate]"/>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right" width="10%">Sub Total</th>
			<th style="text-align: right" width="10%">Net Total</th>
			<th style="text-align: right" width="10%">Balance Due</th>
			<th style="text-align: right" width="10%">CreditNote Date</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${creditNoteInstanceList?.size() > 0 }">
				<g:each in="${creditNoteInstanceList}" var="creditNoteInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center"><g:link action="details" controller="creditNote" id="${creditNoteInstance?.id}">${creditNoteInstance?.creditNoteNumber }</g:link></td>
						<td>${creditNoteInstance?.customer.customerName }</td>
						<td style="text-align: right" width="10%">${creditNoteInstance?.subTotal}</td>
						<td style="text-align: right" width="10%">${creditNoteInstance?.netTotal}</td>
						<td  width="10%" style="text-align: right">${creditNoteInstance?.balanceDue}</td>
						<td style="text-align: right" width="10%"><g:formatDate format='dd/MM/yyyy' date='${creditNoteInstance?.creditNoteDate}'/></td>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="9" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="searchCreditNote" total="${creditNoteInstanceListTotal?:0}" 
			update="creditNoteDetailsListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[toDate : params?.toDate, fromDate : params?.fromDate]" onLoading="showSpinner(\'creditNoteDetailsListDiv\')"/>
</div>

