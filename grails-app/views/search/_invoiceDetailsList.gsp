<%@ page import="com.billing.utility.InvoiceStatus" %>
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn action="searchInvoice" onLoading="showSpinner(\'invoiceDetailsListDiv\')" property="invoiceNumber" update="invoiceDetailsListDiv" title="Invoice#" defaultOrder="asc" params="[max:params?.max ?: 10, invoiceStatus : params?.invoiceStatus, toDate : params?.toDate, fromDate : params?.fromDate]"/>
			<th style="text-align: left">Customer Name</th>
			<th style="text-align: right">Sub Total</th>
			<th style="text-align: right">Net Total</th>
			<th style="text-align: right">Balance Due</th>
			<th style="text-align: right">Invoice Date</th>
			<th style="text-align: right">Status</th>
			<th style="text-align: right">Payment Type</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${invoiceInstanceList?.size() > 0 }">
				<g:each in="${invoiceInstanceList}" var="invoiceInstance" status="i">
					<tr class="gradeX">
						<td width="10%" style="text-align: center"><g:link action="invoiceDetails" controller="invoice" id="${invoiceInstance?.id}">${invoiceInstance?.invoiceNumber }</g:link></td>
						<td>${invoiceInstance?.customer.customerName }</td>
						<td style="text-align: right" width="10%">${invoiceInstance?.subTotal}</td>
						<td style="text-align: right" width="10%">${invoiceInstance?.netTotal}</td>
						<td  width="10%" style="text-align: right">${invoiceInstance?.balanceDue}</td>
						<td style="text-align: right" width="10%"><g:formatDate format='dd/MM/yyyy' date='${invoiceInstance?.invoiceDate}'/></td>
						<td width="10%" class="taskStatus">
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Closed}">
								<span class="invoiced">${invoiceInstance?.invoiceStatus}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Open}">
								<span class="draft">${invoiceInstance?.invoiceStatus}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Cancelled}">
								<span class="cancelled">${invoiceInstance?.invoiceStatus}</span>
							</g:if>
							<g:if test="${invoiceInstance?.invoiceStatus == InvoiceStatus.Overdue}">
								<span class="overdue">${invoiceInstance?.invoiceStatus}</span>
							</g:if>
						</td>
						<td style="text-align: right" width="10%" >${invoiceInstance?.paymentType}</td>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="9" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="searchInvoice" total="${invoiceInstanceListTotal}" 
			update="invoiceDetailsListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[invoiceStatus : params?.invoiceStatus, toDate : params?.toDate, fromDate : params?.fromDate]" onLoading="showSpinner(\'invoiceDetailsListDiv\')"/>
</div>

