
<%@page import="com.billing.utility.InvoiceStatus"%>
<label class="control-label"><g:message code="tax.taxName.label"
		default="From" /></label>
<div class="controls">
	<input type="text" class="allDayDatepicker" id="fromDate"
		name="fromDate" size="10"
		value="${params?.fromDate}" />
</div>
<label class="control-label"><g:message code="tax.taxName.label"
		default="To" /></label>
<div class="controls">
	<input type="text" class="allDayDatepicker" id="toDate"
		name="toDate" size="10"
		value="${params?.toDate}" />
</div>

<label class="control-label"><g:message code="tax.taxName.label"
		default="Invoice Status" /></label>
<div class="controls">
	<g:select name="invoiceStatus"  name="invoiceStatus" from="${InvoiceStatus.list()}"   noSelection="['':'--All--']" value="${params?.invoiceStatus}"/>
</div>