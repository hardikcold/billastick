<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Invoice Details</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
						<div class="accordion" id="collapse-group">
			              <div class="accordion-group widget-box">
			                <div class="accordion-heading">
			                  <div class="widget-title"> <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse"> <span class="icon"><i class="icon-search"></i></span>
			                    <h5>Search (Click Here)</h5>
			                    </a> </div>
			                </div>
			                <div class="collapse in accordion-body" id="collapseGOne">
			                  <div class="widget-content">
				                  	 <form action="searchInvoice" class="form-horizontal">
										  <g:render template="invoiceDetailsSearchForm"></g:render>	
									      <div class="form-actions" >
												<g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Search')}" />
										  </div>
									</form>
			                   </div>
			                </div>
			              </div>
			            </div>
						
						<div class="widget-box">
				          <div class="widget-title">
				             <span class="icon"><i class="icon-th"></i></span> 
				            <h5>Invoice List</h5>
				          </div>
				          <div id="DataTables_Table_2_length" class="dataTables_length" style="float:right">
							<label>
							Show
							<g:select name="max" from="['10','20','50','100']" class="paginationDropdown"  onChange="${remoteFunction(action: 'searchInvoice',
		                       update: 'invoiceDetailsListDiv',
							   onLoading:'showSpinner(\'invoiceDetailsListDiv\')',	
		                       params: '\'max=\' + this.value + \'&invoiceStatus=\' + invoiceStatus.value + \'&fromDate=\' + fromDate.value + \'&toDate=\' + toDate.value')}"/>
							entries
							</label>
						</div>
				          <div class="widget-content nopadding" id="invoiceDetailsListDiv">
				           	<g:render template="invoiceDetailsList"></g:render>
				          </div>
				        </div><!-- End of widget-box -->				
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
