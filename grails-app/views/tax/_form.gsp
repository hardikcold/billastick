<%@page import="com.billing.domain.FinancialYear"%>
<%@ page import="com.billing.domain.Tax"%>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: taxInstance, field: 'taxName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Tax Name" /></label>
     <div class="controls">
         <input type="text" name="taxName" id="taxName" maxlength="20" value="${taxInstance?.taxName}" />
         <span class="help-inline" for="taxName" generated="true"><g:fieldError bean="${taxInstance}" field="taxName"></g:fieldError></span>
     </div>
 </div>
 <div class="control-group fieldcontain ${hasErrors(bean: taxInstance, field: 'taxRate', 'error')}">
     <label class="control-label"><g:message code="tax.taxRate.label" default="Tax Rate" />(Example: 50%)</label>
     <div class="controls">
         <input type="text"  name="taxRate" id="taxRate" value="${taxInstance?.taxRate}" onkeyup="onlyDecimal(this, 5)"/>
         <span class="help-inline" for="taxRate" generated="true"><g:fieldError bean="${taxInstance}" field="taxRate"></g:fieldError></span>
     </div>
 </div>
 <div class="control-group">
     <label class="control-label">Is Additional Tax </label>
     <div class="controls">
         <g:checkBox name="isAdditional" checked="${taxInstance?.isAdditional == true ? 'true' : 'false' }"/>
     </div>
 </div>
 
<script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#taxForm").validate({
			rules:{
				taxName:{
					required:true
				},
				taxRate:{
					required:true
				}
			},
			messages:{
				taxName:"Please enter Tax Name.",
				taxRate:"Please enter Tax Rate."	
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>