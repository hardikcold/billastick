<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Add Tax</title>
	</head>
	<body>
	<!-- End of content-header -->
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Create Tax</h5>
							</div>
							<div class="widget-content nopadding">
								<g:form action="save" class="form-horizontal" name="taxForm">
                                   <g:render template="form"></g:render>
                                   <div class="form-actions">
									     <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Save')}" />
										<input type="reset" class="btn btn-info" value="${message(code: 'default.button.reset.label', default: 'Reset')}" />
									 </div>  
                                </g:form>
							</div>
						</div><!-- End of widget-box -->	
						
						
						<div class="widget-box">
				          <div class="widget-title">
				             <span class="icon"><i class="icon-th"></i></span> 
				            <h5>Tax List</h5>
				          </div>
				          <div id="DataTables_Table_2_length" class="dataTables_length" style="float:right">
							<label>
							Show
							<g:select class="paginationDropdown" name="max" from="['10','20','50','100']" onChange="${remoteFunction(action: 'list',
		                       update: 'taxListDiv',
							   onLoading:'showSpinner(\'taxListDiv\')',	
		                       params: '\'max=\' + this.value')}"/>
							entries
							</label>
						</div>
				          <div class="widget-content nopadding" id="taxListDiv">
				           	<g:render template="dataList"></g:render>
				          </div>
				        </div><!-- End of widget-box -->
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
