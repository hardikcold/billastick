
<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn action="list" onLoading="showSpinner(\'taxTypeListDiv\')" property="taxTypeName" update="taxTypeListDiv" title="Name" defaultOrder="desc" params="[max:params?.max ?: 10]"/>
			<th style="text-align: right">Rates</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${taxTypeInstanceList?.size() > 0 }">
				<g:each in="${taxTypeInstanceList}" var="taxTypeInstance" status="i">
					<tr class="gradeX">
						<td>${taxTypeInstance?.taxTypeName }</td>
						<td style="text-align: right" width="15%">${taxTypeInstance?.tax.taxRate } %</td>
						<td width="5%" style="text-align: right">
								<g:link action="edit" controller="taxType" id="${taxTypeInstance?.id}"><i class="icon-pencil" title="Edit"></i>&nbsp;</g:link>
								<a href="#myAlert${i}" data-toggle="modal"><i class="icon-trash" title="Delete"></i>&nbsp;</a>
						</td>
						 <div id="myAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${taxTypeInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="3" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="list" total="${taxTypeInstanceListTotal}" 
			update="taxTypeListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" onLoading="showSpinner(\'taxTypeListDiv\')"/>
</div>

