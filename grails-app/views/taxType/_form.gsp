<%@ page import="com.billing.domain.Tax"%>


<div class="control-group  fieldcontain ${hasErrors(bean: taxTypeInstance, field: 'taxTypeName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Tax Type" /></label>
     <div class="controls">
         <input type="text" name="taxTypeName" value="${taxTypeInstance?.taxTypeName}" />
         <span class="help-inline" for="taxTypeName" generated="true"><g:fieldError bean="${taxTypeInstance}" field="taxTypeName"></g:fieldError></span>
     </div>
 </div>
 <div class="control-group">
     <label class="control-label">Select Tax </label>
     <div class="controls">
     	<g:each in="${taxList}" status="i" var="taxInstance">
        	  <div style="width:10%;float:left"><g:checkBox name="taxName" value="${taxInstance?.id}" checked="${taxTypeInstance?.tax?.id?.contains(taxInstance?.id) == true ? 'true' : 'false' }"/><span style="margin-top: 10px;">&nbsp;${taxInstance?.taxName}</span></div>
                <g:if test="${ (i+1) % 3 == 0}">
                <br>
                </g:if>
        </g:each>
        <br>	 
     </div>
 </div>
<script type="text/javascript">
	$(document).ready(function(){
		
		// Form Validation
	    $("#taxTypeForm").validate({
			rules:{
				taxTypeName:{
					required:true
				}
			},
			messages:{
				taxTypeName:"Please enter Tax Type."
			},
			errorClass: "help-inline",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.control-group').addClass('error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.control-group').removeClass('error');
			}
		});
	});
		
	</script>