<table class="table table-bordered data-table">
	<thead>
		<tr>
			<util:remoteSortableColumn style="text-align: left" action="search" onLoading="showSpinner(\'userListDiv\')" property="username" update="userListDiv" title="Username" defaultOrder="desc" params="[max:params?.max ?: 10, searchText : params?.searchText?:'']"/>
			<th style="text-align: right">Organization Name</th>
			<th style="text-align: right">Person Name</th>
			<th style="text-align: right">Mobile</th>
			<th style="text-align: right">Invoice Allow</th>
			<th style="text-align: right">Last Login</th>
			<th style="text-align: right">Status</th>
			<th style="text-align: right">Actions</th>
		</tr>
	</thead>
	<tbody>
		<g:if test="${userInstanceList?.size() > 0 }">
				<g:each in="${userInstanceList}" var="userInstance" status="i">
					<tr class="gradeX">
						<td>${userInstance?.username }</td>
						<td style="text-align: right" width="10%">${userInstance?.organizationName }</td>
						<td style="text-align: right" width="10%">${userInstance?.firstName } &nbsp; ${userInstance?.lastName }</td>
						<td style="text-align: right" width="10%">${userInstance?.mobileNumber }</td>
						<td style="text-align: right" width="10%">${userInstance?.remainingInvoiceCount }</td>
						<td style="text-align: right" width="10%"><g:formatDate format='dd/MM/yyyy hh:mm:ss' date='${userInstance?.lastLoginDate}'/></td>
						
						<td style="text-align: right" width="10%">${userInstance?.enabled == true ? 'Active' : 'Inactive' }</td>
						<td width="5%">
							 <div class="btn-group">
									  <button class="btn btn-primary">Action</button>
									  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></button>
									  <ul class="dropdown-menu">
										<li><g:link action="edit" controller="user" id="${userInstance?.id}"><i class="icon-pencil"></i>&nbsp;Edit</g:link></li>
										<li><a href="#myAlert${i}" data-toggle="modal"><i class="icon-trash"></i>&nbsp;Delete</a></li>
									  </ul>
								</div>
                                <div id="myAlert${i}" class="modal hide">
					              <div class="modal-header">
					                <button data-dismiss="modal" class="close" type="button">×</button>
					                <h3>Delete</h3>
					              </div>
					              <div class="modal-body">
					                <p>Are you sure you want to delete this record?</p>
					              </div>
					              <div class="modal-footer"> <g:link action="delete" id="${userInstance?.id}" class="btn btn-primary" href="#">Confirm</g:link> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>
					            </div>
						</td>
					</tr>
				</g:each>
		</g:if>	
		<g:else>
			<tr class="gradeX">
				<td colspan="3" style="text-align: center">No record found</td>
			</tr>
		</g:else>		
	</tbody>
</table>
<div style="float:right" class="pagination">
<util:remotePaginate action="search" total="${userInstanceListTotal}" 
			update="userListDiv" max="${params?.max ?: 10 }" offset="${params?.offset }" params="[searchText : params?.searchText?:'']" onLoading="showSpinner(\'userListDiv\')"/>
</div>

