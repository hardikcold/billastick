<div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="User Name" /></label>
     <div class="controls">
         <input type="text" name="username" value="${userInstance?.username}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="username"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'passwordHash', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Password" /></label>
     <div class="controls">
         <input type="password" name="passwordHash" value="${userInstance?.passwordHash}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="passwordHash"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'organizationName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Organization Name" /></label>
     <div class="controls">
         <input type="text" name="organizationName" value="${userInstance?.organizationName}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="organizationName"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'firstName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="First Name" /></label>
     <div class="controls">
         <input type="text" name="firstName" value="${userInstance?.firstName}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="firstName"></g:fieldError></span>
     </div>
 </div>
 
  <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'lastName', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Last Name" /></label>
     <div class="controls">
         <input type="text" name="lastName" value="${userInstance?.lastName}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="lastName"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'address1', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Address 1" /></label>
     <div class="controls">
         <input type="text" name="address1" value="${userInstance?.address1}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="address1"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'address2', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Address 2" /></label>
     <div class="controls">
         <input type="text" name="address2" value="${userInstance?.address2}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="address2"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'city', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="City" /></label>
     <div class="controls">
         <input type="text" name="city" value="${userInstance?.city}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="city"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'state', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="State" /></label>
     <div class="controls">
         <input type="text" name="state" value="${userInstance?.state}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="state"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'officeNumber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Office#" /></label>
     <div class="controls">
         <input type="text" name="officeNumber" value="${userInstance?.officeNumber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="officeNumber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'mobileNumber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Mobile#" /></label>
     <div class="controls">
         <input type="text" name="mobileNumber" value="${userInstance?.mobileNumber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="mobileNumber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'numberInvoiceAllow', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="Invoice Allow #" /></label>
     <div class="controls">
         <input type="text" name="numberInvoiceAllow" value="${userInstance?.numberInvoiceAllow}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="numberInvoiceAllow"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'tinNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="TIN#" /></label>
     <div class="controls">
         <input type="text" name="tinNummber" value="${userInstance?.tinNummber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="tinNummber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'panNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="PAN#" /></label>
     <div class="controls">
         <input type="text" name="panNummber" value="${userInstance?.panNummber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="panNummber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'stNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="ST#" /></label>
     <div class="controls">
         <input type="text" name="stNummber" value="${userInstance?.stNummber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="stNummber"></g:fieldError></span>
  </div>
     
  <div class="control-group  fieldcontain ${hasErrors(bean: userInstance, field: 'cstNummber', 'error')}">
     <label class="control-label"><g:message code="tax.taxName.label" default="CST#" /></label>
     <div class="controls">
         <input type="text" name="cstNummber" value="${userInstance?.cstNummber}" />
         <span for="required" generated="true" class="help-inline"><g:fieldError bean="${userInstance}" field="cstNummber"></g:fieldError></span>
     </div>
 </div>
 
 <div class="control-group">
		<label class="control-label">Status</label>
		<div class="controls">
			<label><g:radio name="enabled" value="true" checked="${userInstance?.enabled == true ? 'true' : 'false' }" /> Active</label>
			<label><g:radio name="enabled" value="false" checked="${userInstance?.enabled == false}"  /> Inactive</label>
		</div>
	</div>