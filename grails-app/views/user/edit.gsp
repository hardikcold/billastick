<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>Edit User</title>
	</head>
	<body>
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Edit User</h5>
							</div>
							<div class="widget-content nopadding">
								<g:form action="update" class="form-horizontal">
								 	<g:hiddenField name="id" value="${userInstance?.id}" />
									<g:hiddenField name="version" value="${userInstance?.version}" />      
                                   <g:render template="form"></g:render>
                                   <div class="form-actions">
									     <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.save.label', default: 'Save')}" />
										<input type="button" onclick="window.location.href = '${createLink(action: 'search', controller: 'customer')}'" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Cancel')}" />
									 </div>  
                                </g:form>
							</div>
						</div><!-- End of widget-box -->	
						
						
					</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
