<%@page import="org.hibernate.event.def.OnLockVisitor"%>
<%@ page import="com.billing.domain.Tax" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="form">
		<g:set var="entityName" value="${message(code: 'tax.label', default: 'Tax')}" />
		<title>User</title>
	</head>
	<body>
			
               <div class="container-fluid">
				<div class="row-fluid">
					<div class="span10">
					
						 <g:if test="${flash.infoMessage}">
							<div class="alert alert-info">
						          <button class="close" data-dismiss="alert">×</button>
						          <strong>Info!</strong> ${flash.infoMessage}
			          		 </div>
			             </g:if>   
				         <g:if test="${flash.message}">
							<div class="alert alert-success">
					          <button class="close" data-dismiss="alert">×</button>
					          <strong>Success!</strong> ${flash.message }
					         </div>
			              </g:if>
			              
			             <ul class="quick-actions">
			              <li> <g:link action="create" name="create" > <i class="icon-add"></i> Create User</g:link> </li>
			            </ul> 
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Search Customer</h5>
							</div>
							<div class="widget-content nopadding form-horizontal">
							     <label class="control-label"><g:message code="tax.taxName.label" default="User Name" /></label>
							     <div class="controls">
							         <input type="text" name="searchText" onkeyup="${remoteFunction(action: 'search',
						                       update: 'userListDiv',
						                       params: '\'searchText=\' + this.value')}"/>
							     </div>
							</div>
						</div><!-- End of widget-box -->	
						
						
						<div class="widget-box">
				          <div class="widget-title">
				             <span class="icon"><i class="icon-th"></i></span> 
				            <h5>User List</h5>
				          </div>
				          <div id="DataTables_Table_2_length" class="dataTables_length" style="float:right">
							<label>
							Show
							<g:select name="max" from="['10','20','50','100']" onChange="${remoteFunction(action: 'search',
		                       update: 'userListDiv',
							   onLoading:'showSpinner(\'userListDiv\')',	
		                       params: '\'max=\' + this.value + \'&searchText=\'')}"/>
							entries
							</label>
						</div>
				          <div class="widget-content nopadding" id="userListDiv">
				           	<g:render template="dataList"></g:render>
				          </div>
				        </div><!-- End of widget-box -->
				        
				        
				</div><!-- End of span6 -->
			</div><!-- End of row-fluid -->
	</div><!-- End of container-fluid -->
	</body>
</html>
