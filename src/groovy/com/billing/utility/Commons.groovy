package com.billing.utility

import java.text.DecimalFormat

public class Commons {

	public static String convertToDecimalFormat(def amount){
		String format = new DecimalFormat("#.00").format(amount)
		return format
	}
}
