package com.billing.utility

public class Constants {
	public static class Frequency{
		public static final String TODAY = "Today"
		public static final String WEEKLY = "Weekly"
		public static final String MONTHLY = "Monthly"
		public static final String QUARTERLY = "Quarterly"
		public static final String YEARLY = "Yearly"
	} 
	
	public static final String DECIMAL_FORMAT = "##.00"
	public static final String DATE_FORMAT = "dd/MM/yyyy"
	
}

