package com.billing.utility
import java.text.SimpleDateFormat


public class InvoiceUtility {

	public final static DATE_FORMAT = "dd-MM-yyyy"
	/*static main(args) {
		def dateList = getFinancialYearFromStartDate("1-4-2013")
		println dateList
	}
*/
	def static getFinancialYearFromStartDate(String strStartDate){
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Calendar calenderInstance = Calendar.getInstance();
		Date startDate = sdf.parse(strStartDate);
		calenderInstance.setTime(startDate)
		calenderInstance.add(Calendar.YEAR, 1)
		Date endDate = calenderInstance.getTime() - 1
		println "startDate : " + endDate
		return [startDate, endDate]
		
	}
}
