package com.billing.utility;

public enum PaymentType {

	Cash(1), Credit(2)
	PaymentType(int value) {
		this.value = value
	}
	private final int value
	public int value() {
		return value
	}
	
	static list(){
		[Cash, Credit]
	}
}

/*
 * NANPCodeGroup.each{println "1: $it"}
NANPCodeGroup.each{println "2: ${it.value()}"}
println "3: ${NANPCodeGroup.values()}"
NANPCodeGroup.values().each{println "4: $it"}
println "5: ${NANPCodeGroup.USA.value()}"
println "6: ${NANPCodeGroup.USA}"
x = NANPCodeGroup.grep{it.value() == 3}[0]?:''
println "7: $x , ${x.class}"

1: USA
1: Canada
1: Carribean
1: USPacific
2: 3
2: 4
2: 5
2: 6
3: [USA, Canada, Carribean, USPacific]
4: USA
4: Canada
4: Carribean
4: USPacific
5: 3
6: USA
7: USA , class NANPCodeGroup
 */
