package com.billing.exception;

public class BillingException  extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BillingException(String message){
		super(message);
	}
}
