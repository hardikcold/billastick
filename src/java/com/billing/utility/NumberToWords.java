package com.billing.utility;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang.math.NumberUtils;

public class NumberToWords {

	public static void main(String[] args) {
		Double money = 1234.66;
		int rupees = (int) Math.floor(money);
		BigDecimal paisa = new BigDecimal((money - Math.floor(money)) * 100);
		paisa = paisa.setScale(4, RoundingMode.HALF_DOWN);

		String rupeesStr = NumberToText(rupees, false, false);
		String paisaStr = NumberToText(paisa.intValue(), false, false);
		System.out.println("rupeesStr :" + rupeesStr + " and " + paisaStr);
		// String decimalPart[] = d.toString().split('.')[1];
		// System.out.println("Number :" + d.toString());
		// String[] decimalPart = d.toString().split(".");
		// System.out.println("decimalPart :" + decimalPart);
		// String text = NumberToText(i, true, false) + " Point" +
		// DecimalToText(decimalPart);
		// System.out.println(text);
	}

	public static String getWordFromDigit(Double amount) {
		int rupees = (int) Math.floor(amount);
		BigDecimal paisa = new BigDecimal((amount - Math.floor(amount)) * 100);
		paisa = paisa.setScale(4, RoundingMode.HALF_DOWN);

		String rupeesStr = NumberToText(rupees, false, false);
		rupeesStr = rupeesStr + " Rupees";
		String paisaStr = "";
		if(paisa.intValue() > 0){
			paisaStr = NumberToText(paisa.intValue(), false, false);
			paisaStr = " and " + paisaStr + " Paisa";
		}
		return rupeesStr + paisaStr ;
	}

	public static String NumberToText(Integer number, boolean useAnd,
			boolean useArab) {
		if (number == 0)
			return "Zero";

		String and = useAnd ? "and " : ""; // deals with using 'and' separator

		if (number == -2147483648)
			return "Minus Two Hundred "
					+ and
					+ "Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred "
					+ and + "Forty Eight";

		int[] num = new int[4];
		int first = 0;
		int u, h, t;
		StringBuilder sb = new StringBuilder();

		if (number < 0) {
			sb.append("Minus ");
			number = -number;
		}
		String[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ",
				"Six ", "Seven ", "Eight ", "Nine " };
		String[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ",
				"Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ",
				"Nineteen " };
		String[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
				"Seventy ", "Eighty ", "Ninety " };
		String[] words3 = { "Thousand ", "Lakh ", "Crore " };
		num[0] = number % 1000; // units
		num[1] = number / 1000;
		num[2] = number / 100000;
		num[1] = num[1] - 100 * num[2]; // thousands
		num[3] = number / 10000000; // crores
		num[2] = num[2] - 100 * num[3]; // lakhs
		for (int i = 3; i > 0; i--) {
			if (num[i] != 0) {
				first = i;
				break;
			}
		}

		for (int i = first; i >= 0; i--) {
			if (num[i] == 0)
				continue;

			u = num[i] % 10; // ones
			t = num[i] / 10;
			h = num[i] / 100; // hundreds
			t = t - 10 * h; // tens

			if (h > 0)
				sb.append(words0[h] + "Hundred ");
			if (u > 0 || t > 0) {
				if (h > 0 || i < first)
					sb.append(and);

				if (t == 0)
					sb.append(words0[u]);
				else if (t == 1)
					sb.append(words1[u]);
				else
					sb.append(words2[t - 2] + words0[u]);
			}
			if (i != 0)
				sb.append(words3[i - 1]);
		}

		String temp = sb.toString().trim();

		if (useArab && Math.abs(number) >= 1000000000) {
			int index = temp.indexOf("Hundred Crore");
			if (index > -1)
				return temp.substring(0, index) + "Arab"
						+ temp.substring(index + 13);
			index = temp.indexOf("Hundred");
			return temp.substring(0, index) + "Arab"
					+ temp.substring(index + 7);
		}
		return temp;
	}
}
