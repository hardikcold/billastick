
function onlyDecimal(element, maxLength) {	
	var oldValue = ""
	$(element).keypress(function(event) {	
		var keynum
		if(window.event) {// IE8 and earlier
			keynum = event.keyCode;
		} else if(event.which) { // IE9/Firefox/Chrome/Opera/Safari
			keynum = event.which;
		} else {
			keynum = 0;
		}
		
		if(keynum == 8 || keynum == 0 || keynum == 9) {
			return;
		}

		var numeric
		if(keynum < 46
			|| keynum > 57
			|| keynum == 47) {
           event.preventDefault();
		} // prevent if not number/dot
		var value = this.value;
		if(value == "" && event.which == 46) {
			event.preventDefault();
		}
		var digits = 9 
		if(maxLength) {
			digits = maxLength - 3; 
		}
		var decimalIndex = value.indexOf(".");
		if(decimalIndex < 0 && value.length >= digits && keynum != 46) {
			event.preventDefault();
		}
		if(decimalIndex >= 0 && value.substring(0, decimalIndex).length <= 9) {
			oldValue = value;
		}
	});
	
	$(element).keyup(function(event) {
		var decimalPlaces = 2
		var allowNegative = false
		var temp = this.value;
		
		// avoid changing things if already formatted correctly
		var reg0Str = '[0-9]*';
		if (decimalPlaces > 0) {
			reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
		} else if (decimalPlaces < 0) {
			reg0Str += '\\.?[0-9]*';
		}
		reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
		reg0Str = reg0Str + '$';
		var reg0 = new RegExp(reg0Str);
		if (reg0.test(temp)) {
			
		   var digits = 9 
		   if(maxLength) {
			   digits = maxLength - 3; 
		   }
		   var decimalIndex = temp.indexOf(".");

		   if(decimalIndex < 0 && temp.length >= digits) {
			   temp = temp.substring(0, digits);
		   }
		   if(decimalIndex > 0 && oldValue) {
			   if(temp.substring(0, decimalIndex).length > 9) {
				   temp = oldValue;
			   }
		   }
		  // this.value = temp;	
		   return true;
		}
		// first replace all non numbers
		var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
		var reg1 = new RegExp(reg1Str, 'g');
		temp = temp.replace(reg1, '');

		if (allowNegative) {
			// replace extra negative
			var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
			var reg2 = /-/g;
			temp = temp.replace(reg2, '');
			if (hasNegative) temp = '-' + temp;
		}
		
		if (decimalPlaces != 0) {
			var reg3 = /\./g;
			var reg3Array = reg3.exec(temp);
			if (reg3Array != null) {
				// keep only first occurrence of .
				//  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
				var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
				reg3Right = reg3Right.replace(reg3, '');
				reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
				temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
			}
		}
		
	   this.value = temp;			
	
	});
	$(element).blur(function(event) {
		var value = this.value;
		if(value == '' || isNaN(value)) {
			this.value = "0.00";
		} else if(value != '') {
			this.value = parseFloat(value).toFixed(2);
		} 
	});

}

function onlyInteger(element) {	
	$(element).keypress(function(event) {
		var keynum
		if(window.event) { // IE8 and earlier
			keynum = event.keyCode;
		} else if(event.which) { // IE9/Firefox/Chrome/Opera/Safari
			keynum = event.which;
		} else {
			keynum = 0;
		}		
		
		if(keynum == 8 || keynum == 0 || keynum == 9) {
			return;
		}
		if(keynum <= 46
		|| keynum > 57
		|| keynum == 47) {
			event.preventDefault();
		} // prevent if not number/dot

	});
}