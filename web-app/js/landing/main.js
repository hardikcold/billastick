$(document).ready(function() {
 
//==================== Topbar ========================//

$(".topbar-btn").click(function(){
  $(".topbar").slideToggle("slow");
  $(this).toggleClass("active");
});

//==================== Twitterfeed ========================//

$("#tweets").tweet({
  count: 1,
  username: "envato"
});

//==================== Prettyphoto ========================//

$("a[rel^='prettyPhoto']").prettyPhoto({theme:'pp_default'});

//==================== hoverzoom ========================//

$('.es-carousel ul li a').hover(function(){
    $(this).find('img').stop().fadeTo('slow',0.4);
}, 
function(){
    $(this).find('img').stop().fadeTo('slow',1);
});

//==================== carousel ========================//

$('#carousel').elastislide({
  imageW 	: 220,
  minItems	: 4
});

//==================== toggle ========================//

$('#toggle-view li').click(function () {
	var text = $(this).children('div.panel');
	if (text.is(':hidden')) {
	text.slideDown('200');
	$(this).children('span').html('-');		
} else {
	text.slideUp('200');
	$(this).children('span').html('+');		
}
});

//==================== Back to top ========================//

	// hide #back-top first
	$("#back-top").hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

});

